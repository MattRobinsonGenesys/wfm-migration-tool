package com.blackchair.wfm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;


@Service
public class MealService extends WFMConfigService {

    @Autowired
    private SiteService siteService;

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgMealHolder mealHolder = this.getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgMeal wfmEntity : mealHolder.getObjectArray()) {
            CfgMealDTO dto = (CfgMealDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgMealHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgMealFilter cfgMealFilter = new CfgMealFilter();
        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgMealFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getSiteId() != null) {
                cfgMealFilter.withWmSiteId(filterDTO.getSiteId());
            }
            if (filterDTO.getShiftId() != null) {
                cfgMealFilter.withWmShiftId(filterDTO.getShiftId());
            }

            if (filterDTO.getIds() != null) {
                cfgMealFilter.withWmMealId(filterDTO.getIds());
            }
        }

        return super.getService(wfmObjectHolder).getMeal(cfgMealFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Meal.CFG_MEAL_SORT_NAME).withAscending(true),
                new CfgMealDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.updateMeal((CfgMeal) syncDTO.getMergedObject().getDto().getDataSOAP(), false, false);

        checkResponse(res);
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.insertMeal((CfgMeal) syncDTO.getMergedObject().getDto().getDataSOAP(), true);

        checkResponse(res);

        CfgMeal sourceObject = (CfgMeal) syncDTO.getSource().getDto().getDataSOAP();
        this.linkID.put(sourceObject.getWmMealId(), res.getObjectID());
    }

    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {

        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgMeal sourceObject = (CfgMeal) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgMeal targetObject = (CfgMeal) listTarget.get(hashCode).getDataSOAP();
                targetSiteIDs.put(sourceObject.getWmMealId(), targetObject.getWmMealId());
            }
        }

        this.linkID = targetSiteIDs;
    }

    List<CfgShiftMeal> syncShiftMeals(List<CfgShiftMeal> sourceShiftMeals) {
        for (CfgShiftMeal sourceShiftMeal : sourceShiftMeals) {
            if (this.linkID.containsKey(sourceShiftMeal.getWmMealId())) {
                sourceShiftMeal.setWmMealId(this.linkID.get(sourceShiftMeal.getWmMealId()));
            }
        }

        return sourceShiftMeals;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgMeal wfmObject = (CfgMeal) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "Meals";
    }


    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {

        CfgMeal sourceObject = (CfgMeal) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;

        boolean doAction = false;

        switch (status) {
            case UPDATE:
                CfgMeal targetObject = (CfgMeal) target.getDto().getDataSOAP();
                this.linkID.put(sourceObject.getWmMealId(), targetObject.getWmMealId());

                sourceObject.setWmMealId(targetObject.getWmMealId());
                sourceObject.setWmSiteIdNSizeIs(targetObject.getWmSiteIdNSizeIs());
                sourceObject.setWmSiteId(targetObject.getWmSiteId());

                if (sourceObject.getWmDuration() != targetObject.getWmDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmEndDuration() != targetObject.getWmEndDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmLengthAfterMeal() != targetObject.getWmLengthAfterMeal()) {
                    doAction = true;
                }

                if (sourceObject.getWmLengthBeforeMeal() != targetObject.getWmLengthBeforeMeal()) {
                    doAction = true;
                }

                if (sourceObject.getWmStartStep() != targetObject.getWmStartStep()) {
                    doAction = true;
                }

                if (sourceObject.getWmStartTime() != targetObject.getWmStartTime()) {
                    doAction = true;
                }
                break;
            case INSERT:
                doAction = true;
                List<Integer> sourceSiteIDs = sourceObject.getWmSiteId();

                sourceObject.setWmSiteId(siteService.getTargetSiteIDsBySourceSiteIDs(sourceSiteIDs));
                break;
        }

        if (doAction) {
            CfgMealDTO objectDTO = new CfgMealDTO();
            objectDTO.setDataSOAP(sourceObject);

            try {
                mergedObjectHolder = target.setDto(objectDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mergedObjectHolder;
    }
}
