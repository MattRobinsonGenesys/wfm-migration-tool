package com.blackchair.wfm;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TeamService extends WFMConfigService {
    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgTeamHolder cfgCarpoolHolder = this.getCfgHolder(wfmObjectHolder);
        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgTeam wfmEntity : cfgCarpoolHolder.getObjectArray()) {
            CfgTeamDTO dto = (CfgTeamDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgTeamHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgTeamFilter cfgTeamFilter = new CfgTeamFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgTeamFilter.withWmBUId(filterDTO.getBuId());
            }

            if (filterDTO.getSiteId() != null) {
                cfgTeamFilter.withWmSiteId(filterDTO.getSiteId());
            }
        }

        return super.getService(wfmObjectHolder).getTeam(cfgTeamFilter, cfgAgentFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Team.CFG_TEAM_SORT_NAME).withAscending(true),
                new CfgTeamDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        return null;
    }

    @Override
    public String getObjectServiceName() {
        return null;
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgTeam sourceObject = (CfgTeam) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgTeam targetObject = (CfgTeam) listTarget.get(hashCode).getDataSOAP();
                this.linkID.put(sourceObject.getWmTeamId(), targetObject.getWmTeamId());
            }
        }
    }
}
