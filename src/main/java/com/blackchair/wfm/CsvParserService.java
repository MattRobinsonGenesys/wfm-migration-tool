package com.blackchair.wfm;

import com.blackchair.dto.WfmObjectHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CsvParserService {

    @Autowired
    private AgentService agentService;

    @Autowired
    private CalendarItemService calendarItemService;

    public boolean csvParser(WfmObjectHolder holder, int type) {
        boolean success;
        switch (type) {
            case 1: success = this.agentService.updateFromCSV(holder);
                    break;
            case 2: success = this.agentService.parseTimeOffBonus(holder);
                    break;
            case 3: success = this.calendarItemService.parseCalendarItems(holder, 1);
                    break;
            case 4: success = this.calendarItemService.parseCalendarItems(holder, 2);
                    break;
            default: success = false;
        }

        return success;
    }
}
