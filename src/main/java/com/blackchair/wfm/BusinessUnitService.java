package com.blackchair.wfm;

import java.util.HashMap;
import java.util.Map;

import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;

@Service
public class BusinessUnitService extends WFMConfigService {

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        // Retrieve Business Unit names
        CfgBUHolder buHolder = this.getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgBU wfmEntity : buHolder.getObjectArray()) {
            CfgBUDTO dto = (CfgBUDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgBUHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        return super.getService(wfmObjectHolder).getBU(null, // All BUs
                new CfgSortMode().withSortMode(ECfgSortMode.BU.CFG_BU_SORT_NAME).withAscending(true), // ascending
                new CfgBUDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgBU wfmObject = (CfgBU) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgBU sourceObject = (CfgBU) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgBU targetObject = (CfgBU) listTarget.get(hashCode).getDataSOAP();
                targetSiteIDs.put(sourceObject.getWmBUId(), targetObject.getWmBUId());
            }
        }

        this.linkID = targetSiteIDs;
    }

    @Override
    public String getObjectServiceName() {
        return "CfgBU";
    }
}
