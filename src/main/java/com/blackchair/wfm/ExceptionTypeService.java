package com.blackchair.wfm;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ExceptionTypeService extends WFMConfigService {
    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgExceptionTypeHolder cfgExceptionTypeHolder = this.getCfgHolder(wfmObjectHolder);
        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgExceptionType wfmEntity : cfgExceptionTypeHolder.getObjectArray()) {
            CfgExceptionTypeDTO dto = (CfgExceptionTypeDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgExceptionTypeHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgExceptionTypeFilter cfgExceptionTypeFilter = new CfgExceptionTypeFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgExceptionTypeFilter.withWmBUId(filterDTO.getBuId());
            }

            if (filterDTO.getSiteId() != null) {
                cfgExceptionTypeFilter.withWmSiteId(filterDTO.getSiteId());
            }
        }

        return super.getService(wfmObjectHolder).getExceptionType(cfgExceptionTypeFilter, cfgAgentFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.ExceptionType.CFG_EXCEPTION_TYPE_SORT_NAME).withAscending(true),
                new CfgExceptionTypeDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        return null;
    }

    @Override
    public String getObjectServiceName() {
        return null;
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {

    }
}
