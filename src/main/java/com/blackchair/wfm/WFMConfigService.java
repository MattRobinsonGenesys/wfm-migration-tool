package com.blackchair.wfm;

import java.util.HashMap;

import com.blackchair.dto.SyncDTO;
import com.genesyslab.wfm8.API.service.calendar851.CalItemError;
import com.genesyslab.wfm8.API.service.calendar851.CalItemWarning;
import com.genesyslab.wfm8.API.service.calendar851.CalValidationHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import com.blackchair.FactoryServiceSoap;
import com.blackchair.auth.AuthenticationService;
import com.blackchair.auth.Session;
import com.blackchair.dto.BaseDTO;
import com.blackchair.dto.WfmObjectHolder;
import com.genesyslab.wfm8.API.service.calendar851.WFMCalendarService851Soap;
import com.genesyslab.wfm8.API.service.config852.CfgValidationHolder;
import com.genesyslab.wfm8.API.service.config852.CfgValidationMsg;
import com.genesyslab.wfm8.API.service.config852.WFMConfigService852Soap;


public abstract class WFMConfigService {

    @Autowired
    protected AuthenticationService authenticationService;
    @Autowired
    protected ApplicationEventPublisher event;

    //sourceId, targetId
    protected HashMap<Integer, Integer> linkID = new HashMap<>();

    private String wsdLocationName = "/ConfigService/WFMConfigService852.wsdl";
    private String wfmServiceName = "WFMConfigService852";
    private String serviceSOAPName = "WFMConfigService852Soap";
    private String serviceName = "ConfigService852";

    public WFMConfigService852Soap getService(WfmObjectHolder wfmObjectHolder) throws Exception {
        Session session = this.authenticationService.connect(wfmObjectHolder.getConnection());

        return FactoryServiceSoap.create(WFMConfigService852Soap.class, this.wsdLocationName, this.wfmServiceName,
                this.serviceSOAPName, session.getServiceMap().get(this.serviceName).getServiceURL());
    }

    WFMCalendarService851Soap getCalendarService(WfmObjectHolder wfmObjectHolder) throws Exception {
        Session session = this.authenticationService.connect(wfmObjectHolder.getConnection());

        String wsdLocationName = "/CalendarService/WFMCalendarService851.wsdl";
        String wfmServiceName = "WFMCalendarService851";
        String serviceSOAPName = "WFMCalendarService851Soap";
        String serviceName = "CalendarService851";

        return FactoryServiceSoap.create(WFMCalendarService851Soap.class, wsdLocationName, wfmServiceName, serviceSOAPName,
                session.getServiceMap().get(serviceName).getServiceURL());
    }

    public abstract <T extends BaseDTO> HashMap<Integer, T> getAll(WfmObjectHolder wfmObjectHolder) throws Exception;

    public abstract void update(SyncDTO syncDTO) throws Exception;

    public abstract void insert(SyncDTO syncDTO) throws Exception;

    public abstract WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status);

    public abstract String getObjectName(WfmObjectHolder wfmObjectHolder);

    public abstract String getObjectServiceName();

    void checkResponse(CfgValidationHolder res) {
        if (!res.isSuccess()) {

            StringBuilder error = new StringBuilder();
            if (res.getErrorArrayNSizeIs() > 0) {
                for (CfgValidationMsg message : res.getErrorArray()) {
                    error.append(" ").append(message.getMessage());
                }
            }

            if (res.getWarningArrayNSizeIs() > 0) {
                for (CfgValidationMsg message : res.getWarningArray()) {
                    error.append(" ").append(message.getMessage());
                }
            }

            throw new IllegalStateException(error.toString());
        }
    }

    void checkResponse(CalValidationHolder res){
        if (((res.getErrorArraySize() > 0) || (res.getWarningArraySize() > 0))) {

            StringBuilder error = new StringBuilder();
            if (res.getErrorArraySize() > 0) {
                for (CalItemError message : res.getErrors()) {
                    error.append(" ").append(message.getError());
                }
            }

            if (res.getWarningArraySize() > 0) {
                for (CalItemWarning message : res.getWarnings()) {
                    error.append(" ").append(message.getWarning());
                }
            }

            throw new IllegalStateException(error.toString());
        }
    }

    public abstract void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception;

    Integer getTargetIdBySourceId(Integer sourceId) throws IllegalArgumentException {
        if (!this.linkID.containsKey(sourceId)) {
            throw new IllegalArgumentException("Missing target object");
        }

        return this.linkID.get(sourceId);
    }

    public HashMap<Integer, Integer> getLinkID() {
        return linkID;
    }

    public void setLinkID(HashMap<Integer, Integer> linkID) {
        this.linkID = linkID;
    }
}
