package com.blackchair.wfm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blackchair.aggregator.MergeService;
import com.blackchair.dto.*;
import com.blackchair.job.SetupObjects;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.event.LogEvent;


@Service
public class ShiftService extends WFMConfigService {

    @Autowired
    protected BreakService breakService;
    @Autowired
    protected MealService mealService;
    @Autowired
    protected ContractService contractService;
    @Autowired
    protected SiteService siteService;
    @Autowired
    protected TaskSequenceService taskSequenceService;

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgShiftHolder shiftHolder = getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgShift cfgShift : shiftHolder.getObjectArray()) {
            CfgShiftDTO cfgShiftDTO = (CfgShiftDTO) FactoryDTO.create(cfgShift);
            list.put(cfgShiftDTO.hashCode(), cfgShiftDTO);
        }

        return list;
    }

    public CfgShiftHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgShiftFilter cfgShiftFilter = new CfgShiftFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgShiftFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getIds() != null) {
                cfgShiftFilter.withWmShiftId(filterDTO.getIds());
            }
            if (filterDTO.getSiteId() != null) {
                cfgShiftFilter.withWmSiteId(filterDTO.getSiteId());
            }
            if (filterDTO.getContractId() != null) {
                cfgShiftFilter.withWmContractId(filterDTO.getContractId());
            }
            if (filterDTO.getBreakId() != null) {
                cfgShiftFilter.withWmBreakId(filterDTO.getBreakId());
            }
            if (filterDTO.getMealId() != null) {
                cfgShiftFilter.withWmMealId(filterDTO.getMealId());
            }
            if (filterDTO.getTaskSequenceId() != null) {
                cfgShiftFilter.withWmTaskSequenceId(filterDTO.getTaskSequenceId());
            }
        }

        return super.getService(wfmObjectHolder).getShift(cfgShiftFilter, cfgAgentFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Shift.CFG_SHIFT_SORT_NAME).withAscending(true),
                new CfgShiftDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgShift wfmObject = (CfgShift) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "Shifts";
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {
        this.setupSourceEntity(syncDTO);

        CfgShift mergedObject = (CfgShift) syncDTO.getMergedObject().getDto().getDataSOAP();
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.updateShift(mergedObject, false, false);

        checkResponse(res);
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        this.setupSourceEntity(syncDTO);

        CfgShift mergedObject = (CfgShift) syncDTO.getMergedObject().getDto().getDataSOAP();
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.insertShift(mergedObject, true);

        checkResponse(res);

        CfgShift sourceObject = (CfgShift) syncDTO.getSource().getDto().getDataSOAP();
        this.linkID.put(sourceObject.getWmShiftId(), res.getObjectID());
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        CfgShift sourceObject = (CfgShift) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;

        boolean doAction = false;

        switch (status) {
            case UPDATE:
                CfgShift targetObject = (CfgShift) target.getDto().getDataSOAP();

                this.linkID.put(sourceObject.getWmShiftId(), targetObject.getWmShiftId());

                sourceObject.setWmShiftId(targetObject.getWmShiftId());
                sourceObject.setWmSiteIdNSizeIs(targetObject.getWmSiteIdNSizeIs());
                sourceObject.setWmSiteId(targetObject.getWmSiteId());

                if (sourceObject.getWmBreakMaxDistance() != targetObject.getWmBreakMaxDistance()) {
                    doAction = true;
                }

                if (sourceObject.getWmBreakMinDistance() != targetObject.getWmBreakMinDistance()) {
                    doAction = true;
                }

                if (sourceObject.getWmConsecutiveDays() != targetObject.getWmConsecutiveDays()) {
                    doAction = true;
                }

                if (sourceObject.getWmDayOffRule() != targetObject.getWmDayOffRule()) {
                    doAction = true;
                }

                if (sourceObject.getWmDistribPeriod() != targetObject.getWmDistribPeriod()) {
                    doAction = true;
                }

                if (sourceObject.getWmEarliestEndDuration() != targetObject.getWmEarliestEndDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmEarliestStartTime() != targetObject.getWmEarliestStartTime()) {
                    doAction = true;
                }

                if (sourceObject.getWmStartStep() != targetObject.getWmStartStep()) {
                    doAction = true;
                }

                if (sourceObject.getWmLatestEndDuration() != targetObject.getWmLatestEndDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmMaxDuration() != targetObject.getWmMaxDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmMaxOccur() != targetObject.getWmMaxOccur()) {
                    doAction = true;
                }

                if (sourceObject.getWmMinDuration() != targetObject.getWmMinDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmMinOccur() != targetObject.getWmMinOccur()) {
                    doAction = true;
                }

                if (sourceObject.getWmLatestStartDuration() != targetObject.getWmLatestStartDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmTaskSequenceUsage() != targetObject.getWmTaskSequenceUsage()) {
                    doAction = true;
                }

                for (int i = 0; i < sourceObject.getWmOpenWeekDays().size(); i++) {
                    if (sourceObject.getWmOpenWeekDays().get(i) != targetObject.getWmOpenWeekDays().get(i)) {
                        doAction = true;
                        break;
                    }
                }
                break;
            case INSERT:
                doAction = true;
                List<Integer> sourceSiteIDs = sourceObject.getWmSiteId();
                sourceObject.setWmSiteId(siteService.getTargetSiteIDsBySourceSiteIDs(sourceSiteIDs));
                break;
        }

        if (this.checkMeals(source, target)) {
            doAction = true;
        }

        if (this.checkBreaks(source, target)) {
            doAction = true;
        }

        if (this.checkTaskSequences(source, target)) {
            doAction = true;
        }

        if (this.checkContracts(source, target)) {
            doAction = true;
        }

        if (doAction) {
            CfgShiftDTO cfgDTO = new CfgShiftDTO();
            cfgDTO.setDataSOAP(sourceObject);

            try {
                mergedObjectHolder = target.setDto(cfgDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mergedObjectHolder;
    }

    private boolean checkMeals(WfmObjectHolder source, WfmObjectHolder target) throws IllegalArgumentException {
        if (source.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift sourceObject = (CfgShift) source.getDto().getDataSOAP();

        if (!sourceObject.getWmShiftMeals().isEmpty() && (target.getDto() == null || ((CfgShift) target.getDto().getDataSOAP()).getWmShiftMeals().isEmpty())) {

            if (!MergeService.servicesForMerge.contains(mealService)) {
                List<Integer> searchIDs = new ArrayList<>();
                for (CfgShiftMeal cfgShiftMeal : sourceObject.getWmShiftMeals()) {
                    searchIDs.add(cfgShiftMeal.getWmMealId());
                }

                source.getFilter().setIds(searchIDs);
                SetupObjects.scan(mealService, source, target);
                source.getFilter().setIds(null);
            }

            return true;
        }

        return false;
    }

    private boolean checkBreaks(WfmObjectHolder source, WfmObjectHolder target) throws IllegalArgumentException {
        if (source.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift sourceObject = (CfgShift) source.getDto().getDataSOAP();

        if (!sourceObject.getWmShiftBreaks().isEmpty() && (target.getDto() == null || ((CfgShift) target.getDto().getDataSOAP()).getWmShiftBreaks().isEmpty())) {

            if (!MergeService.servicesForMerge.contains(breakService)) {
                List<Integer> searchIDs = new ArrayList<>();
                for (CfgShiftBreak cfgShiftBreak : sourceObject.getWmShiftBreaks()) {
                    searchIDs.add(cfgShiftBreak.getWmBreakId());
                }

                source.getFilter().setIds(searchIDs);
                SetupObjects.scan(breakService, source, target);
                source.getFilter().setIds(null);
            }

            return true;
        }

        return false;
    }

    private boolean checkTaskSequences(WfmObjectHolder source, WfmObjectHolder target) throws IllegalArgumentException {
        if (source.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift sourceObject = (CfgShift) source.getDto().getDataSOAP();

        if (!sourceObject.getWmShiftTaskSequences().isEmpty() && (target.getDto() == null || ((CfgShift) target.getDto().getDataSOAP()).getWmShiftTaskSequences().isEmpty())) {
            if (!MergeService.servicesForMerge.contains(taskSequenceService)) {
                source.getFilter().setIds(sourceObject.getWmShiftTaskSequences());
                SetupObjects.scan(taskSequenceService, source, target);
                source.getFilter().setIds(null);
            }

            return true;
        }

        return false;
    }

    private boolean checkContracts(WfmObjectHolder source, WfmObjectHolder target) throws IllegalArgumentException {
        if (source.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift sourceObject = (CfgShift) source.getDto().getDataSOAP();
        if (!sourceObject.getWmShiftContracts().isEmpty() && (target.getDto() == null || ((CfgShift) target.getDto().getDataSOAP()).getWmShiftContracts().isEmpty())) {
            if (!MergeService.servicesForMerge.contains(contractService)) {
                List<Integer> searchIDs = new ArrayList<>();
                for (CfgContractShift cfgShiftContract : sourceObject.getWmShiftContracts()) {
                    searchIDs.add(cfgShiftContract.getWmContractId());
                }
                source.getFilter().setIds(searchIDs);
                SetupObjects.scan(contractService, source, target);
                source.getFilter().setIds(null);
            }

            return true;
        }

        return false;
    }

    private void setupSourceEntity(SyncDTO syncDTO) throws Exception {
        WfmObjectHolder mergedObjectHolder = syncDTO.getMergedObject();
        WfmObjectHolder target = syncDTO.getTarget();

        this.event.publishEvent(new LogEvent(this, "setup the source associates... "));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        try {
            this.associatingMeals(mergedObjectHolder, target);
        } catch (Exception e) {
            this.event.publishEvent(new LogEvent(this, e.getMessage()));
        }

        try {
            this.associatingBreaks(mergedObjectHolder, target);
        } catch (Exception e) {
            this.event.publishEvent(new LogEvent(this, e.getMessage()));
        }

        try {
            this.associatingTaskSequences(mergedObjectHolder, target);
        } catch (Exception e) {
            this.event.publishEvent(new LogEvent(this, e.getMessage()));
        }

        try {
            this.associatingContracts(mergedObjectHolder, target);
        } catch (Exception e) {
            this.event.publishEvent(new LogEvent(this, e.getMessage()));
        }
    }

    private void associatingMeals(WfmObjectHolder mergedObjectHolder, WfmObjectHolder target) throws IllegalArgumentException {
        this.event.publishEvent(new LogEvent(this, "associating meal relations ... "));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift mergedSourceObject = (CfgShift) mergedObjectHolder.getDto().getDataSOAP();
        List<CfgShiftMeal> cfgRelationsId = new ArrayList<>();

        if (target.getDto() == null) {
            if (!mergedSourceObject.getWmShiftMeals().isEmpty()) {
                cfgRelationsId = mealService.syncShiftMeals(mergedSourceObject.getWmShiftMeals());
            }

        } else {
            CfgShift targetObject = (CfgShift) target.getDto().getDataSOAP();

            if (!mergedSourceObject.getWmShiftMeals().isEmpty()) {
                cfgRelationsId = mealService.syncShiftMeals(mergedSourceObject.getWmShiftMeals());
            } else if (!mergedSourceObject.getWmShiftMeals().isEmpty() && targetObject.getWmShiftMeals().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "target does not have any meals... merging meals..."));
                cfgRelationsId = mealService.syncShiftMeals(mergedSourceObject.getWmShiftMeals());
            } else if (!mergedSourceObject.getWmShiftMeals().isEmpty() && !targetObject.getWmShiftMeals().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "Warning " + getObjectServiceName() + ": " + getObjectName(target) + " already  has meals"));
                cfgRelationsId = targetObject.getWmShiftMeals();
            } else if (mergedSourceObject.getWmShiftMeals().isEmpty() && !targetObject.getWmShiftMeals().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "source does not have any meals... but target has..."));
                cfgRelationsId = targetObject.getWmShiftMeals();
            }
        }

        mergedSourceObject.setWmShiftMeals(cfgRelationsId);
    }

    private void associatingBreaks(WfmObjectHolder mergedObjectHolder, WfmObjectHolder target) throws IllegalArgumentException {
        this.event.publishEvent(new LogEvent(this, "associating break relations ... "));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift mergedSourceObject = (CfgShift) mergedObjectHolder.getDto().getDataSOAP();

        List<CfgShiftBreak> cfgRelationsId = new ArrayList<>();
        if (target.getDto() == null) {
            if (!mergedSourceObject.getWmShiftBreaks().isEmpty()) {
                cfgRelationsId = breakService.syncShiftBreaks(mergedSourceObject.getWmShiftBreaks());
            }
        } else {
            CfgShift targetObject = (CfgShift) target.getDto().getDataSOAP();

            if (!mergedSourceObject.getWmShiftBreaks().isEmpty() && targetObject.getWmShiftBreaks().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "target does not have any breaks... merging breaks..."));
                cfgRelationsId = breakService.syncShiftBreaks(mergedSourceObject.getWmShiftBreaks());
            } else if (!mergedSourceObject.getWmShiftBreaks().isEmpty() && !targetObject.getWmShiftBreaks().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "Warning " + getObjectServiceName() + ": " + getObjectName(target) + " already  has breaks"));
                cfgRelationsId = targetObject.getWmShiftBreaks();
            } else if (mergedSourceObject.getWmShiftBreaks().isEmpty() && !targetObject.getWmShiftBreaks().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "source does not have any breaks... but target has..."));
                cfgRelationsId = targetObject.getWmShiftBreaks();
            }
        }

        mergedSourceObject.setWmShiftBreaks(cfgRelationsId);
    }

    private void associatingTaskSequences(WfmObjectHolder mergedObjectHolder, WfmObjectHolder target) throws IllegalArgumentException {
        this.event.publishEvent(new LogEvent(this, "associating task sequence relations ... "));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift mergedSourceObject = (CfgShift) mergedObjectHolder.getDto().getDataSOAP();

        List<Integer> cfgRelationsId = new ArrayList<>();
        if (target.getDto() == null) {
            if (!mergedSourceObject.getWmShiftTaskSequences().isEmpty()) {
                cfgRelationsId = taskSequenceService.syncShiftTaskSequences(mergedSourceObject.getWmShiftTaskSequences());
            }
        } else {
            CfgShift targetObject = (CfgShift) target.getDto().getDataSOAP();

            if (!mergedSourceObject.getWmShiftTaskSequences().isEmpty() && targetObject.getWmShiftTaskSequences().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "target does not have any task sequence... merging task sequence..."));
                cfgRelationsId = taskSequenceService.syncShiftTaskSequences(mergedSourceObject.getWmShiftTaskSequences());
            } else if (!mergedSourceObject.getWmShiftTaskSequences().isEmpty() && !targetObject.getWmShiftTaskSequences().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "Warning " + getObjectServiceName() + ": " + getObjectName(target) + " already  has task sequences"));
                cfgRelationsId = targetObject.getWmShiftTaskSequences();
            } else if (mergedSourceObject.getWmShiftTaskSequences().isEmpty() && !targetObject.getWmShiftTaskSequences().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "source does not have any task sequences... but target has..."));
                cfgRelationsId = targetObject.getWmShiftTaskSequences();
            }
        }

        mergedSourceObject.setWmShiftTaskSequences(cfgRelationsId);

        if (mergedSourceObject.getWmShiftTaskSequences().size() > 0 && mergedSourceObject.getWmTaskSequenceUsage() != 0) {
            mergedSourceObject.setWmTaskSequenceUsage(1);
        } else {
            mergedSourceObject.setWmTaskSequenceUsage(0);
        }

    }

    private void associatingContracts(WfmObjectHolder mergedObjectHolder, WfmObjectHolder target) throws IllegalArgumentException {
        this.event.publishEvent(new LogEvent(this, "associating contract relations ... "));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgShift mergedSourceObject = (CfgShift) mergedObjectHolder.getDto().getDataSOAP();
        List<CfgContractShift> cfgRelationsId = new ArrayList<>();

        if (target.getDto() == null) {
            if (!mergedSourceObject.getWmShiftContracts().isEmpty()) {
                cfgRelationsId = contractService.syncShiftContracts(mergedSourceObject.getWmShiftContracts());
            }
        } else {
            CfgShift targetObject = (CfgShift) target.getDto().getDataSOAP();
            if (!mergedSourceObject.getWmShiftContracts().isEmpty()) {
                cfgRelationsId = contractService.syncShiftContracts(mergedSourceObject.getWmShiftContracts());
            } else if (!mergedSourceObject.getWmShiftContracts().isEmpty() && targetObject.getWmShiftContracts().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "target does not have any contracts... merging contracts..."));
                cfgRelationsId = contractService.syncShiftContracts(mergedSourceObject.getWmShiftContracts());
            } else if (!mergedSourceObject.getWmShiftContracts().isEmpty() && !targetObject.getWmShiftContracts().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "Warning " + getObjectServiceName() + ": " + getObjectName(target) + " already  has contracts"));
                cfgRelationsId = targetObject.getWmShiftContracts();
            } else if (mergedSourceObject.getWmShiftContracts().isEmpty() && !targetObject.getWmShiftContracts().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "source does not have any contracts... but target has..."));
                cfgRelationsId = targetObject.getWmShiftContracts();
            }
        }

        mergedSourceObject.setWmShiftContracts(cfgRelationsId);
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgShift sourceObject = (CfgShift) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgShift targetObject = (CfgShift) listTarget.get(hashCode).getDataSOAP();
                targetSiteIDs.put(sourceObject.getWmShiftId(), targetObject.getWmShiftId());
            }
        }

        this.linkID = targetSiteIDs;
    }
}
