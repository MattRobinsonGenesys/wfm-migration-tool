package com.blackchair.wfm;

import com.blackchair.PocApplication;
import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.csvheaders.BaseHeaders;
import com.blackchair.csvheaders.CalendarItemHeaders;
import com.blackchair.dto.*;
import com.blackchair.event.LogEvent;
import com.blackchair.event.LogImportEvent;
import com.blackchair.event.LogSystemEvent;
import com.blackchair.event.importcsv.StartedImportEvent;
import com.blackchair.event.importcsv.StoppedImportEvent;
import com.blackchair.event.systemlog.StartedSystemEvent;
import com.csvreader.CsvReader;
import com.genesyslab.wfm8.API.service.calendar851.*;
import com.genesyslab.wfm8.API.service.config851.CfgActivityFilter;
import com.genesyslab.wfm8.API.service.config851.CfgAgentDetails;
import com.genesyslab.wfm8.API.service.config851.CfgAgentFilter;
import com.genesyslab.wfm8.API.service.config852.*;
import com.genesyslab.wfm8.API.util.OleDateTime;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class CalendarItemService extends WFMConfigService {
    @Autowired
    SiteService siteService;
    @Autowired
    AgentService agentService;
    @Autowired
    ExceptionTypeService exceptionTypeService;
    @Autowired
    TimeOffTypeService timeOffTypeService;

    @Autowired
    private ApplicationEventPublisher event;

    @Autowired
    private UploadFileService uploadFileService;

    //Fill this when we first call insert so we don't have to call getAll every time we look up them
    private HashMap<String, Integer> siteCache;
    private HashMap<Integer, Collection<BaseDTO>> timeOffTypeCache;
    private HashMap<Integer, Collection<BaseDTO>> exceptionCache;

    private WfmObjectHolder wfmObjectHolder;
    private CsvReader reader;

    private void clearCache() {
        siteCache = new HashMap<>();
        timeOffTypeCache = new HashMap<>();
        exceptionCache = new HashMap<>();
    }

    private List<Double> oneYearBackTwoForward() {
        List<Double> days = new ArrayList<>();
        Calendar cal = Calendar.getInstance();

        Integer year = cal.get(Calendar.YEAR) - 1;
        Integer month = cal.get(Calendar.MONTH) + 1;
        Integer day = cal.get(Calendar.DAY_OF_MONTH) + 1;

        for (int i = 1; i <= 37; i++) {
            if (month >= 13) {
                year++;
                month = 1;
            }
            //ZA MESECI S 31 DNI
            switch (month) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12: {
                    if (i < 37) {
                        while (day <= 31) {
                            //System.out.println(new OleDateTime(year, month, day));
                            days.add(new OleDateTime(year, month, day).asDouble());
                            day++;
                        }
                        day = 1;
                        month++;
                        break;
                    } else {
                        while (day < cal.get(Calendar.DAY_OF_MONTH)) {
                            //System.out.println(new OleDateTime(year, month, day));
                            days.add(new OleDateTime(year, month, day).asDouble());
                            day++;
                        }
                        day = 1;
                        month++;
                        break;
                    }
                }
                //ZA MESECI S 30 DNI
                case 4:
                case 6:
                case 9:
                case 11: {
                    if (i < 37) {
                        while (day <= 30) {
                            //System.out.println(new OleDateTime(year, month, day));
                            days.add(new OleDateTime(year, month, day).asDouble());
                            day++;
                        }
                        day = 1;
                        month++;
                        break;
                    } else {
                        while (day < cal.get(Calendar.DAY_OF_MONTH)) {
                            //System.out.println(new OleDateTime(year, month, day));
                            days.add(new OleDateTime(year, month, day).asDouble());
                            day++;
                        }
                        day = 1;
                        month++;
                        break;
                    }
                }
                //ZA FEVRUARI
                case 2: {
                    if (i < 37) {
                        if (year % 4 == 0) {
                            while (day <= 29) {
                                //System.out.println(new OleDateTime(year, month, day));
                                days.add(new OleDateTime(year, month, day).asDouble());
                                day++;
                            }
                            day = 1;
                            month++;
                            break;
                        } else {
                            while (day <= 28) {
                                //System.out.println(new OleDateTime(year, month, day));
                                days.add(new OleDateTime(year, month, day).asDouble());
                                day++;
                            }
                            day = 1;
                            month++;
                            break;
                        }
                    } else {
                        while (day < cal.get(Calendar.DAY_OF_MONTH)) {
                            //System.out.println(new OleDateTime(year, month, day));
                            days.add(new OleDateTime(year, month, day).asDouble());
                            day++;
                        }
                        day = 1;
                        month++;
                        break;
                    }
                }
            }
        }
        return days;
    }

    private boolean matchCalendarCsv(List<String> header) {
        //Required fields: Employee ID,First Name,Last Name,Site ID,Date,Item Type,Full Day
        List<String> validator = new ArrayList<>();
        CalendarItemHeaders calendarItemHeaders = new CalendarItemHeaders();
        validator.addAll(BaseHeaders.getIdentifiers());
        validator.addAll(calendarItemHeaders.getRequiredFields());
        //Optional fields: Start Time,End Time,Paid Time,Comment
        //Check if each field in validator matches any of the fields in header. If it matches a given field, remove it from the stream.
        //If the stream is not empty, this means that one or more fields didn't match, so return false.
        return validator.stream().filter(x -> (!header.stream().anyMatch(y -> y.equals(x)))).count() <= 0;
    }

    private CalData getCalData(WfmObjectHolder wfmObjectHolder) throws Exception {

        WFMCalendarService851Soap calService = super.getCalendarService(wfmObjectHolder);

        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();
        CfgActivityFilter cfgActivityFilter = new CfgActivityFilter();
        CfgAgentDetails cfgAgentDetails = new CfgAgentDetails();

        List<Double> dates = oneYearBackTwoForward();
//        List<Double> dates = new ArrayList<>();
//        dates.add(new OleDateTime(2017, 8, 1).asDouble());

        CalFilter calFilter = new CalFilter();
        CalSort calSort = new CalSort();
        cfgAgentFilter.withGswAgentId(313);

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgAgentFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getSiteId() != null) {
                cfgAgentFilter.withWmSiteId(filterDTO.getSiteId());
            }
        }

        return calService.queryCalendarItemsForDates(ECalAccessLevels.CAL_ACCESSLEVEL_ADMIN, cfgAgentFilter,
                cfgActivityFilter, cfgAgentDetails.withInfoType(ECfgInfoType.CFG_INFO_OBJECT_SHORT), 0, dates, 0, calFilter, calSort,
                0);
    }

    boolean parseCalendarItems(WfmObjectHolder wfmObjectHolder, int type) {
        //type 1 = exception, 2 = time-off
        Long start = Calendar.getInstance().getTimeInMillis();
        this.wfmObjectHolder = wfmObjectHolder;
        String fileName = this.uploadFileService.getRootPath() + File.separator + "CSV files" + File.separator + "data.csv";
        boolean success = true;

        this.event.publishEvent(new StartedImportEvent(this));
        if (PocApplication.debugMode) {
            this.event.publishEvent(new StartedSystemEvent(this));
        }

        try {
            WFMCalendarService851Soap calService = super.getCalendarService(wfmObjectHolder);
            //Open a csvreader
            //TODO reader = new CsvReader(fileName,',', Charset.forName("UTF-16"));
            reader = new CsvReader(fileName);
            //Read the headers of the csv
            reader.readHeaders();
            if (!matchCalendarCsv(Arrays.asList(reader.getHeaders()))) {
                if (type == 1) {
                    throw new IllegalArgumentException("CSV File Headers don't match the required headers for Calendar Exceptions");
                } else {
                    throw new IllegalAccessException("CSV File Headers don't match the required headers for Calendar Time-off");
                }
            }
            Set<String> siteNames = new HashSet<>();
            List<Integer> siteIDs = new ArrayList<>();
            while (reader.readRecord()) {
                siteNames.add(reader.get(CalendarItemHeaders.SITE));
            }
            reader.close();
            //TODO reader = new CsvReader(fileName,',', Charset.forName("UTF-16"));
            reader = new CsvReader(fileName);
            reader.readHeaders();

            //Fill the agent cache
            this.event.publishEvent(new LogImportEvent(this, "Caching started"));
            siteCache = siteService.getSiteIDsAndNames(wfmObjectHolder);
            this.event.publishEvent(new LogImportEvent(this, "Sites cached"));
            //FILTER THE SITES
            siteNames.forEach(x -> siteIDs.add(siteCache.get(x)));
            if (type == 1) {
                exceptionCache = agentService.fillCache(exceptionTypeService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Exception Types cached"));
            } else {
                timeOffTypeCache = agentService.fillCache(timeOffTypeService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Time Off Types cached"));
            }
            this.event.publishEvent(new LogImportEvent(this, "Caching finished"));

            //Loop through every record in the csv
            while (reader.readRecord()) {
                try {
                    //Create a new calendar item which we are going to insert
                    CalItemShortInformation item = new CalItemShortInformation();
                    //We have to set saved status to 1 (granted)
                    item.setSavedStatus(1);

                    //Hashcode is based on the required columns Employee ID, First Name and Last Name (REQUIRED FIELDS)
                    //Get the agent that matches the given hashcode
                    CfgAgent agent;
                    CfgAgentDTO agentDTO = agentService.getAgentByIdentifier(wfmObjectHolder, reader);
                    if (agentDTO == null) {
                        throw new IllegalArgumentException("Agent " + reader.get(CalendarItemHeaders.FIRST_NAME) + " " + reader.get(CalendarItemHeaders.LAST_NAME) + " " + reader.get(CalendarItemHeaders.EMPLOYEE_ID) + " does not exist in database");
                    }
                    agent = agentDTO.getDataSOAP();

                    if (siteCache.get(reader.get(CalendarItemHeaders.SITE)) == null) {
                        throw new IllegalArgumentException("Site " + reader.get(CalendarItemHeaders.SITE) + " does not exist in database");
                    }
                    item.setAgentID(agent.getGswAgentId());
                    //Set start date (REQUIRED FIELD)
                    String value = reader.get(CalendarItemHeaders.DATE);
                    double startDate = new OleDateTime(Integer.parseInt(value.substring(6)), Integer.parseInt(value.substring(3, 5)), Integer.parseInt(value.substring(0, 2))).asDouble();
                    item.setDate(startDate);
                    //Set start time (OPTIONAL FIELD)
                    if (reader.get(CalendarItemHeaders.START_TIME) != null && !reader.get(CalendarItemHeaders.START_TIME).equals("")) {
                        if (reader.get(CalendarItemHeaders.START_TIME).length() > 1) {
                            String startTimeString = reader.get(CalendarItemHeaders.START_TIME);
                            String hour = startTimeString.substring(0, 2);
                            String minute = startTimeString.substring(3);
                            int hourInt = 0;
                            int minuteInt = 0;
                            if (hour.charAt(0) == '0') {
                                if (!(hour.charAt(1) == '0'))
                                    hourInt = Integer.valueOf(hour.charAt(1) + "");
                            } else {
                                hourInt = Integer.valueOf(hour);
                            }
                            if (minute.charAt(0) == '0') {
                                if (!(minute.charAt(1) == '0'))
                                    minuteInt = Integer.valueOf(minute.charAt(1) + "");
                            } else {
                                minuteInt = Integer.valueOf(minute);
                            }
                            double startTime = new OleDateTime(hourInt, minuteInt).asDouble();
                            item.setStartTime(startTime);
                        }
                    }
                    //Set end time (OPTIONAL FIELD)
                    if (reader.get(CalendarItemHeaders.END_TIME) != null && !reader.get(CalendarItemHeaders.END_TIME).equals("")) {
                        if (reader.get(CalendarItemHeaders.END_TIME).length() > 1) {
                            String endTimeString = reader.get(CalendarItemHeaders.END_TIME);
                            String hour = endTimeString.substring(0, 2);
                            String minute = endTimeString.substring(3);
                            int hourInt = 0;
                            int minuteInt = 0;
                            if (hour.charAt(0) == '0') {
                                if (!(hour.charAt(1) == '0'))
                                    hourInt = Integer.valueOf(hour.charAt(1) + "");
                            } else {
                                hourInt = Integer.valueOf(hour);
                            }
                            if (minute.charAt(0) == '0') {
                                if (!(minute.charAt(1) == '0'))
                                    minuteInt = Integer.valueOf(minute.charAt(1) + "");
                            } else {
                                minuteInt = Integer.valueOf(minute);
                            }
                            double endTime = new OleDateTime(hourInt, minuteInt).asDouble();
                            item.setEndTime(endTime);
                        }
                    }
                    //Set paid time (OPTIONAL FIELD)
                    if (reader.get(CalendarItemHeaders.PAID_TIME) != null && !reader.get(CalendarItemHeaders.PAID_TIME).equals("")) {
                        item.setPaidTime(Integer.parseInt(reader.get(CalendarItemHeaders.PAID_TIME)));
                    }
                    //Set item type (REQUIRED FIELD)
                    if (type == 1) {
                        item.setItemTypeID(this.getExceptionTypeIDByName(agent.getWmSiteId()));
                    } else {
                        item.setItemTypeID(this.getTimeOffTypeIDByName(agent.getWmSiteId()));
                    }
                    //Set comment (OPTIONAL FIELD)
                    if (reader.get(CalendarItemHeaders.COMMENT) != null && !reader.get(CalendarItemHeaders.COMMENT).equals("")) {
                        item.setComment(reader.get(CalendarItemHeaders.COMMENT));
                    }
                    //Set full day (REQUIRED FIELD)
                    //if full day is 1, item is 0 (full-day exception), else item is 1 (part-day exception)
                    if (type == 1) {
                        if (Integer.parseInt(reader.get(CalendarItemHeaders.FULL_DAY)) == 1) {
                            item.setFullDay(1);
                            item.setItem(0);
                        }
                        if (Integer.parseInt(reader.get(CalendarItemHeaders.FULL_DAY)) == 0) {
                            item.setFullDay(0);
                            item.setItem(1);
                        }
                    } else {
                        item.setFullDay(Integer.parseInt(reader.get(CalendarItemHeaders.FULL_DAY)));
                        item.setItem(3);
                    }

                    List<CalItemShortInformation> items = new ArrayList<>();
                    items.add(item);
                    //Set site id
                    CalValidationHolder res = calService.insertCalendarItems(ECalAccessLevels.CAL_ACCESSLEVEL_ADMIN, agent.getWmSiteId(), 0, items, true);
                    checkResponse(res);
                    this.event.publishEvent(new LogImportEvent(this, "SUCCESS (row " + (reader.getCurrentRecord() + 2) + ") - Agent " + agent.getGswFirstName() + " " + agent.getGswLastName() + " with ID " + agent.getGswEmployeeId()));
                    //} catch (IllegalArgumentException | IllegalStateException | ServerSOAPFaultException e) {
                } catch (Exception e) {
                    this.event.publishEvent(new LogImportEvent(this, "FAIL (row " + (reader.getCurrentRecord() + 2) + ") - " + e.getMessage()));
                    e.printStackTrace();
                    if (PocApplication.debugMode) {
                        for (int i = 0; i < e.getStackTrace().length; i++) {
                            this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                        }
                    }
                }
            }
            //If the headers don't match, throw exception
        } catch (Exception e) {
            this.event.publishEvent(new LogImportEvent(this, "ERROR - (row " + (reader.getCurrentRecord() + 2) + ")" + e.getMessage()));
            if (PocApplication.debugMode) {
                for (int i = 0; i < e.getStackTrace().length; i++) {
                    this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                }
            }
            success = false;
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
            clearCache();
            this.event.publishEvent(new LogImportEvent(this, "Import ended in " + ((Calendar.getInstance().getTimeInMillis() - start) / 1000) + " seconds"));
            this.event.publishEvent(new StoppedImportEvent(this, "Done!"));
        }

        return success;
    }

    private int getExceptionTypeIDByName(int siteID) throws Exception {
        CfgExceptionType exceptionType;
        String exceptionTypeName = this.reader.get(CalendarItemHeaders.ITEM_TYPE);
        int exceptionTypeID = 0;
        label:
        for (BaseDTO baseDTO : exceptionCache.get(siteID)) {
            exceptionType = (CfgExceptionType) baseDTO.getDataSOAP();
            for (Integer site : exceptionType.getWmSiteId()) {
                if (site == siteID && exceptionType.getWmName().equals(exceptionTypeName)) {
                    exceptionTypeID = exceptionType.getWmExceptionTypeId();
                    break label;
                }
            }
        }
        if (exceptionTypeID == 0) {
            throw new IllegalArgumentException("There is no exception type with name " + exceptionTypeName);
        }
        return exceptionTypeID;
    }

    private int getTimeOffTypeIDByName(int siteID) throws Exception {
        CfgTimeOffType timeOffType;
        String timeOffTypeName = this.reader.get(CalendarItemHeaders.ITEM_TYPE);
        if (timeOffTypeName.equals("Vacation")) return 0;
        int timeOffTypeID = 0;
        label:
        for (BaseDTO baseDTO : timeOffTypeCache.get(siteID)) {
            timeOffType = (CfgTimeOffType) baseDTO.getDataSOAP();
            for (Integer site : timeOffType.getWmSiteId()) {
                if (site == siteID && timeOffType.getWmName().equals(timeOffTypeName)) {
                    timeOffTypeID = timeOffType.getWmTimeOffTypeId();
                    break label;
                }
            }
        }
        if (timeOffTypeID == 0) {
            throw new IllegalArgumentException("There is no time-off type with name " + timeOffTypeName);
        }
        return timeOffTypeID;
    }

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        CalData calData = this.getCalData(wfmObjectHolder);

        HashMap<Integer, List<CalItemInformationDTO>> duplicates = new HashMap<>();
        HashMap<Integer, BaseDTO> list = new HashMap<>();

        //TODO: CURRENT HASH CODE METHOD SUBJECT TO CHANGE, AT THE MOMENT IT IS BASED ON DATE AND ITEM.
        //IN THE MAP duplicates, WE INSERT EVERY CALENDAR ITEM WITH THE SAME HASHCODE INTO THE SAME LIST
        //THEN, WE SORT THE LIST FOR EACH HASH CODE (BY PRIORITY), AND INSERT THE HIGHEST PRIORITY CALENDAR ITEM
        //INTO THE list, THAT WE ARE GONNA RETURN
        for (CalItemInformation wfmEntity : calData.getInformation()) {
            if (wfmEntity.getItem() > 6) {
                continue;
            }
            CalItemInformationDTO dto = (CalItemInformationDTO) FactoryDTO.create(wfmEntity);

            //IF duplicates DOES NOT HAVE A LIST FOR THE CURRENT HASHCODE, CREATE ONE
            if (!duplicates.containsKey(dto.hashCode())) {
                duplicates.put(dto.hashCode(), new ArrayList<>());
            }

            //ADD THE DTO TO THE CORRESPONDING LIST
            duplicates.get(dto.hashCode()).add(dto);
        }

        //LOOP THROUGH THE LIST OF EACH ENTRY, PICK THE CALENDAR ITEM WITH THE HIGHEST PRIORITY, AND ADD IT TO THE list
        for (Map.Entry entry : duplicates.entrySet()) {
            //BUFFER VARIABLE TO HOLD THE CURRENT HIGHEST PRIORITY ITEM, USED FOR THE LOOP BELOW
            CalItemInformationDTO highestPrio = null;

            //IF THE DTO IS TIME-BASED, AND IT HAS EQUAL PRIORITY WITH highestPrio (0), WE HAVE TO INSERT THEM BOTH.
            //WE ADD THEM TO THE HASHMAP WITH HASHCODE BASED ON ???
            //THIS LIST HOLDS ALL SUCH ITEMS, WHICH ARE TO BE INSERTED IF IN THE END, highestPrio IS TIME-BASED.
            List<CalItemInformationDTO> timeBasedItems = new ArrayList<>();

            for (CalItemInformationDTO ciiDto : (ArrayList<CalItemInformationDTO>) entry.getValue()) {
                if (highestPrio == null) {
                    highestPrio = ciiDto;
                    continue;
                }
                switch (ciiDto.compareTo(highestPrio)) {
                    case 1: {
                        highestPrio = ciiDto;
                        //THIS IS USED IN CASE highestPrio was 1 (LOWER PRIO THAN 3+1), AND NOW ANOTHER TIME-BASED ITEM IS
                        //HIGHER. IN THAT SITUATION, WE HAVE TO EMPTY THE LIST FIRST.
                        if (highestPrio.getDataSOAP().getItem() == 3 && highestPrio.getDataSOAP().getFullDay() == 1) {
                            timeBasedItems.clear();
                        }
                        break;
                    }
                    case 0: {
                        //IF highestPrio IS TIME-BASED, WE CHECK IF OUR ITEM IS COMPATIBLE WITH IT (BASED ON TIME), AND
                        //ADD IT TO THE timeBasedItems LIST IF IT IS
                        if ((highestPrio.getDataSOAP().getItem() == 3 && highestPrio.getDataSOAP().getFullDay() == 1)
                                || highestPrio.getDataSOAP().getItem() == 1) {
                            timeBasedItems.add(ciiDto);
                        }
                        break;
                    }
                    case -1: {
                        break;
                    }
                }
            }
            //NOW WE CHECK IF highestPrio is TIME-BASED. IF IT IS, WE HAVE TO ADD ALL OF THE ITEMS TO THE list
            if (highestPrio != null) {
                if ((highestPrio.getDataSOAP().getItem() == 3 && highestPrio.getDataSOAP().getFullDay() == 1)
                        || highestPrio.getDataSOAP().getItem() == 1) {
                    for (CalItemInformationDTO item : timeBasedItems) {
                        list.put(new HashCodeBuilder().append(item.hashCode()).append(item.getDataSOAP().getStartTime()).append(item.getDataSOAP().getEndTime()).toHashCode(), item);
                    }
                    list.put(new HashCodeBuilder().append(highestPrio.hashCode()).append(highestPrio.getDataSOAP().getStartTime()).append(highestPrio.getDataSOAP().getEndTime()).toHashCode(), highestPrio);
                } else {
                    list.put(highestPrio.hashCode(), highestPrio);
                }
            }
        }
        return list;
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {
        this.event.publishEvent(new LogEvent(this, "the target database has an object for the same agent at the same date, but with lower priority, inserting the new object with higher priority"));
        this.insert(syncDTO);
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {

        CalItemInformation sourceObject = (CalItemInformation) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;

        boolean doAction = false;

        //we should to insert
        switch (status) {
            case UPDATE:
                if (((CalItemInformationDTO) source.getDto()).compareTo(((CalItemInformationDTO) target.getDto())) == 1) {
                    doAction = true;
                    List<Integer> sourceSitesIDs = new ArrayList<>();
                    sourceSitesIDs.add(sourceObject.getSiteId());

                    sourceObject.setSiteId(siteService.getTargetSiteIDsBySourceSiteIDs(sourceSitesIDs).get(0));
                }
                break;
            case INSERT:
                doAction = true;
                List<Integer> sourceSiteIDs = new ArrayList<>();
                sourceSiteIDs.add(sourceObject.getSiteId());

                sourceObject.setSiteId(siteService.getTargetSiteIDsBySourceSiteIDs(sourceSiteIDs).get(0));
                break;
        }

        if (doAction) {
            CalItemInformationDTO calItemInformationDTO = new CalItemInformationDTO();
            calItemInformationDTO.setDataSOAP(sourceObject);

            try {
                mergedObjectHolder = target.setDto(calItemInformationDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mergedObjectHolder;
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        CalItemInformation calItemInformation = (CalItemInformation) syncDTO.getMergedObject().getDto().getDataSOAP();
        HashMap<Integer, Integer> syncedAgents = agentService.getLinkID();

        if (!syncedAgents.containsKey(calItemInformation.getAgentId())) {
            throw new IllegalStateException("the agent is missing in the target environment");
        }

        WFMCalendarService851Soap calService = super.getCalendarService(syncDTO.getMergedObject());

        List<CalItemShortInformation> items = this.convertLongToShort(calItemInformation);

        Integer targetSiteID = ((CalItemInformationDTO) syncDTO.getMergedObject().getDto()).getDataSOAP().getSiteId();

        CalValidationHolder res = calService.insertCalendarItems(ECalAccessLevels.CAL_ACCESSLEVEL_ADMIN, targetSiteID, 0, items, false);

        checkResponse(res);
    }

    private List<CalItemShortInformation> convertLongToShort(CalItemInformation longItem) {
        List<CalItemShortInformation> results = new ArrayList<>();
        CalItemShortInformation shortItem = new CalItemShortInformation();
        HashMap<Integer, Integer> syncedAgents = agentService.getLinkID();
        shortItem.setAgentID(syncedAgents.get(longItem.getAgentId()));
        shortItem.setComment(longItem.getComment());
        shortItem.setDate(longItem.getDate());
        shortItem.setEndTime(longItem.getEndTime());
        shortItem.setFullDay(longItem.getFullDay());
        shortItem.setItem(longItem.getItem());
        shortItem.setItemID(longItem.getItemId());
        shortItem.setItemTypeID(longItem.getItemType());
        shortItem.setPaidTime(longItem.getPaidTime());
        shortItem.setSavedStatus(longItem.getSavedStatus());
        shortItem.setStartTime(longItem.getStartTime());
        shortItem.setTimestamp(longItem.getTimestamp());

        results.add(shortItem);

        return results;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CalItemInformation wfmObject = (CalItemInformation) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getItemName();
    }

    @Override
    public String getObjectServiceName() {
        return "Calendar Item";
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {

    }
}
