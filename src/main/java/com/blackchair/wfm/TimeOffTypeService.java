package com.blackchair.wfm;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class TimeOffTypeService extends WFMConfigService {
    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgTimeOffTypeHolder cfgTimeOffTypeHolder = this.getCfgHolder(wfmObjectHolder);
        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgTimeOffType wfmEntity : cfgTimeOffTypeHolder.getObjectArray()) {
            CfgTimeOffTypeDTO dto = (CfgTimeOffTypeDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgTimeOffTypeHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgTimeOffTypeFilter cfgTimeOffTypeFilter = new CfgTimeOffTypeFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgTimeOffTypeFilter.withWmBUId(filterDTO.getBuId());
            }

            if (filterDTO.getSiteId() != null) {
                cfgTimeOffTypeFilter.withWmSiteId(filterDTO.getSiteId());
            }
        }

        return super.getService(wfmObjectHolder).getTimeOffType(cfgTimeOffTypeFilter, cfgAgentFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Team.CFG_TEAM_SORT_NAME).withAscending(true),
                new CfgTimeOffTypeDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        return null;
    }

    @Override
    public String getObjectServiceName() {
        return null;
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {

    }
}
