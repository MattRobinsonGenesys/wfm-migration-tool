package com.blackchair.wfm;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ContractService extends WFMConfigService {
    @Autowired
    protected SiteService siteService;

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgContractHolder cfgContractHolder = getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgContract wfmEntity : cfgContractHolder.getObjectArray()) {
            CfgContractDTO dto = (CfgContractDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgContractHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgContractFilter cfgContractFilter = new CfgContractFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getId() != null) {
                cfgContractFilter.withWmContractId(filterDTO.getId());
            }

            if (filterDTO.getIds() != null) {
                cfgContractFilter.withWmContractId(filterDTO.getIds());
            }

            if (filterDTO.getBuId() != null) {
                cfgContractFilter.withWmBUId(filterDTO.getBuId());
            }

            if (filterDTO.getSiteId() != null) {
                cfgContractFilter.withWmSiteId(filterDTO.getSiteId());
            }

            if (filterDTO.getShiftId() != null) {
                cfgContractFilter.withWmShiftId(filterDTO.getShiftId());
            }
        }

        return super.getService(wfmObjectHolder).getContract(cfgContractFilter, cfgAgentFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Contract.CFG_CONTRACT_SORT_NAME).withAscending(true),
                new CfgContractDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    List<CfgAgentContract> syncAgentContracts(List<CfgAgentContract> sourceAgentContracts) {
        for (CfgAgentContract sourceAgentContract : sourceAgentContracts) {
            if (this.linkID.containsKey(sourceAgentContract.getWmContractId())) {
                sourceAgentContract.setWmContractId(this.linkID.get(sourceAgentContract.getWmContractId()));
            }
        }

        return sourceAgentContracts;
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        CfgContract sourceObject = (CfgContract) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;

        boolean doAction = false;

        //we should to insert
        switch (status) {
            case UPDATE:
                CfgContract targetObject = (CfgContract) target.getDto().getDataSOAP();
                this.linkID.put(sourceObject.getWmContractId(), targetObject.getWmContractId());

                sourceObject.setWmContractId(targetObject.getWmContractId());
                sourceObject.setWmSiteIdNSizeIs(targetObject.getWmSiteIdNSizeIs());
                sourceObject.setWmSiteId(targetObject.getWmSiteId());
                sourceObject.setWmContractShifts(null);

                for (int i = 0; i < sourceObject.getWmAvailabilities().size(); i++) {
                    if (sourceObject.getWmAvailabilities().get(i).getWmDuration() != targetObject.getWmAvailabilities().get(i).getWmDuration() ||
                            sourceObject.getWmAvailabilities().get(i).getWmStartTime() != targetObject.getWmAvailabilities().get(i).getWmStartTime()) {
                        doAction = true;
                        break;
                    }
                }

                if (sourceObject.getWmCompensationOver() != targetObject.getWmCompensationOver()) {
                    doAction = true;
                }

                if (sourceObject.getWmCompensationOver2() != targetObject.getWmCompensationOver2()) {
                    doAction = true;
                }

                for (int i = 0; i < sourceObject.getWmContractMonthHours().size(); i++) {
                    if (sourceObject.getWmContractMonthHours().get(i).getWmMinutesMax() != targetObject.getWmContractMonthHours().get(i).getWmMinutesMax() ||
                            sourceObject.getWmContractMonthHours().get(i).getWmMinutesMin() != targetObject.getWmContractMonthHours().get(i).getWmMinutesMin() ||
                            sourceObject.getWmContractMonthHours().get(i).getWmMinutesStd() != targetObject.getWmContractMonthHours().get(i).getWmMinutesStd()) {
                        doAction = true;
                        break;
                    }
                }

                if (sourceObject.getWmDailyMinutesMax() != targetObject.getWmDailyMinutesMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmDailyMinutesMin() != targetObject.getWmDailyMinutesMin()) {
                    doAction = true;
                }

                if (sourceObject.getWmDailyMinutesOver() != targetObject.getWmDailyMinutesOver()) {
                    doAction = true;
                }

                if (sourceObject.getWmDailyMinutesOver2() != targetObject.getWmDailyMinutesOver2()) {
                    doAction = true;
                }

                if (sourceObject.getWmDailyMinutesStd() != targetObject.getWmDailyMinutesStd()) {
                    doAction = true;
                }

                if (sourceObject.getWmDayoffEx() != targetObject.getWmDayoffEx()) {
                    doAction = true;
                }

                if (sourceObject.getWmDayoffMax() != targetObject.getWmDayoffMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmDayoffMin() != targetObject.getWmDayoffMin()) {
                    doAction = true;
                }

                if (sourceObject.getWmIcon() != targetObject.getWmIcon()) {
                    doAction = true;
                }

                if (sourceObject.getWmMaxBetweenDaysoff() != targetObject.getWmMaxBetweenDaysoff()) {
                    doAction = true;
                }

                if (sourceObject.getWmMaxConsecWorkSundays() != targetObject.getWmMaxConsecWorkSundays()) {
                    doAction = true;
                }

                if (sourceObject.getWmMaxConsecWorkWeekends() != targetObject.getWmMaxConsecWorkWeekends()) {
                    doAction = true;
                }

                if (sourceObject.getWmMinConsecTimeoff() != targetObject.getWmMinConsecTimeoff()) {
                    doAction = true;
                }

                if (sourceObject.getWmMinTimeBetweenDays() != targetObject.getWmMinTimeBetweenDays()) {
                    doAction = true;
                }

                if (sourceObject.getWmMinTimeBetweenStarts() != targetObject.getWmMinTimeBetweenStarts()) {
                    doAction = true;
                }

                if (sourceObject.getWmMonthlyMinutesMax() != targetObject.getWmMonthlyMinutesMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmMonthlyMinutesMin() != targetObject.getWmMonthlyMinutesMin()) {
                    doAction = true;
                }

                if (sourceObject.getWmMonthlyMinutesOver() != targetObject.getWmMonthlyMinutesOver()) {
                    doAction = true;
                }

                if (sourceObject.getWmMonthlyMinutesOver2() != targetObject.getWmMonthlyMinutesOver2()) {
                    doAction = true;
                }

                if (sourceObject.getWmMonthlyMinutesStd() != targetObject.getWmMonthlyMinutesStd()) {
                    doAction = true;
                }

                if (sourceObject.getWmSatOffMax() != targetObject.getWmSatOffMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmSatOffMin() != targetObject.getWmSatOffMin()) {
                    doAction = true;
                }

                if (sourceObject.getWmSunOffMax() != targetObject.getWmSunOffMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmSunOffMin() != targetObject.getWmSunOffMin()) {
                    doAction = true;
                }

                for (int i = 0; i < sourceObject.getWmSynch().size(); i++) {
                    if (sourceObject.getWmSynch().get(i) != targetObject.getWmSynch().get(i)) {
                        doAction = true;
                        break;
                    }
                }

                if (sourceObject.getWmSynchThreshold() != targetObject.getWmSynchThreshold()) {
                    doAction = true;
                }

                if (sourceObject.getWmSynchType() != targetObject.getWmSynchType()) {
                    doAction = true;
                }

                if (sourceObject.getWmSynchWeeks() != targetObject.getWmSynchWeeks()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeekendFirstDay() != targetObject.getWmWeekendFirstDay()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeekendOffMax() != targetObject.getWmWeekendOffMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeekendOffMin() != targetObject.getWmWeekendOffMin()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeeklyDaysMax() != targetObject.getWmWeeklyDaysMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeeklyDaysMin() != targetObject.getWmWeeklyDaysMin()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeeklyMinutesMax() != targetObject.getWmWeeklyMinutesMax()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeeklyMinutesMin() != targetObject.getWmWeeklyMinutesMin()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeeklyMinutesOver() != targetObject.getWmWeeklyMinutesOver()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeeklyMinutesOver2() != targetObject.getWmWeeklyMinutesOver2()) {
                    doAction = true;
                }

                if (sourceObject.getWmWeeklyMinutesStd() != targetObject.getWmWeeklyMinutesStd()) {
                    doAction = true;
                }

                break;
            case INSERT:

                List<Integer> sourceSiteIDs = sourceObject.getWmSiteId();
                List<Integer> targetLinkSiteId = siteService.getTargetSiteIDsBySourceSiteIDs(sourceSiteIDs);

                if (!targetLinkSiteId.isEmpty()) {
                    doAction = true;
                    sourceObject.setWmSiteId(targetLinkSiteId);
                    sourceObject.setWmContractShifts(null);
                }

                break;
        }

        if (doAction) {
            CfgContractDTO cfgContractDTO = new CfgContractDTO();
            cfgContractDTO.setDataSOAP(sourceObject);

            try {
                mergedObjectHolder = target.setDto(cfgContractDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mergedObjectHolder;
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgContract sourceObject = (CfgContract) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgContract targetObject = (CfgContract) listTarget.get(hashCode).getDataSOAP();
                targetSiteIDs.put(sourceObject.getWmContractId(), targetObject.getWmContractId());
            }
        }

        this.linkID = targetSiteIDs;
    }

    List<CfgContractShift> syncShiftContracts(List<CfgContractShift> sourceShiftContracts) {
        for (CfgContractShift sourceShiftContract : sourceShiftContracts) {
            if (this.linkID.containsKey(sourceShiftContract.getWmContractId())) {
                sourceShiftContract.setWmContractId(this.linkID.get(sourceShiftContract.getWmContractId()));
            }
        }

        return sourceShiftContracts;
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.updateContract((CfgContract) syncDTO.getMergedObject().getDto().getDataSOAP(), false, false);
        checkResponse(res);
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.insertContract((CfgContract) syncDTO.getMergedObject().getDto().getDataSOAP(), true);

        checkResponse(res);

        CfgContract sourceObject = (CfgContract) syncDTO.getSource().getDto().getDataSOAP();
        this.linkID.put(sourceObject.getWmContractId(), res.getObjectID());
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgContract wfmObject = (CfgContract) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "Contract";
    }
}
