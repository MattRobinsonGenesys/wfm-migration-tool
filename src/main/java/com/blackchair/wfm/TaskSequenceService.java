package com.blackchair.wfm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blackchair.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;
import com.genesyslab.wfm8.API.service.config852.CfgSortMode;
import com.genesyslab.wfm8.API.service.config852.CfgTaskSequence;
import com.genesyslab.wfm8.API.service.config852.CfgTaskSequenceDetails;
import com.genesyslab.wfm8.API.service.config852.CfgTaskSequenceFilter;
import com.genesyslab.wfm8.API.service.config852.CfgTaskSequenceHolder;
import com.genesyslab.wfm8.API.service.config852.CfgTaskSequenceItem;
import com.genesyslab.wfm8.API.service.config852.CfgValidationHolder;
import com.genesyslab.wfm8.API.service.config852.ECfgInfoType;
import com.genesyslab.wfm8.API.service.config852.ECfgSortMode;
import com.genesyslab.wfm8.API.service.config852.WFMConfigService852Soap;


@Service
public class TaskSequenceService extends WFMConfigService {
    @Autowired
    protected ActivitySetService activitySetService;

    @Autowired
    protected SiteService siteService;

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgTaskSequence wfmEntity : this.getCfgHolder(wfmObjectHolder).getObjectArray()) {
            CfgTaskSequenceDTO dto = (CfgTaskSequenceDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgTaskSequenceHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgTaskSequenceFilter cfgTaskSequenceFilter = new CfgTaskSequenceFilter();
        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgTaskSequenceFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getSiteId() != null) {
                cfgTaskSequenceFilter.withWmSiteId(filterDTO.getSiteId());
            }
            if (filterDTO.getShiftId() != null) {
                cfgTaskSequenceFilter.withWmShiftId(filterDTO.getShiftId());
            }

            if (filterDTO.getIds() != null) {
                cfgTaskSequenceFilter.withWmTaskSequenceId(filterDTO.getIds());
            }
        }

        return super.getService(wfmObjectHolder).getTaskSequence(cfgTaskSequenceFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.TaskSequence.CFG_TASK_SEQUENCE_SORT_NAME).withAscending(true),
                new CfgTaskSequenceDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.updateTaskSequence((CfgTaskSequence) syncDTO.getMergedObject().getDto().getDataSOAP(), false, false);

        checkResponse(res);
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());

        CfgValidationHolder res = cfgService.insertTaskSequence((CfgTaskSequence) syncDTO.getMergedObject().getDto().getDataSOAP(), true);

        checkResponse(res);

        CfgTaskSequence sourceSOAP = (CfgTaskSequence) syncDTO.getSource().getDto().getDataSOAP();
        this.linkID.put(sourceSOAP.getWmTaskSequenceId(), res.getObjectID());
    }

    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {

        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgTaskSequence sourceObject = (CfgTaskSequence) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgTaskSequence targetObject = (CfgTaskSequence) listTarget.get(hashCode).getDataSOAP();
                targetSiteIDs.put(sourceObject.getWmTaskSequenceId(), targetObject.getWmTaskSequenceId());
            }
        }

        this.linkID = targetSiteIDs;
    }

    List<Integer> syncShiftTaskSequences(List<Integer> sourceShiftTaskSequences) {
        List<Integer> result = new ArrayList<>();

        for (Integer sourceShiftTaskSequence : sourceShiftTaskSequences) {
            if (this.linkID.containsKey(sourceShiftTaskSequence)) {
                result.add(this.linkID.get(sourceShiftTaskSequence));
            }
        }

        return result;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgTaskSequence wfmObject = (CfgTaskSequence) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "TaskSequence";
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {

        CfgTaskSequence sourceObject = (CfgTaskSequence) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;
        boolean doAction = false;

        switch (status) {
            case UPDATE:
                CfgTaskSequence targetObject = (CfgTaskSequence) target.getDto().getDataSOAP();

                this.linkID.put(sourceObject.getWmTaskSequenceId(), targetObject.getWmTaskSequenceId());

                sourceObject.setWmTaskSequenceId(targetObject.getWmTaskSequenceId());
                sourceObject.setWmSiteIdNSizeIs(targetObject.getWmSiteIdNSizeIs());
                sourceObject.setWmSiteId(targetObject.getWmSiteId());

                if (sourceObject.getWmTaskSequenceItems().size() != targetObject.getWmTaskSequenceItems().size()) {
                    doAction = true;
                }
                break;
            case INSERT:
                doAction = true;
                List<Integer> sourceSiteIDs = sourceObject.getWmSiteId();

                sourceObject.setWmSiteId(siteService.getTargetSiteIDsBySourceSiteIDs(sourceSiteIDs));

                break;
        }

        if (!sourceObject.getWmTaskSequenceItems().isEmpty() && doAction) {
            List<CfgTaskSequenceItem> cfgTaskSequenceItems = new ArrayList<>();

            //SYNC ACTIVITY SETS
            HashMap<Integer, Integer> sourceToTargetActivitySetIDs = activitySetService.getLinkID();

            for (CfgTaskSequenceItem sourceTsi : sourceObject.getWmTaskSequenceItems()) {
                if (sourceToTargetActivitySetIDs.containsKey(sourceTsi.getWmActivitySetId())) {
                    sourceTsi.setWmActivitySetId(sourceToTargetActivitySetIDs.get(sourceTsi.getWmActivitySetId()));
                    cfgTaskSequenceItems.add(sourceTsi);
                }
            }

            if (!cfgTaskSequenceItems.isEmpty()) {
                sourceObject.setWmTaskSequenceItems(cfgTaskSequenceItems);

                CfgTaskSequenceDTO objectDTO = new CfgTaskSequenceDTO();
                objectDTO.setDataSOAP(sourceObject);

                try {
                    mergedObjectHolder = target.setDto(objectDTO);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return mergedObjectHolder;
    }
}
