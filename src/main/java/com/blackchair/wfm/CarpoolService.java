package com.blackchair.wfm;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CarpoolService extends WFMConfigService {
    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgCarpoolHolder cfgCarpoolHolder = this.getCfgHolder(wfmObjectHolder);
        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgCarpool wfmEntity : cfgCarpoolHolder.getObjectArray()) {
            CfgCarpoolDTO dto = (CfgCarpoolDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgCarpoolHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgCarpoolFilter cfgCarpoolFilter = new CfgCarpoolFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgCarpoolFilter.withWmBUId(filterDTO.getBuId());
            }

            if (filterDTO.getSiteId() != null) {
                cfgCarpoolFilter.withWmSiteId(filterDTO.getSiteId());
            }
        }

        return super.getService(wfmObjectHolder).getCarpool(cfgCarpoolFilter, cfgAgentFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Carpool.CFG_CARPOOL_SORT_NAME).withAscending(true),
                new CfgCarpoolDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        return null;
    }

    @Override
    public String getObjectServiceName() {
        return null;
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgCarpool sourceObject = (CfgCarpool) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgCarpool targetObject = (CfgCarpool) listTarget.get(hashCode).getDataSOAP();
                this.linkID.put(sourceObject.getWmCarpoolId(), targetObject.getWmCarpoolId());
            }
        }
    }
}
