package com.blackchair.wfm;

import java.util.HashMap;
import java.util.Map;

import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.event.LogEvent;


@Service
public class ActivitySetService extends WFMConfigService {

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgActivitySetHolder cfgActivitySetHolder = this.getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgActivitySet wfmEntity : cfgActivitySetHolder.getObjectArray()) {

            CfgActivitySetDTO dto = (CfgActivitySetDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);

        }

        return list;
    }

    public CfgActivitySetHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgActivitySetFilter cfgActivitySetFilter = new CfgActivitySetFilter();
        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgActivitySetFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getSiteId() != null) {
                cfgActivitySetFilter.withWmSiteId(filterDTO.getSiteId());
            }
        }


        return super.getService(wfmObjectHolder).getActivitySet(cfgActivitySetFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.ActivitySet.CFG_ACTIVITY_SET_SORT_NAME).withAscending(true),
                new CfgActivitySetDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgActivitySet sourceObject = (CfgActivitySet) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgActivitySet targetObject = (CfgActivitySet) listTarget.get(hashCode).getDataSOAP();
                this.linkID.put(sourceObject.getWmActivitySetId(), targetObject.getWmActivitySetId());
            } else {
                this.event.publishEvent(new LogEvent(this, "Missing Activity Set with name " + sourceObject.getWmName() + " does not exist in target database"));
            }
        }
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgActivitySet wfmObject = (CfgActivitySet) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "ActivitySet";
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }
}
