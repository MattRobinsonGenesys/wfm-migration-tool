package com.blackchair.wfm;

import java.util.HashMap;
import java.util.List;

import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;


@Service
public class BreakService extends WFMConfigService {
    @Autowired
    private SiteService siteService;

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder source) throws Exception {

        CfgBreakHolder breakHolder = this.getCfgHolder(source);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgBreak cfgBreak : breakHolder.getObjectArray()) {
            CfgBreakDTO dto = (CfgBreakDTO) FactoryDTO.create(cfgBreak);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgBreakHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgBreakFilter cfgBreakFilter = new CfgBreakFilter();
        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgBreakFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getIds() != null) {
                cfgBreakFilter.withWmBreakId(filterDTO.getIds());
            }
            if (filterDTO.getSiteId() != null) {
                cfgBreakFilter.withWmSiteId(filterDTO.getSiteId());
            }
            if (filterDTO.getShiftId() != null) {
                cfgBreakFilter.withWmShiftId(filterDTO.getShiftId());
            }
        }

        return super.getService(wfmObjectHolder).getBreak(cfgBreakFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Break.CFG_BREAK_SORT_NAME).withAscending(true),
                new CfgBreakDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.updateBreak((CfgBreak) syncDTO.getMergedObject().getDto().getDataSOAP(), false, false);

        checkResponse(res);
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgService.insertBreak((CfgBreak) syncDTO.getMergedObject().getDto().getDataSOAP(), true);

        checkResponse(res);

        CfgBreak sourceObject = (CfgBreak) syncDTO.getSource().getDto().getDataSOAP();
        this.linkID.put(sourceObject.getWmBreakId(), res.getObjectID());
    }

    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {

        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        CfgBreakHolder sourceHolder = this.getCfgHolder(source);
        CfgBreakHolder targetHolder = this.getCfgHolder(target);

        for (CfgBreak sourceObject : sourceHolder.getObjectArray()) {
            for (CfgBreak targetObject : targetHolder.getObjectArray()) {
                if (sourceObject.getWmName().equalsIgnoreCase(targetObject.getWmName())) {
                    targetSiteIDs.put(sourceObject.getWmBreakId(), targetObject.getWmBreakId());
                    break;
                }
            }
        }

        this.linkID = targetSiteIDs;
    }

    List<CfgShiftBreak> syncShiftBreaks(List<CfgShiftBreak> sourceShiftBreaks) {
        for (CfgShiftBreak sourceShiftBreak : sourceShiftBreaks) {
            if (this.linkID.containsKey(sourceShiftBreak.getWmBreakId())) {
                sourceShiftBreak.setWmBreakId(this.linkID.get(sourceShiftBreak.getWmBreakId()));
            }
        }

        return sourceShiftBreaks;
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgBreak wfmObject = (CfgBreak) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "Breaks";
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {

        CfgBreak sourceObject = (CfgBreak) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;

        boolean doAction = false;

        switch (status) {
            case UPDATE:
                CfgBreak targetObject = (CfgBreak) target.getDto().getDataSOAP();

                this.linkID.put(sourceObject.getWmBreakId(), targetObject.getWmBreakId());

                sourceObject.setWmBreakId(targetObject.getWmBreakId());
                sourceObject.setWmSiteIdNSizeIs(targetObject.getWmSiteIdNSizeIs());
                sourceObject.setWmSiteId(targetObject.getWmSiteId());

                if (sourceObject.getWmAnchor() != targetObject.getWmAnchor()) {
                    doAction = true;
                }

                if (sourceObject.getWmDuration() != targetObject.getWmDuration()) {
                    doAction = true;
                }

                if (sourceObject.getWmMaxDistance() != targetObject.getWmMaxDistance()) {
                    doAction = true;
                }

                if (sourceObject.getWmMaxLengthBefore() != targetObject.getWmMaxLengthBefore()) {
                    doAction = true;
                }

                if (sourceObject.getWmMinLengthBefore() != targetObject.getWmMinLengthBefore()) {
                    doAction = true;
                }

                if (sourceObject.getWmMinLengthAfter() != targetObject.getWmMinLengthAfter()) {
                    doAction = true;
                }

                if (sourceObject.getWmStartOffset() != targetObject.getWmStartOffset()) {
                    doAction = true;
                }

                if (sourceObject.getWmStartStep() != targetObject.getWmStartStep()) {
                    doAction = true;
                }

                break;
            case INSERT:
                doAction = true;
                List<Integer> sourceSiteIDs = sourceObject.getWmSiteId();

                sourceObject.setWmSiteId(siteService.getTargetSiteIDsBySourceSiteIDs(sourceSiteIDs));

                break;
        }

        if (doAction) {
            CfgBreakDTO cfgBreakDTO = new CfgBreakDTO();
            cfgBreakDTO.setDataSOAP(sourceObject);

            try {
                mergedObjectHolder = target.setDto(cfgBreakDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mergedObjectHolder;
    }
}
