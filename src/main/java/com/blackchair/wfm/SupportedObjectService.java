package com.blackchair.wfm;

import com.blackchair.csvheaders.AgentHeaders;
import com.blackchair.csvheaders.CalendarItemHeaders;
import com.blackchair.csvheaders.TimeOffBonusHeaders;
import com.blackchair.dto.SupportedObject;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import java.util.List;

import static java.util.Collections.singletonList;

@Service
public class SupportedObjectService {

    private String identifierDescription = "Identifier Fields list the fields, required to identify a unique agent. ";
    private String requiredDescription = "Required Fields list the rest of the fields, that are required in order to insert data. ";
    private String optionalDescription = "Optional Fields list fields, that can be included in the CSV, but are not required in order to insert data. ";
    private String optionalAssociationDescription = "Optional Associations are groups of fields, which are required in order to insert an association (Agent -> Object). ";


    public List<SupportedObject> getSupprotedObjects() {
        SupportedObject
                agent = getAgent(),
                timeOffBonus = getTimeOffBonus(),
                exception = getException(),
                timeOff = getTimeOff();

        return Arrays.asList(agent, timeOffBonus, exception, timeOff);
    }

    private SupportedObject getAgent() {
        String importantOne = "IMPORTANT: The “Target Site” field is used to note that you are moving the agent to a different site. If you are using it, the “Site” field for the associations " +
                "will be IGNORED, and they will refer to the site noted in “Target Site” (this is done in order to avoid having an agent associated with objects from different sites). ";
        String exampleHeader = AgentHeaders.EMPLOYEE_ID + "," + AgentHeaders.FIRST_NAME + "," + AgentHeaders.LAST_NAME + "," + AgentHeaders.TARGET_SITE + "," + AgentHeaders.TEAM + "," + AgentHeaders.EMAIL
                + "," + AgentHeaders.HIRE_DATE + "," + AgentHeaders.TERMINATION_DATE + "," + AgentHeaders.HOURLY_WAGE + "," + AgentHeaders.RANK + "," + AgentHeaders.COMMENTS + "," + AgentHeaders.CONTRACT_NAME
                + "," + AgentHeaders.CONTRACT_EFFECTIVE_DATE + "," + AgentHeaders.TIMEOFF_RULE_NAME + "," + AgentHeaders.TIMEOFF_TYPE + "," + AgentHeaders.TIMEOFF_RULE_START_DATE + "," + AgentHeaders.TRANSFER_BALANCE
                + "," + AgentHeaders.TIMEOFF_RULE_END_DATE + "," + AgentHeaders.ACTIVITY_NAME + "," + AgentHeaders.ACTIVITY_EFFECTIVE_DATE + "," + AgentHeaders.ACTIVITY_TYPE + "," + AgentHeaders.ROTATING_PATTERN_NAME
                + "," + AgentHeaders.ROTATING_PATTERN_EFFECTIVE_DATE + "," + AgentHeaders.ROTATING_PATTERN_STARTING_WEEK;
        String exampleData = "12345678,John,Smith,My Site,Cool Team,email@mail.com,20/03/2017,30/03/2017,50,1,This agent’s comment is good,8-hours Workday Contract,21/03/2017," +
                "Core Leave FT (8 Hours),Core Leave,21/03/2017,true,22/03/2017,Active Activity,21/03/2017,Primary,Rotating Pattern that rotates,20/03/2017,3";
        List<String> fieldsForIdentifier = singletonList(AgentHeaders.EMPLOYEE_ID + ", " + AgentHeaders.FIRST_NAME + ", " + AgentHeaders.LAST_NAME);
        List<String> optionalFields = singletonList(AgentHeaders.TARGET_SITE + ", " + AgentHeaders.TEAM + ", " + AgentHeaders.EMAIL + ", " + AgentHeaders.HIRE_DATE + ", " + AgentHeaders.TERMINATION_DATE
                + ", " + AgentHeaders.HOURLY_WAGE + ", " + AgentHeaders.RANK + ", " + AgentHeaders.COMMENTS);
        List<String> optionalAssociations = Arrays.asList("Contract Fields: " + AgentHeaders.CONTRACT_NAME + ", " + AgentHeaders.CONTRACT_EFFECTIVE_DATE + ", " + AgentHeaders.SITE,
                "Time-off Rule Fields: " + AgentHeaders.TIMEOFF_RULE_NAME + ", " + AgentHeaders.TIMEOFF_TYPE + ", " + AgentHeaders.TIMEOFF_RULE_START_DATE + ", " + AgentHeaders.TRANSFER_BALANCE
                        + ", " + AgentHeaders.SITE + "; Optional: " + AgentHeaders.TIMEOFF_RULE_END_DATE,
                "Activity Fields: " + AgentHeaders.ACTIVITY_NAME + ", " + AgentHeaders.ACTIVITY_EFFECTIVE_DATE + ", " + AgentHeaders.ACTIVITY_TYPE + ", " + AgentHeaders.SITE,
                "Rotating Pattern Fields: " + AgentHeaders.ROTATING_PATTERN_NAME + ", " + AgentHeaders.ROTATING_PATTERN_EFFECTIVE_DATE + ", " + AgentHeaders.ROTATING_PATTERN_STARTING_WEEK + ", " + AgentHeaders.SITE);
        List<String> description = Arrays.asList(identifierDescription, optionalDescription, optionalAssociationDescription, importantOne);
        List<String> examples = Arrays.asList(exampleHeader, exampleData);

        return new SupportedObject("Agent", 1, fieldsForIdentifier, null, optionalFields, optionalAssociations, description, examples);
    }

    private SupportedObject getTimeOffBonus() {
        String exampleHeader = TimeOffBonusHeaders.EMPLOYEE_ID + "," + TimeOffBonusHeaders.FIRST_NAME + "," + TimeOffBonusHeaders.LAST_NAME + "," + TimeOffBonusHeaders.TIMEOFF_TYPE
                + "," + TimeOffBonusHeaders.START_DATE + "," + TimeOffBonusHeaders.SITE + "," + TimeOffBonusHeaders.END_DATE + "," + TimeOffBonusHeaders.COMMENTS + "," + TimeOffBonusHeaders.BONUS_HOURS;
        String exampleData = "12345678,John,Smith,Vacation,20/03/2017,My Site,20/03/2017,This comment is about the bonus,10";
        List<String> fieldsForIdentifier = singletonList(TimeOffBonusHeaders.EMPLOYEE_ID + ", " + TimeOffBonusHeaders.FIRST_NAME + ", " + TimeOffBonusHeaders.LAST_NAME);
        List<String> requiredFields = singletonList(TimeOffBonusHeaders.TIMEOFF_TYPE + ", " + TimeOffBonusHeaders.START_DATE + ", " + TimeOffBonusHeaders.SITE);
        List<String> optionalFields = singletonList(TimeOffBonusHeaders.END_DATE + ", " + TimeOffBonusHeaders.COMMENTS + ", " + TimeOffBonusHeaders.BONUS_HOURS);
        List<String> description = Arrays.asList(identifierDescription, requiredDescription, optionalDescription);
        List<String> examples = Arrays.asList(exampleHeader, exampleData);

        return new SupportedObject("TimeOff Bonus", 2, fieldsForIdentifier, requiredFields, optionalFields, null, description, examples);
    }

    private SupportedObject getException() {
        String importantOne = "IMPORTANT: Item Type stands for Exception Type Name (the headers for time-off and exceptions are the same due to them both being calendar items and requiring very similar data); ";
        String importantTwo = "IMPORTANT: Paid Time is set in MINUTES";
        String importantThree = "IMPORTANT: Start/End time has to be inserted in UTC. WFM will take care of timezone offsets.";
        String exampleHeader = CalendarItemHeaders.EMPLOYEE_ID + "," + CalendarItemHeaders.FIRST_NAME + "," + CalendarItemHeaders.LAST_NAME + "," + CalendarItemHeaders.SITE + "," + CalendarItemHeaders.DATE
                + "," + CalendarItemHeaders.ITEM_TYPE + "," + CalendarItemHeaders.FULL_DAY + "," + CalendarItemHeaders.START_TIME + "," + CalendarItemHeaders.END_TIME + "," + CalendarItemHeaders.COMMENT
                + "," + CalendarItemHeaders.PAID_TIME;
        String exampleData = "12345678,John,Smith,My Site,20/03/2017,Coaching Exception,1,10:00,12:30,This comment is about the exception,120";
        List<String> fieldsForIdentifier = singletonList(CalendarItemHeaders.EMPLOYEE_ID + ", " + CalendarItemHeaders.FIRST_NAME + ", " + CalendarItemHeaders.LAST_NAME);
        List<String> requiredFields = singletonList(CalendarItemHeaders.SITE + ", " + CalendarItemHeaders.DATE + ", " + CalendarItemHeaders.ITEM_TYPE + ", " + CalendarItemHeaders.FULL_DAY);
        List<String> optionalFields = singletonList(CalendarItemHeaders.START_TIME + ", " + CalendarItemHeaders.END_TIME + ", " + CalendarItemHeaders.COMMENT + ", " + CalendarItemHeaders.PAID_TIME);
        List<String> description = Arrays.asList(identifierDescription, requiredDescription, optionalDescription, importantOne, importantTwo, importantThree);
        List<String> examples = Arrays.asList(exampleHeader, exampleData);

        return new SupportedObject("Exception", 3, fieldsForIdentifier, requiredFields, optionalFields, null, description, examples);
    }

    private SupportedObject getTimeOff() {
        String importantOne = "IMPORTANT: Item Type stands for Time-off Type Name (the headers for time-off and exceptions are the same due to them both being calendar items and requiring very similar data); ";
        String importantTwo = "IMPORTANT: Paid Time is set in MINUTES";
        String importantThree = "IMPORTANT: Start/End time has to be inserted in UTC. WFM will take care of timezone offsets.";
        String exampleHeader = CalendarItemHeaders.EMPLOYEE_ID + "," + CalendarItemHeaders.FIRST_NAME + "," + CalendarItemHeaders.LAST_NAME + "," + CalendarItemHeaders.SITE + "," + CalendarItemHeaders.DATE
                + "," + CalendarItemHeaders.ITEM_TYPE + "," + CalendarItemHeaders.FULL_DAY + "," + CalendarItemHeaders.START_TIME + "," + CalendarItemHeaders.END_TIME + "," + CalendarItemHeaders.COMMENT
                + "," + CalendarItemHeaders.PAID_TIME;
        String exampleData = "12345678,John,Smith,My Site,20/03/2017,Vacation,1,10:00,12:30,This comment is about the vacation,120";
        List<String> fieldsForIdentifier = singletonList(CalendarItemHeaders.EMPLOYEE_ID + ", " + CalendarItemHeaders.FIRST_NAME + ", " + CalendarItemHeaders.LAST_NAME);
        List<String> requiredFields = singletonList(CalendarItemHeaders.SITE + ", " + CalendarItemHeaders.DATE + ", " + CalendarItemHeaders.ITEM_TYPE + ", " + CalendarItemHeaders.FULL_DAY);
        List<String> optionalFields = singletonList(CalendarItemHeaders.START_TIME + ", " + CalendarItemHeaders.END_TIME + ", " + CalendarItemHeaders.COMMENT + ", " + CalendarItemHeaders.PAID_TIME);
        List<String> description = Arrays.asList(identifierDescription, requiredDescription, optionalDescription, importantOne, importantTwo, importantThree);
        List<String> examples = Arrays.asList(exampleHeader, exampleData);

        return new SupportedObject("TimeOff", 4, fieldsForIdentifier, requiredFields, optionalFields, null, description, examples);
    }
}
