package com.blackchair.wfm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.blackchair.dto.*;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;
import com.genesyslab.wfm8.API.service.config852.CfgSite;
import com.genesyslab.wfm8.API.service.config852.CfgSiteDetails;
import com.genesyslab.wfm8.API.service.config852.CfgSiteFilter;
import com.genesyslab.wfm8.API.service.config852.CfgSiteHolder;
import com.genesyslab.wfm8.API.service.config852.CfgSortMode;
import com.genesyslab.wfm8.API.service.config852.ECfgInfoType;
import com.genesyslab.wfm8.API.service.config852.ECfgSortMode;

@Service
public class SiteService extends WFMConfigService {

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgSiteHolder cfgSiteHolder = this.getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgSite wfmEntity : cfgSiteHolder.getObjectArray()) {
            CfgSiteDTO dto = (CfgSiteDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgSiteHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgSiteFilter cfgSiteFilter = new CfgSiteFilter();
        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null && filterDTO.getBuId() != null) {
            cfgSiteFilter.withWmBUId(filterDTO.getBuId());
        }

        if (filterDTO != null && filterDTO.getId() != null) {
            cfgSiteFilter.withWmSiteId(filterDTO.getId());
        }

        return super.getService(wfmObjectHolder).getSite(cfgSiteFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Site.CFG_SITE_SORT_NAME).withAscending(true),
                new CfgSiteDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    List<Integer> getTargetSiteIDsBySourceSiteIDs(List<Integer> sourceSiteIDs) {
        List<Integer> targetSiteIDs = new ArrayList<>();

        for (Integer sourceId : sourceSiteIDs) {
            if (this.linkID.containsKey(sourceId)) {
                targetSiteIDs.add(this.linkID.get(sourceId));
            }
        }
        return targetSiteIDs;
    }

    HashMap<String, Integer> getSiteIDsAndNames(WfmObjectHolder wfmObjectHolder) throws Exception
    {
        HashMap<Integer, BaseDTO> list = this.getAll(wfmObjectHolder);
        HashMap<String, Integer> result = new HashMap<>();
        CfgSite site;
        for (BaseDTO baseDTO : list.values())
        {
            site = ((CfgSite) baseDTO.getDataSOAP());
            result.put(site.getWmName(), site.getWmSiteId());
        }
        return result;
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgSite sourceObject = (CfgSite) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgSite targetObject = (CfgSite) listTarget.get(hashCode).getDataSOAP();
                this.linkID.put(sourceObject.getWmSiteId(), targetObject.getWmSiteId());
            }
        }
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgSite wfmObject = (CfgSite) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "Site";
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }
}
