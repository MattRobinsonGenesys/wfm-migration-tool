package com.blackchair.wfm;

import com.blackchair.PocApplication;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UploadFileService {

    public boolean uploadFile(MultipartFile file) {
        boolean isUploaded;
        String fileDir = getRootPath() + File.separator + "CSV files" + File.separator;
        String fileName = "data.csv";
//        String currentTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"));
//        String fileName = file.getName() + "_" + currentTime + "." + file.getContentType().split("/")[1];
        Path path = Paths.get(fileDir + fileName);
        try {
            File directory = new File(fileDir);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            Files.write(path, file.getBytes());
            isUploaded = true;
        } catch (IOException e) {
            isUploaded = false;
            e.printStackTrace();
        }
        return isUploaded;
    }

    public String getRootPath() {
        String jarPath = null;

        try {
            String mainClassFolder = PocApplication.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            mainClassFolder = URLDecoder.decode(mainClassFolder, "UTF-8");
            mainClassFolder = mainClassFolder.replace('/', File.separatorChar);

            if (mainClassFolder.contains("file:") && mainClassFolder.contains(".jar")) {
                String regex = "file:(.*)[\\/\\\\].*\\.jar";
                Matcher matcher = Pattern.compile(regex).matcher(mainClassFolder);
                while (matcher.find()) {
                    jarPath = matcher.group(1);
                }
            } else {
                String spliter;
                if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
                    spliter = "\\\\classes\\\\";
                } else {
                    spliter = File.separator + "classes" + File.separator;
                }
                String[] paths = mainClassFolder.split(spliter);
                if (paths.length > 0) {
                    jarPath = paths[0];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            jarPath = jarPath.substring(1);
        }

        return jarPath;
    }
}
