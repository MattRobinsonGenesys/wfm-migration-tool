package com.blackchair.wfm;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import com.blackchair.PocApplication;
import com.blackchair.csvheaders.AgentHeaders;
import com.blackchair.csvheaders.BaseHeaders;
import com.blackchair.csvheaders.TimeOffBonusHeaders;
import com.blackchair.dto.*;
import com.blackchair.event.LogEvent;
import com.blackchair.event.LogImportEvent;
import com.blackchair.event.LogSystemEvent;
import com.blackchair.event.importcsv.StartedImportEvent;
import com.blackchair.event.importcsv.StoppedImportEvent;
import com.blackchair.event.systemlog.StartedSystemEvent;
import com.blackchair.job.SetupObjects;
import com.csvreader.CsvReader;
import com.genesyslab.wfm8.API.service.config852.*;
import com.genesyslab.wfm8.API.util.OleDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.aggregator.MergeService;

@Service
public class AgentService extends WFMConfigService {

    @Autowired
    protected ContractService contractService;

    @Autowired
    protected ActivityService activityService;

    @Autowired
    protected CalendarItemService calendarItemService;

    @Autowired
    protected AccrualRuleService accrualRuleService;

    @Autowired
    protected TimeOffTypeService timeOffTypeService;

    @Autowired
    protected RotatingPatternService rotatingPatternService;

    @Autowired
    protected BusinessUnitService businessUnitService;

    @Autowired
    private CarpoolService carpoolService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private SiteService siteService;

    @Autowired
    private UploadFileService uploadFileService;

    private WfmObjectHolder wfmObjectHolder;
    private CsvReader reader;

    //Fill this when we first call insert so we don't have to call getAll every time we look up them
    //format for most is: <siteID, collection<object>>
    private HashMap<String, Integer> siteCache;
    private HashMap<Integer, Collection<BaseDTO>> timeOffTypeCache;
    private HashMap<Integer, Collection<BaseDTO>> contractCache;
    private HashMap<Integer, Collection<BaseDTO>> activityCache;
    private HashMap<Integer, Collection<BaseDTO>> timeOffRuleCache;
    private HashMap<Integer, Collection<BaseDTO>> rotatingPatternCache;
    private HashMap<Integer, Collection<BaseDTO>> teamCache;

    private void clearCache() {
        siteCache = new HashMap<>();
        timeOffRuleCache = new HashMap<>();
        timeOffTypeCache = new HashMap<>();
        contractCache = new HashMap<>();
        activityCache = new HashMap<>();
        rotatingPatternCache = new HashMap<>();
        teamCache = new HashMap<>();
    }

    public void setWfmObjectHolder(WfmObjectHolder wfmObjectHolder) {
        this.wfmObjectHolder = wfmObjectHolder;
    }

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgAgentHolder cfgAgentHolder = this.getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgAgent wfmEntity : cfgAgentHolder.getObjectArray()) {
            CfgAgentDTO dto = (CfgAgentDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }
        return list;
    }

    public CfgAgentHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();
        CfgActivityFilter cfgActivityFilter = new CfgActivityFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getAgentEmployeeId() != null) {
                cfgAgentFilter.withGswEmployeeId(filterDTO.getAgentEmployeeId());
            }
            if (filterDTO.getAgentFirstName() != null) {
                cfgAgentFilter.withGswFirstName(filterDTO.getAgentFirstName());
            }
            if (filterDTO.getAgentLastName() != null) {
                cfgAgentFilter.withGswLastName(filterDTO.getAgentLastName());
            }
            if (filterDTO.getBuId() != null) {
                cfgAgentFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getContractId() != null) {
                cfgAgentFilter.withWmContractId(filterDTO.getContractId());
            }

            if (filterDTO.getIds() != null) {
                cfgAgentFilter.withGswAgentId(filterDTO.getIds());
            }

            if (filterDTO.getSiteId() != null) {
                cfgAgentFilter.withWmSiteId(filterDTO.getSiteId());
            }
        }

        return super.getService(wfmObjectHolder).getAgent(cfgAgentFilter, cfgActivityFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Agent.CFG_AGENT_SORT_FIRST_NAME).withAscending(true),
                new CfgAgentDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgService = super.getService(syncDTO.getMergedObject());
        this.updateContract(syncDTO, cfgService);
        this.updateRotatingPattern(syncDTO, cfgService);
    }

    private void updateContract(SyncDTO syncDTO, WFMConfigService852Soap cfgService) throws Exception {
        WfmObjectHolder mergedObjectHolder = syncDTO.getMergedObject();
        WfmObjectHolder target = syncDTO.getTarget();

        this.event.publishEvent(new LogEvent(this, "updating the contracts..."));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        this.associatingContracts(mergedObjectHolder, target);

        CfgAgent mergedObject = (CfgAgent) syncDTO.getMergedObject().getDto().getDataSOAP();
        CfgValidationHolder res = cfgService.updateAgent(mergedObject, false, false);

        checkResponse(res);
        this.event.publishEvent(new LogEvent(this, "Agent contract was successfully updated"));
    }

    private void updateRotatingPattern(SyncDTO syncDTO, WFMConfigService852Soap cfgService) throws Exception {
        WfmObjectHolder mergedObjectHolder = syncDTO.getMergedObject();
        WfmObjectHolder target = syncDTO.getTarget();

        this.event.publishEvent(new LogEvent(this, "updating the rotating patterns..."));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source");
        }

        CfgAgent mergedObject = (CfgAgent) mergedObjectHolder.getDto().getDataSOAP();
        List<CfgAgentRotation> cfgAgentRotations = mergedObject.getWmAgentRotatingSch();

        try {
            this.associatingRotatingPatterns(mergedObject, target);
            CfgValidationHolder res = cfgService.updateAgent(mergedObject, false, false);
            checkResponse(res);
            this.event.publishEvent(new LogEvent(this, "Agent rotating patterns were successfully updated"));
        } catch (Exception e) {
            mergedObject.setWmAgentRotatingSch(cfgAgentRotations);
            this.event.publishEvent(new LogEvent(this, "Warning: " + e.getMessage()));
        }
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        throw new IllegalStateException("disabled action");
    }

    private void associatingContracts(WfmObjectHolder mergedObjectHolder, WfmObjectHolder target) throws Exception {
        this.event.publishEvent(new LogEvent(this, "associating contract relations ... "));

        if (mergedObjectHolder.getDto() == null) {
            throw new IllegalArgumentException("Missing source checking contract association");
        }

        CfgAgent mergedSourceObject = (CfgAgent) mergedObjectHolder.getDto().getDataSOAP();
        List<CfgAgentContract> cfgRelationsId = new ArrayList<>();

        if (target.getDto() == null) {
            if (!mergedSourceObject.getWmAgentContracts().isEmpty()) {
                cfgRelationsId = this.contractService.syncAgentContracts(mergedSourceObject.getWmAgentContracts());
            }

        } else {
            CfgAgent targetObject = (CfgAgent) target.getDto().getDataSOAP();

            if (!mergedSourceObject.getWmAgentContracts().isEmpty()) {
                cfgRelationsId = this.contractService.syncAgentContracts(mergedSourceObject.getWmAgentContracts());
            } else if (!mergedSourceObject.getWmAgentContracts().isEmpty() && targetObject.getWmAgentContracts().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "target does not have any contracts... merging contracts..."));
                cfgRelationsId = this.contractService.syncAgentContracts(mergedSourceObject.getWmAgentContracts());
            } else if (!mergedSourceObject.getWmAgentContracts().isEmpty() && !targetObject.getWmAgentContracts().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "Warning " + getObjectServiceName() + ": " + getObjectName(target) + " already  has contracts"));
                cfgRelationsId = targetObject.getWmAgentContracts();
            } else if (mergedSourceObject.getWmAgentContracts().isEmpty() && !targetObject.getWmAgentContracts().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "source does not have any contracts... but target has..."));
                cfgRelationsId = targetObject.getWmAgentContracts();
            }
        }

        mergedSourceObject.setWmAgentContracts(cfgRelationsId);
    }

    private void associatingRotatingPatterns(CfgAgent mergedSourceObject, WfmObjectHolder target) throws IllegalArgumentException {
        this.event.publishEvent(new LogEvent(this, "associating rotating pattern relations ... "));

        if (mergedSourceObject == null) {
            throw new IllegalArgumentException("Missing source checking rotating pattern association");
        }

        List<CfgAgentRotation> cfgRelationsId = new ArrayList<>();

        if (target.getDto() == null) {
            if (!mergedSourceObject.getWmAgentRotatingSch().isEmpty()) {
                cfgRelationsId = this.rotatingPatternService.syncAgentRotatingSch(mergedSourceObject.getWmAgentRotatingSch());
            }
        } else {
            CfgAgent targetObject = (CfgAgent) target.getDto().getDataSOAP();

            if (!mergedSourceObject.getWmAgentRotatingSch().isEmpty() && targetObject.getWmAgentRotatingSch().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "target does not have any rotating patterns... merging rotating patterns..."));
                cfgRelationsId = this.rotatingPatternService.syncAgentRotatingSch(mergedSourceObject.getWmAgentRotatingSch());
            } else if (!mergedSourceObject.getWmAgentRotatingSch().isEmpty() && !targetObject.getWmAgentRotatingSch().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "Warning " + getObjectServiceName() + ": " + getObjectName(target) + " already  has rotating patterns"));
                cfgRelationsId = targetObject.getWmAgentRotatingSch();
            } else if (mergedSourceObject.getWmAgentRotatingSch().isEmpty() && !targetObject.getWmAgentRotatingSch().isEmpty()) {
                this.event.publishEvent(new LogEvent(this, "source does not have any rotating patterns... but target has..."));
                cfgRelationsId = targetObject.getWmAgentRotatingSch();
            }
        }

        mergedSourceObject.setWmAgentRotatingSch(cfgRelationsId);
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgAgent wfmObject = (CfgAgent) wfmObjectHolder.getDto().getDataSOAP();
        return "EmployeeId:: " + wfmObject.getGswEmployeeId() + " Name: " + wfmObject.getGswFirstName() + " " + wfmObject.getGswLastName();
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {

        CfgAgent sourceObject = (CfgAgent) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;

        boolean doAction = false;

        //we should to insert
        switch (status) {
            case UPDATE:
                CfgAgent targetObject = (CfgAgent) target.getDto().getDataSOAP();

                this.linkID.put(sourceObject.getGswAgentId(), targetObject.getGswAgentId());

                sourceObject.setGswAgentId(targetObject.getGswAgentId());
                sourceObject.setWmSiteId(targetObject.getWmSiteId());

                if (!Objects.equals(sourceObject.getGswEmail(), targetObject.getGswEmail())) {
                    doAction = true;
                }

                if (sourceObject.getWmCarpoolId() > 0) {

                    if (carpoolService.getLinkID().containsKey(sourceObject.getWmCarpoolId())) {
                        int targetCarpoolId = carpoolService.getLinkID().get(sourceObject.getWmCarpoolId());

                        if (targetCarpoolId != targetObject.getWmCarpoolId()) {
                            sourceObject.setWmCarpoolId(targetCarpoolId);
                            doAction = true;
                        }
                    }
                }

                if (!Objects.equals(sourceObject.getWmComments(), targetObject.getWmComments())) {
                    doAction = true;
                }

                if (sourceObject.getWmContractId() > 0) {

                    if (contractService.getLinkID().containsKey(sourceObject.getWmContractId())) {
                        int targetContractId = contractService.getLinkID().get(sourceObject.getWmContractId());

                        if (targetContractId != targetObject.getWmContractId()) {
                            sourceObject.setWmContractId(targetContractId);
                            doAction = true;
                        }
                    }
                }

                if (sourceObject.getWmEndDate() != targetObject.getWmEndDate()) {
                    doAction = true;
                }

                if (sourceObject.getWmHourlyWage() != targetObject.getWmHourlyWage()) {
                    doAction = true;
                }

                if (sourceObject.getWmSeniority() != targetObject.getWmSeniority()) {
                    doAction = true;
                }

                if (sourceObject.getWmStartDate() != targetObject.getWmStartDate()) {
                    doAction = true;
                }

                if (sourceObject.getWmTeamId() > 0) {

                    if (teamService.getLinkID().containsKey(sourceObject.getWmTeamId())) {
                        int targetTeamId = teamService.getLinkID().get(sourceObject.getWmTeamId());
                        if (targetTeamId != targetObject.getWmTeamId()) {
                            sourceObject.setWmTeamId(targetTeamId);
                            doAction = true;
                        }
                    }
                }

                if (this.checkRotatingPatterns(source, target)) {
                    doAction = true;
                }

                if (this.checkContracts(source, target)) {
                    doAction = true;
                }

                break;
            case INSERT:
                doAction = false;
                break;
        }

        if (doAction) {
            CfgAgentDTO cfgAgentDTO = new CfgAgentDTO();
            cfgAgentDTO.setDataSOAP(sourceObject);

            try {
                mergedObjectHolder = target.setDto(cfgAgentDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mergedObjectHolder;
    }

    private boolean matchAgentTimeBonusCsv(List<String> header) {
        //Required fields: Employee ID,First Name,Last Name,Time-off Type ID,Start Date
        List<String> validator = new ArrayList<>();
        TimeOffBonusHeaders timeOffBonusHeaders = new TimeOffBonusHeaders();
        validator.addAll(BaseHeaders.getIdentifiers());
        validator.addAll(timeOffBonusHeaders.getRequiredFields());
        //Optional fields: End Date,Comments,Bonus Hours
        //Check if each field in validator matches any of the fields in header. If it matches a given field, remove it from the stream.
        //If the stream is not empty, this means that one or more fields didn't match, so return false.
        return validator.stream().filter(x -> (!header.stream().anyMatch(y -> y.equals(x)))).count() <= 0;
    }

    CfgAgentDTO getAgentByIdentifier(WfmObjectHolder wfmObjectHolder, CsvReader reader) throws Exception {
        wfmObjectHolder.getFilter().setSiteId(null);
        wfmObjectHolder.getFilter().setBuId(null);
        wfmObjectHolder.getFilter().setAgentEmployeeId(reader.get(BaseHeaders.EMPLOYEE_ID));
        wfmObjectHolder.getFilter().setAgentFirstName(reader.get(BaseHeaders.FIRST_NAME));
        wfmObjectHolder.getFilter().setAgentLastName(reader.get(BaseHeaders.LAST_NAME));
        HashMap<Integer, BaseDTO> agents = this.getAll(wfmObjectHolder);
        wfmObjectHolder.getFilter().setAgentEmployeeId(null);
        wfmObjectHolder.getFilter().setAgentFirstName(null);
        wfmObjectHolder.getFilter().setAgentLastName(null);
        if (agents == null || agents.values().isEmpty()) {
            throw new IllegalArgumentException("Agent " + reader.get(BaseHeaders.FIRST_NAME) + " " + reader.get(BaseHeaders.LAST_NAME) + " " + reader.get(BaseHeaders.EMPLOYEE_ID) + " does not exist in database");
        }
        return (CfgAgentDTO) agents.values().toArray()[0];
    }

    boolean parseTimeOffBonus(WfmObjectHolder wfmObjectHolder) {
        Long start = Calendar.getInstance().getTimeInMillis();
        this.wfmObjectHolder = wfmObjectHolder;
        String fileName = this.uploadFileService.getRootPath() + File.separator + "CSV files" + File.separator + "data.csv";
        boolean success = true;

        this.event.publishEvent(new StartedImportEvent(this));
        if (PocApplication.debugMode) {
            this.event.publishEvent(new StartedSystemEvent(this));
        }

        try {
            WFMConfigService852Soap cfgService = super.getService(wfmObjectHolder);
            //TODO reader = new CsvReader(fileName,',', Charset.forName("UTF-16"));
            reader = new CsvReader(fileName);
            //Read the headers of the csv
            reader.readHeaders();
            //If the headers don't match, throw exception
            if (!matchAgentTimeBonusCsv(Arrays.asList(reader.getHeaders()))) {
                throw new IllegalArgumentException("CSV File Headers don't match the required headers for Agent Time Bonus");
            }
            Set<String> siteNames = new HashSet<>();
            List<Integer> siteIDs = new ArrayList<>();
            while (reader.readRecord()) {
                siteNames.add(reader.get(BaseHeaders.SITE));
            }
            reader.close();
            //TODO reader = new CsvReader(fileName,',', Charset.forName("UTF-16"));
            reader = new CsvReader(fileName);
            reader.readHeaders();

            //Fill the agent cache
            this.event.publishEvent(new LogImportEvent(this, "Caching started"));
            siteCache = siteService.getSiteIDsAndNames(wfmObjectHolder);
            this.event.publishEvent(new LogImportEvent(this, "Sites cached"));
            //FILTER THE SITES
            siteNames.forEach(x -> siteIDs.add(siteCache.get(x)));
            timeOffTypeCache = fillCache(timeOffTypeService, wfmObjectHolder, siteIDs);
            this.event.publishEvent(new LogImportEvent(this, "Time Off Types cached"));
            this.event.publishEvent(new LogImportEvent(this, "Caching finished"));

            //Loop through every record in the csv
            while (reader.readRecord()) {
                try {
                    //Hashcode is based on the required columns Employee ID, First Name and Last Name (REQUIRED FIELDS)
                    //Get the agent that matches the given hashcode
                    CfgAgent agent;
                    CfgAgentDTO agentDTO = getAgentByIdentifier(wfmObjectHolder, reader);
                    if (agentDTO == null) {
                        throw new IllegalArgumentException("Agent " + reader.get(BaseHeaders.FIRST_NAME) + " " + reader.get(BaseHeaders.LAST_NAME) + " " + reader.get(BaseHeaders.EMPLOYEE_ID) + " does not exist in database");
                    }
                    agent = agentDTO.getDataSOAP();

                    if (siteCache.get(reader.get(BaseHeaders.SITE)) == null) {
                        throw new IllegalArgumentException("Site " + reader.get(BaseHeaders.SITE) + " does not exist in database");
                    }
                    //Get a list of the agent's current time off bonuses
                    List<CfgAgentTimeOffBonus> timeOffBonusList = agent.getWmAgentTimeOffBonus();
                    //Create an empty timeOffBonus, which we are going to fill below
                    CfgAgentTimeOffBonus timeOffBonus = new CfgAgentTimeOffBonus();

                    //Set agent id
                    timeOffBonus.setGswAgentId(agent.getGswAgentId());
                    //Set time off type (REQUIRED FIELD)
                    timeOffBonus.setWmTimeoffTypeId(getTimeOffTypeIDByName(agent.getWmSiteId()));
                    //Set start date (REQUIRED FIELD)
                    String date = reader.get(TimeOffBonusHeaders.START_DATE);
                    double startDate = new OleDateTime(Integer.parseInt(date.substring(6)), Integer.parseInt(date.substring(3, 5)), Integer.parseInt(date.substring(0, 2))).asDouble();
                    timeOffBonus.setWmStartDate(startDate);
                    //Set end date (OPTIONAL FIELD)
                    if (reader.get(TimeOffBonusHeaders.END_DATE) != null && !reader.get(TimeOffBonusHeaders.END_DATE).equals("")) {
                        date = reader.get(TimeOffBonusHeaders.END_DATE);
                        if (date.length() > 1) {
                            double endDate = new OleDateTime(Integer.parseInt(date.substring(6)), Integer.parseInt(date.substring(3, 5)), Integer.parseInt(date.substring(0, 2))).asDouble();
                            timeOffBonus.setWmEndDate(endDate);
                        }
                    }
                    if ((timeOffBonus.getWmEndDate() > 0) && (timeOffBonus.getWmEndDate() < timeOffBonus.getWmStartDate())) {
                        throw new IllegalArgumentException("The End Date of the Time-off bonus is before the Start Date");
                    }
                    //Set comments (OPTIONAL FIELD)
                    if (reader.get(TimeOffBonusHeaders.COMMENTS) != null && !reader.get(TimeOffBonusHeaders.COMMENTS).equals("")) {
                        timeOffBonus.setWmComments(reader.get(TimeOffBonusHeaders.COMMENTS));
                    }
                    //Set bonus hours (OPTIONAL FIELD)
                    if (reader.get(TimeOffBonusHeaders.BONUS_HOURS) != null && !reader.get(TimeOffBonusHeaders.BONUS_HOURS).equals("")) {
                        timeOffBonus.setWmBonusHours(Double.parseDouble(reader.get(TimeOffBonusHeaders.BONUS_HOURS)));
                    }

                    timeOffBonusList.add(timeOffBonus);
                    agent.setWmAgentTimeOffBonus(timeOffBonusList);

                    CfgValidationHolder res = cfgService.updateAgent(agent, false, true);
                    checkResponse(res);
                    this.event.publishEvent(new LogImportEvent(this, "SUCCESS (row " + (reader.getCurrentRecord() + 2) + ") - Agent " + agent.getGswFirstName() + " " + agent.getGswLastName() + " with ID " + agent.getGswEmployeeId()));
                    //} catch (IllegalArgumentException | IllegalStateException | ServerSOAPFaultException e) {
                } catch (Exception e) {
                    this.event.publishEvent(new LogImportEvent(this, "FAIL (row " + (reader.getCurrentRecord() + 2) + ") - " + e.getMessage()));
                    e.printStackTrace();
                    if (PocApplication.debugMode) {
                        for (int i = 0; i < e.getStackTrace().length; i++) {
                            this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                        }
                    }
                }
            }
        } catch (Exception e) {
            this.event.publishEvent(new LogImportEvent(this, "ERROR - (row " + (reader.getCurrentRecord() + 2) + ")" + e.getMessage()));
            if (PocApplication.debugMode) {
                for (int i = 0; i < e.getStackTrace().length; i++) {
                    this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                }
            }
            success = false;
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
            clearCache();
            this.event.publishEvent(new LogImportEvent(this, "Import ended in " + ((Calendar.getInstance().getTimeInMillis() - start) / 1000) + " seconds"));
            this.event.publishEvent(new StoppedImportEvent(this, "Done!"));
        }

        return success;
    }

    HashMap<Integer, Collection<BaseDTO>> fillCache(WFMConfigService service, WfmObjectHolder wfmObjectHolder, Collection<Integer> siteList) throws Exception {
        HashMap<Integer, Collection<BaseDTO>> cache = new HashMap<>();
        for (Integer siteId : siteList) {
            if (siteId == null) {
                continue;
            }
            FilterDTO filterDTO = new FilterDTO();
            filterDTO.setSiteId(siteId);
            wfmObjectHolder.setFilter(filterDTO);
            Collection<BaseDTO> objects = service.getAll(wfmObjectHolder).values();
            if (objects.isEmpty()) {
                cache.put(siteId, new ArrayList<>());
            } else {
                cache.put(siteId, objects);
            }
        }
        return cache;
    }

    private boolean checkHeaderForValue(String[] headers, String value) {
        for (int i = 0; i < headers.length; i++) {
            if (headers[i].equals(value)) {
                return true;
            }
        }
        return false;
    }


    boolean updateFromCSV(WfmObjectHolder wfmObjectHolder) throws IllegalArgumentException {
        Long start = Calendar.getInstance().getTimeInMillis();
        this.wfmObjectHolder = wfmObjectHolder;
        boolean success = true;

        this.event.publishEvent(new StartedImportEvent(this));
        if (PocApplication.debugMode) {
            this.event.publishEvent(new StartedSystemEvent(this));
        }

        try {
            WFMConfigService852Soap cfgService = getService(wfmObjectHolder);
            String fileName = this.uploadFileService.getRootPath() + File.separator + "CSV files" + File.separator + "data.csv";
            //TODO reader = new CsvReader(fileName,',', Charset.forName("UTF-16"));
            reader = new CsvReader(fileName);
            reader.readHeaders();
            String[] headerValues = reader.getHeaders();
            checkHeader();
            //read site names from the csv
            Set<String> siteNames = new HashSet<>();
            List<Integer> siteIDs = new ArrayList<>();
            if (checkHeaderForValue(headerValues, BaseHeaders.SITE) || checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE)) {
                while (reader.readRecord()) {
                    if (reader.get(BaseHeaders.SITE) != null && !reader.get(BaseHeaders.SITE).equals("")) {
                        siteNames.add(reader.get(BaseHeaders.SITE));
                    }
                    if (reader.get(AgentHeaders.TARGET_SITE) != null && !reader.get(AgentHeaders.TARGET_SITE).equals("")) {
                        siteNames.add(reader.get(AgentHeaders.TARGET_SITE));
                    }
                }
            }
            reader.close();
            //TODO reader = new CsvReader(fileName,',', Charset.forName("UTF-16"));
            reader = new CsvReader(fileName);
            reader.readHeaders();

            this.event.publishEvent(new LogImportEvent(this, "Caching started"));
            if (checkHeaderForValue(headerValues, BaseHeaders.SITE) || checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE)) {
                siteCache = siteService.getSiteIDsAndNames(wfmObjectHolder);
                this.event.publishEvent(new LogImportEvent(this, "Sites cached"));
                siteNames.forEach(x -> siteIDs.add(siteCache.get(x)));
            }
            if (checkHeaderForValue(headerValues, AgentHeaders.CONTRACT_NAME)) {
                contractCache = fillCache(contractService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Contracts cached"));
            }
            if (checkHeaderForValue(headerValues, AgentHeaders.ACTIVITY_NAME)) {
                activityCache = fillCache(activityService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Activities cached"));
            }
            if (checkHeaderForValue(headerValues, AgentHeaders.TIMEOFF_RULE_NAME)) {
                timeOffTypeCache = fillCache(timeOffTypeService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Time Off Types cached"));
                timeOffRuleCache = fillCache(accrualRuleService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Time Off Rules cached"));
            }
            if (checkHeaderForValue(headerValues, AgentHeaders.ROTATING_PATTERN_NAME)) {
                rotatingPatternCache = fillCache(rotatingPatternService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Rotating Patterns cached"));
            }
            if (checkHeaderForValue(headerValues, AgentHeaders.TEAM)) {
                teamCache = fillCache(teamService, wfmObjectHolder, siteIDs);
                this.event.publishEvent(new LogImportEvent(this, "Teams cached"));
            }
            this.event.publishEvent(new LogImportEvent(this, "Caching finished"));

            while (reader.readRecord()) {
                try {
                    checkRow();
                    CfgAgent agent;
                    boolean doUpdate = false;
                    //Make a get request for agent with requested employee ID
                    CfgAgentDTO agentDTO = getAgentByIdentifier(wfmObjectHolder, this.reader);
                    if (agentDTO == null) {
                        throw new IllegalArgumentException("Agent " + reader.get(AgentHeaders.FIRST_NAME) + " " + reader.get(AgentHeaders.LAST_NAME) + " " + reader.get(AgentHeaders.EMPLOYEE_ID) + " does not exist in database");
                    }
                    agent = agentDTO.getDataSOAP();

                    if (checkHeaderForValue(headerValues, AgentHeaders.SITE)) {
                        if (!checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE)) {
                            if (!siteCache.containsKey(reader.get(AgentHeaders.SITE))) {
                                throw new IllegalArgumentException("Site " + reader.get(AgentHeaders.SITE) + " does not exist in database");
                            }
                        }
                    }
                    //Check if agent is gonna be moved to a new site
                    if (checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE) && !reader.get(AgentHeaders.TARGET_SITE).equals("")) {
                        if (!siteCache.containsKey(reader.get(AgentHeaders.TARGET_SITE))) {
                            throw new IllegalArgumentException("Target Site " + reader.get(AgentHeaders.TARGET_SITE) + " does not exist in database");
                        }
                        agent.setWmSiteId(siteCache.get(reader.get(AgentHeaders.TARGET_SITE)));
                        doUpdate = true;
                    }
                    for (int i = 0; i < headerValues.length; ++i) {
                        if (!reader.get(i).equals("") && updateAgentProperty(agent, i)) {
                            doUpdate = true;
                        }
                    }
                    if (doUpdate) {
                        //TODO IGNORE WARNINGS IS THIRD BOOLEAN
                        CfgValidationHolder res = cfgService.updateAgent(agent, false, true);
                        checkResponse(res);
                        this.event.publishEvent(new LogImportEvent(this, "SUCCESS (row " + (reader.getCurrentRecord() + 2) + ") - Agent " + agent.getGswFirstName() + " " + agent.getGswLastName() + " with ID " + agent.getGswEmployeeId()));
                    }
                    //} catch (IllegalArgumentException | IllegalStateException | ServerSOAPFaultException e) {
                } catch (Exception e) {
                    this.event.publishEvent(new LogImportEvent(this, "FAIL (row " + (reader.getCurrentRecord() + 2) + ") - " + e.getMessage()));
                    e.printStackTrace();
                    if (PocApplication.debugMode) {
                        for (int i = 0; i < e.getStackTrace().length; i++) {
                            this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                        }
                    }
                }
            }
        } catch (Exception e) {
            this.event.publishEvent(new LogImportEvent(this, "ERROR - (row " + (reader.getCurrentRecord() + 2) + ")" + e.getMessage()));
            if (PocApplication.debugMode) {
                for (int i = 0; i < e.getStackTrace().length; i++) {
                    this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                }
            }
            success = false;
            e.printStackTrace();
        } finally {
            if (reader != null) {
                reader.close();
            }
            clearCache();
            this.event.publishEvent(new LogImportEvent(this, "Import ended in " + ((Calendar.getInstance().getTimeInMillis() - start) / 1000) + " seconds"));
            this.event.publishEvent(new StoppedImportEvent(this, "Done!"));
        }

        return success;
    }

    private void checkRow() throws IOException {
        String missingValues = "";
        if (reader.get(BaseHeaders.EMPLOYEE_ID).equals("")) {
            missingValues = missingValues + BaseHeaders.EMPLOYEE_ID + ", ";
        }
        if (reader.get(BaseHeaders.FIRST_NAME).equals("")) {
            missingValues = missingValues + BaseHeaders.FIRST_NAME + ", ";
        }
        if (reader.get(BaseHeaders.LAST_NAME).equals("")) {
            missingValues = missingValues + BaseHeaders.LAST_NAME;
        }
        if (missingValues.endsWith(", ")) {
            missingValues = new StringBuilder(missingValues).delete(missingValues.length() - 2, missingValues.length() - 1).toString();
        }
        if (!missingValues.equals("")) {
            throw new IllegalArgumentException("Missing values for properties: " + missingValues);
        }

    }

    private void checkHeader() throws IllegalArgumentException, IOException {
        String[] headerValues = this.reader.getHeaders();
        String[] requiredHeaders;
        requiredHeaders = BaseHeaders.getIdentifiers().toArray(new String[BaseHeaders.getIdentifiers().size()]);
        StringBuilder headers = new StringBuilder();
        boolean missingHeader = false;

        label:
        for (String requiredHeader : requiredHeaders) {
            for (String headerValue : headerValues) {
                if (headerValue.equals(requiredHeader)) {
                    continue label;
                }
            }
            missingHeader = true;
            headers.append(requiredHeader).append(", ");
        }
        if (missingHeader) {
            headers = headers.delete(headers.length() - 2, headers.length() - 1);
            throw new IllegalArgumentException("Missing headers: " + headers);
        }
    }

    private boolean updateAgentProperty(CfgAgent agent, int index) throws Exception {

        String value = reader.get(index);
        double date;
        boolean doUpdate = false;

        switch (this.reader.getHeader(index)) {
            case AgentHeaders.TEAM:
                if (reader.get(AgentHeaders.TEAM) != null && !reader.get(AgentHeaders.TEAM).equals("")) {
                    int teamID = getTeamIDByName(agent.getWmSiteId());
                    if (agent.getWmTeamId() != teamID) {
                        doUpdate = true;
                        agent.setWmTeamId(teamID);
                    }
                }
                break;
            case AgentHeaders.CONTRACT_NAME:
                if (reader.get(AgentHeaders.CONTRACT_NAME) != null && !reader.get(AgentHeaders.CONTRACT_NAME).equals("")) {
                    checkContractHeaders();
                    doUpdate = true;
                    setContract(agent);
                }
                break;
            case AgentHeaders.ROTATING_PATTERN_NAME:
                if (reader.get(AgentHeaders.ROTATING_PATTERN_NAME) != null && !reader.get(AgentHeaders.ROTATING_PATTERN_NAME).equals("")) {
                    checkRotatingPatternHeaders();
                    doUpdate = true;
                    setRotatingPattern(agent);
                }
                break;
            case AgentHeaders.TIMEOFF_RULE_NAME:
                if (reader.get(AgentHeaders.TIMEOFF_RULE_NAME) != null && !reader.get(AgentHeaders.TIMEOFF_RULE_NAME).equals("")) {
                    checkTimeOffRuleHeaders();
                    doUpdate = true;
                    setTimeOffRule(agent);
                }
                break;
            case AgentHeaders.ACTIVITY_NAME:
                if (reader.get(AgentHeaders.ACTIVITY_NAME) != null && !reader.get(AgentHeaders.ACTIVITY_NAME).equals("")) {
                    checkActivityHeaders();
                    doUpdate = true;
                    setActivity(agent);
                }
                break;
            case AgentHeaders.EMAIL:
                if (reader.get(AgentHeaders.EMAIL) != null && !reader.get(AgentHeaders.EMAIL).equals("")) {
                    if (!value.equals(agent.getGswEmail())) {
                        doUpdate = true;
                        agent.setGswEmail(value);
                    }
                }
                break;
            case AgentHeaders.HIRE_DATE:
                if (reader.get(AgentHeaders.HIRE_DATE) != null && !reader.get(AgentHeaders.HIRE_DATE).equals("")) {
                    date = new OleDateTime(Integer.parseInt(value.substring(6)), Integer.parseInt(value.substring(3, 5)), Integer.parseInt(value.substring(0, 2))).asDouble();

                    if (agent.getWmStartDate() != date) {
                        doUpdate = true;
                        agent.setWmStartDate(date);
                    }
                }
                break;
            case AgentHeaders.TERMINATION_DATE:
                if (reader.get(AgentHeaders.TERMINATION_DATE) != null && !reader.get(AgentHeaders.TERMINATION_DATE).equals("")) {
                    date = new OleDateTime(Integer.parseInt(value.substring(6)), Integer.parseInt(value.substring(3, 5)), Integer.parseInt(value.substring(0, 2))).asDouble();

                    if (agent.getWmEndDate() != date) {
                        doUpdate = true;
                        agent.setWmEndDate(date);
                    }
                }
                break;
            case AgentHeaders.HOURLY_WAGE:
                if (reader.get(AgentHeaders.HOURLY_WAGE) != null && !reader.get(AgentHeaders.HOURLY_WAGE).equals("")) {
                    if (agent.getWmHourlyWage() != Integer.parseInt(value)) {
                        doUpdate = true;
                        agent.setWmHourlyWage(Integer.parseInt(value));
                    }
                }
                break;
            case AgentHeaders.RANK:
                if (reader.get(AgentHeaders.RANK) != null && !reader.get(AgentHeaders.RANK).equals("")) {
                    if (agent.getWmSeniority() != Integer.parseInt(value)) {
                        doUpdate = true;
                        agent.setWmSeniority(Integer.parseInt(value));
                    }
                }
                break;
            case AgentHeaders.COMMENTS:
                if (reader.get(AgentHeaders.COMMENTS) != null && !reader.get(AgentHeaders.COMMENTS).equals("")) {
                    if (!value.equals(agent.getWmComments())) {
                        doUpdate = true;
                        agent.setWmComments(value);
                    }
                }
                break;
            default:
                break;
        }

        return doUpdate;
    }

    private void checkRotatingPatternHeaders() throws IOException {
        String[] headerValues = this.reader.getHeaders();
        String[] requiredHeaders;
        if (checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE)) {
            requiredHeaders = new String[]{AgentHeaders.ROTATING_PATTERN_NAME, AgentHeaders.ROTATING_PATTERN_EFFECTIVE_DATE, AgentHeaders.TARGET_SITE, AgentHeaders.ROTATING_PATTERN_STARTING_WEEK};
        } else {
            requiredHeaders = new String[]{AgentHeaders.ROTATING_PATTERN_NAME, AgentHeaders.ROTATING_PATTERN_EFFECTIVE_DATE, AgentHeaders.SITE, AgentHeaders.ROTATING_PATTERN_STARTING_WEEK};
        }
        StringBuilder msg = new StringBuilder();
        boolean missingHeader = false;

        label:
        for (String requiredHeader : requiredHeaders) {
            for (String headerValue : headerValues) {
                if (headerValue.equals(requiredHeader)) {
                    continue label;
                }
            }
            missingHeader = true;
            msg.append(requiredHeader).append(", ");
        }
        if (missingHeader) {
            msg = msg.delete(msg.length() - 2, msg.length() - 1);
            throw new IllegalArgumentException("Missing rotating pattern headers: " + msg);
        }
    }

    private void checkActivityHeaders() throws IOException {
        String[] headerValues = this.reader.getHeaders();
        String[] requiredHeaders;
        if (checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE)) {
            requiredHeaders = new String[]{AgentHeaders.ACTIVITY_NAME, AgentHeaders.TARGET_SITE, AgentHeaders.ACTIVITY_EFFECTIVE_DATE, AgentHeaders.ACTIVITY_TYPE};
        } else {
            requiredHeaders = new String[]{AgentHeaders.ACTIVITY_NAME, AgentHeaders.SITE, AgentHeaders.ACTIVITY_EFFECTIVE_DATE, AgentHeaders.ACTIVITY_TYPE};
        }
        StringBuilder msg = new StringBuilder();
        boolean missingHeader = false;

        label:
        for (String requiredHeader : requiredHeaders) {
            for (String headerValue : headerValues) {
                if (headerValue.equals(requiredHeader)) {
                    continue label;
                }
            }
            missingHeader = true;
            msg.append(requiredHeader).append(", ");
        }
        if (missingHeader) {
            msg = msg.delete(msg.length() - 2, msg.length() - 1);
            throw new IllegalArgumentException("Missing activity headers: " + msg);
        }
    }

    private void checkTimeOffRuleHeaders() throws IOException {
        String[] headerValues = this.reader.getHeaders();
        String[] requiredHeaders;
        if (checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE)) {
            requiredHeaders = new String[]{AgentHeaders.TIMEOFF_RULE_NAME, AgentHeaders.TARGET_SITE, AgentHeaders.TIMEOFF_TYPE, AgentHeaders.TIMEOFF_RULE_START_DATE, AgentHeaders.TRANSFER_BALANCE};
        } else {
            requiredHeaders = new String[]{AgentHeaders.TIMEOFF_RULE_NAME, AgentHeaders.SITE, AgentHeaders.TIMEOFF_TYPE, AgentHeaders.TIMEOFF_RULE_START_DATE, AgentHeaders.TRANSFER_BALANCE};
        }
        StringBuilder msg = new StringBuilder();
        boolean missingHeader = false;

        label:
        for (String requiredHeader : requiredHeaders) {
            for (String headerValue : headerValues) {
                if (headerValue.equals(requiredHeader)) {
                    continue label;
                }
            }
            missingHeader = true;
            msg.append(requiredHeader).append(", ");
        }
        if (missingHeader) {
            msg = msg.delete(msg.length() - 2, msg.length() - 1);
            throw new IllegalArgumentException("Missing time-off rule headers: " + msg);
        }
    }

    private void checkContractHeaders() throws IOException {
        String[] headerValues = this.reader.getHeaders();
        String[] requiredHeaders;
        if (checkHeaderForValue(headerValues, AgentHeaders.TARGET_SITE)) {
            requiredHeaders = new String[]{AgentHeaders.CONTRACT_NAME, AgentHeaders.CONTRACT_EFFECTIVE_DATE, AgentHeaders.TARGET_SITE};
        } else {
            requiredHeaders = new String[]{AgentHeaders.CONTRACT_NAME, AgentHeaders.CONTRACT_EFFECTIVE_DATE, AgentHeaders.SITE};
        }
        StringBuilder msg = new StringBuilder();
        boolean missingHeader = false;

        label:
        for (String requiredHeader : requiredHeaders) {
            for (String headerValue : headerValues) {
                if (headerValue.equals(requiredHeader)) {
                    continue label;
                }
            }
            missingHeader = true;
            msg.append(requiredHeader).append(", ");
        }
        if (missingHeader) {
            msg = msg.delete(msg.length() - 2, msg.length() - 1);
            throw new IllegalArgumentException("Missing contract headers: " + msg);
        }
    }

//    private void setContract(CfgAgent agent) throws Exception {
//        CfgAgentContract agentContract = new CfgAgentContract();
//        agentContract.setWmContractId(getContractIDByName(agent.getWmSiteId()));
//        String value = this.reader.get(AgentHeaders.CONTRACT_EFFECTIVE_DATE);
//        double date = new OleDateTime(Integer.parseInt(value.substring(6)), Integer.parseInt(value.substring(3, 5)), Integer.parseInt(value.substring(0, 2))).asDouble();
//        agentContract.setWmEffectiveDate(date);
//        agent.getWmAgentContracts().add(agentContract);
//    }

    private void setContract(CfgAgent agent) throws Exception {
        boolean newContract = false;
        int contractID = getContractIDByName(agent.getWmSiteId());
        CfgAgentContract agentContract = agent.getWmAgentContracts().stream().filter(x -> x.getWmContractId() == contractID).findFirst().orElse(new CfgAgentContract());
        if (agentContract.getWmContractId() == 0) {
            newContract = true;
            agentContract.setWmContractId(contractID);
        }
        String value = this.reader.get(AgentHeaders.CONTRACT_EFFECTIVE_DATE);
        double date = new OleDateTime(Integer.parseInt(value.substring(6)), Integer.parseInt(value.substring(3, 5)), Integer.parseInt(value.substring(0, 2))).asDouble();
        agentContract.setWmEffectiveDate(date);
        if (newContract) {
            agent.getWmAgentContracts().add(agentContract);
        }
    }

    private void setActivity(CfgAgent agent) throws Exception {
        boolean newActivity = false;
        int activityID = getActivityIDByName(agent.getWmSiteId());
        CfgAgentActivity agentActivity = agent.getWmAgentActivities().stream().filter(x -> x.getWmActivityId() == activityID).findFirst().orElse(new CfgAgentActivity());
        if (agentActivity.getWmActivityId() == 0) {
            newActivity = true;
            agentActivity.setWmActivityId(activityID);
        }
        String value = this.reader.get(AgentHeaders.ACTIVITY_EFFECTIVE_DATE);
        double date = new OleDateTime(Integer.parseInt(value.substring(6)), Integer.parseInt(value.substring(3, 5)), Integer.parseInt(value.substring(0, 2))).asDouble();
        agentActivity.setWmEffectiveDate(date);
        switch (this.reader.get(AgentHeaders.ACTIVITY_TYPE)) {
            case "Disabled":
                agentActivity.setWmType(0);
                break;
            case "Primary":
                agentActivity.setWmType(1);
                break;
            case "Secondary":
                agentActivity.setWmType(2);
                break;
            case "Auto":
                agentActivity.setWmType(3);
                break;
        }
        if (newActivity) {
            agent.getWmAgentActivities().add(agentActivity);
        }
    }

    private void setRotatingPattern(CfgAgent agent) throws Exception {
        boolean newPattern = false;
        int rotatingPatternID = getRotatingPatternIDByName(agent.getWmSiteId());
        CfgAgentRotation agentRotatingPattern = agent.getWmAgentRotatingSch().stream().filter(x -> x.getWmRotatingSchId() == rotatingPatternID).findFirst().orElse(new CfgAgentRotation());
        if (agentRotatingPattern.getWmRotatingSchId() == 0) {
            newPattern = true;
            agentRotatingPattern.setWmRotatingSchId(rotatingPatternID);
        }
        agentRotatingPattern.setWmRotatingSchId(getRotatingPatternIDByName(agent.getWmSiteId()));
        agentRotatingPattern.setWmStartWeek(Integer.parseInt(this.reader.get(AgentHeaders.ROTATING_PATTERN_STARTING_WEEK)));
        String value = this.reader.get(AgentHeaders.ROTATING_PATTERN_EFFECTIVE_DATE);
        double date = new OleDateTime(Integer.parseInt(value.substring(6)), Integer.parseInt(value.substring(3, 5)), Integer.parseInt(value.substring(0, 2))).asDouble();
        agentRotatingPattern.setWmEffectiveDate(date);

        if (newPattern) {
            agent.getWmAgentRotatingSch().add(agentRotatingPattern);
        }
    }

    private void setTimeOffRule(CfgAgent agent) throws Exception {
        boolean newRule = false;
        int timeOffRuleId = getTimeOffRuleIDByName(agent.getWmSiteId());
        CfgAgentTimeOffRule agentTimeOffRule = agent.getWmAgentTimeOffRules().stream().filter(x -> x.getWmAccrualRuleId() == timeOffRuleId).findFirst().orElse(new CfgAgentTimeOffRule());
        if (agentTimeOffRule.getWmAccrualRuleId() == 0) {
            newRule = true;
            agentTimeOffRule.setWmAccrualRuleId(timeOffRuleId);
        }
        //Set Dates
        double startDate;
        double endDate;
        String startDateString = this.reader.get(AgentHeaders.TIMEOFF_RULE_START_DATE);
        String endDateString = this.reader.get(AgentHeaders.TIMEOFF_RULE_END_DATE);
        if (startDateString != null && !startDateString.equals("")) {
            startDate = new OleDateTime(Integer.parseInt(startDateString.substring(6)), Integer.parseInt(startDateString.substring(3, 5)), Integer.parseInt(startDateString.substring(0, 2))).asDouble();
            agentTimeOffRule.setWmCarryOverDate(startDate);
        }
        if (endDateString != null && !endDateString.equals("")) {
            endDate = new OleDateTime(Integer.parseInt(endDateString.substring(6)), Integer.parseInt(endDateString.substring(3, 5)), Integer.parseInt(endDateString.substring(0, 2))).asDouble();
            agentTimeOffRule.setWmStopAccrualDate(endDate);
        }
        //Set Time Off Types
        int currentType = getTimeOffTypeIDByName(agent.getWmSiteId());

        CfgAgentTimeOffType agentTimeOffType = new CfgAgentTimeOffType();
        agentTimeOffType.setWmTimeOffTypeId(currentType);
        agentTimeOffType.setWmTransferBalance(Boolean.parseBoolean(this.reader.get(AgentHeaders.TRANSFER_BALANCE)));
        agentTimeOffRule.getWmAgentTimeOffTypes().add(agentTimeOffType);

        if (newRule) {
            agent.getWmAgentTimeOffRules().add(agentTimeOffRule);
        }

    }

    private int getTeamIDByName(int siteID) throws Exception {
        String teamName = this.reader.get(AgentHeaders.TEAM);
        CfgTeam cfgTeam;
        int teamID = 0;
        for (BaseDTO team : teamCache.get(siteID)) {
            cfgTeam = (CfgTeam) team.getDataSOAP();
            if (cfgTeam.getWmSiteId() == siteID && cfgTeam.getWmName().equals(teamName)) {
                teamID = cfgTeam.getWmTeamId();
                break;
            }
        }
        if (teamID == 0) {
            throw new IllegalArgumentException("There is no team with name " + teamName);
        }
        return teamID;
    }

    private int getTimeOffTypeIDByName(int siteID) throws Exception {
        CfgTimeOffType timeOffType;
        String timeOffTypeName = this.reader.get(AgentHeaders.TIMEOFF_TYPE);
        if (timeOffTypeName.equals("Vacation")) return 0;
        int timeOffTypeID = 0;
        label:
        for (BaseDTO baseDTO : timeOffTypeCache.get(siteID)) {
            timeOffType = (CfgTimeOffType) baseDTO.getDataSOAP();
            for (Integer site : timeOffType.getWmSiteId()) {
                if (site == siteID && timeOffType.getWmName().equals(timeOffTypeName)) {
                    timeOffTypeID = timeOffType.getWmTimeOffTypeId();
                    break label;
                }
            }
        }
        if (timeOffTypeID == 0) {
            throw new IllegalArgumentException("There is no time-off type with name " + timeOffTypeName);
        }
        return timeOffTypeID;
    }

    private int getContractIDByName(int siteID) throws Exception {
        CfgContract contract;
        String contractName = this.reader.get(AgentHeaders.CONTRACT_NAME);
        int contractID = 0;
        label:
        for (BaseDTO baseDTO : contractCache.get(siteID)) {
            contract = (CfgContract) baseDTO.getDataSOAP();
            for (Integer site : contract.getWmSiteId()) {
                if (site == siteID && contract.getWmName().equals(contractName)) {
                    contractID = contract.getWmContractId();
                    break label;
                }
            }
        }
        if (contractID == 0) {
            throw new IllegalArgumentException("There is no contract with name " + contractName);
        }
        return contractID;
    }

    private int getActivityIDByName(int siteID) throws Exception {
        CfgActivity activity;
        String activityName = this.reader.get(AgentHeaders.ACTIVITY_NAME);
        int activityID = 0;
        for (BaseDTO baseDTO : activityCache.get(siteID)) {
            activity = (CfgActivity) baseDTO.getDataSOAP();
            if (activity.getWmSiteId() == siteID && activity.getWmName().equals(activityName)) {
                activityID = activity.getWmActivityId();
                break;
            }
        }
        if (activityID == 0) {
            throw new IllegalArgumentException("There is no activity with name " + activityName);
        }
        return activityID;
    }

    private int getTimeOffRuleIDByName(int siteID) throws Exception {
        CfgAccrualRule timeOffRule;
        String timeOffRuleName = this.reader.get(AgentHeaders.TIMEOFF_RULE_NAME);
        int timeOffRuleID = 0;
        label:
        for (BaseDTO baseDTO : timeOffRuleCache.get(siteID)) {
            timeOffRule = (CfgAccrualRule) baseDTO.getDataSOAP();
            for (Integer site : timeOffRule.getWmSiteId()) {
                if (site == siteID && timeOffRule.getWmName().equals(timeOffRuleName)) {
                    timeOffRuleID = timeOffRule.getWmAccrualRuleId();
                    break label;
                }
            }
        }
        if (timeOffRuleID == 0) {
            throw new IllegalArgumentException("There is no time-off rule with name " + timeOffRuleName);
        }
        return timeOffRuleID;
    }

    private int getRotatingPatternIDByName(int siteID) throws Exception {
        CfgRotation rotatingPattern;
        String rotatingPatternName = this.reader.get(AgentHeaders.ROTATING_PATTERN_NAME);
        int rotatingPatternID = 0;
        for (BaseDTO baseDTO : rotatingPatternCache.get(siteID)) {
            rotatingPattern = (CfgRotation) baseDTO.getDataSOAP();
            if (rotatingPattern.getWmSiteId() == siteID && rotatingPattern.getWmName().equals(rotatingPatternName)) {
                rotatingPatternID = rotatingPattern.getWmRotatingSchId();
                break;
            }
        }
        if (rotatingPatternID == 0) {
            throw new IllegalArgumentException("There is no rotating pattern with name " + rotatingPatternName);
        }
        return rotatingPatternID;
    }

    private boolean checkContracts(WfmObjectHolder source, WfmObjectHolder target) throws IllegalArgumentException {
        if (source.getDto() == null) {
            throw new IllegalArgumentException("Missing source checking the contracts association");
        }

        CfgAgent sourceObject = (CfgAgent) source.getDto().getDataSOAP();

        if (!sourceObject.getWmAgentContracts().isEmpty() && (target.getDto() == null || ((CfgAgent) target.getDto().getDataSOAP()).getWmAgentContracts().isEmpty())) {

            if (!MergeService.servicesForMerge.contains(this.contractService)) {
                List<Integer> searchIDs = new ArrayList<>();
                for (CfgAgentContract cfgAgentContract : sourceObject.getWmAgentContracts()) {
                    searchIDs.add(cfgAgentContract.getWmContractId());
                }

                source.getFilter().setIds(searchIDs);
                SetupObjects.scan(this.contractService, source, target);
                source.getFilter().setIds(null);
            }

            return true;
        }

        return false;
    }

    private boolean checkRotatingPatterns(WfmObjectHolder source, WfmObjectHolder target) throws IllegalArgumentException {
        if (source.getDto() == null) {
            throw new IllegalArgumentException("Missing source checking the Rotating Patterns association");
        }

        CfgAgent sourceObject = (CfgAgent) source.getDto().getDataSOAP();

        if (!sourceObject.getWmAgentRotatingSch().isEmpty() && (target.getDto() == null || ((CfgAgent) target.getDto().getDataSOAP()).getWmAgentRotatingSch().isEmpty())) {

            if (!MergeService.servicesForMerge.contains(this.rotatingPatternService)) {
                List<Integer> searchIDs = new ArrayList<>();
                for (CfgAgentRotation cfgAgentRotation : sourceObject.getWmAgentRotatingSch()) {
                    searchIDs.add(cfgAgentRotation.getWmRotatingSchId());
                }

                source.getFilter().setIds(searchIDs);
                SetupObjects.scan(this.rotatingPatternService, source, target);
                source.getFilter().setIds(null);
            }

            return true;
        }

        return false;
    }

    @Override
    public String getObjectServiceName() {
        return "Agent";
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgAgent sourceObject = (CfgAgent) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgAgent targetObject = (CfgAgent) listTarget.get(hashCode).getDataSOAP();
                targetSiteIDs.put(sourceObject.getGswAgentId(), targetObject.getGswAgentId());
            }
        }

        this.linkID = targetSiteIDs;
    }
}

