package com.blackchair.wfm;

import java.util.*;

import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.event.LogEvent;


@Service
public class RotatingPatternService extends WFMConfigService {
    @Autowired
    private SiteService siteService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ShiftService shiftService;

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {

        CfgRotationHolder cfgRotationHolder = this.getCfgHolder(wfmObjectHolder);

        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgRotation wfmEntity : cfgRotationHolder.getObjectArray()) {
            CfgRotationDTO dto = (CfgRotationDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    public CfgRotationHolder getCfgHolder(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgRotationFilter cfgRotationFilter = new CfgRotationFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();

        FilterDTO filterDTO = wfmObjectHolder.getFilter();

        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgRotationFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getSiteId() != null) {
                cfgRotationFilter.withWmSiteId(filterDTO.getSiteId());
            }

            if (filterDTO.getId() != null) {
                cfgRotationFilter.withWmRotatingSchId(filterDTO.getId());
            }

            if (filterDTO.getIds() != null) {
                cfgRotationFilter.withWmRotatingSchId(filterDTO.getIds());
            }
        }

        return super.getService(wfmObjectHolder).getRotation(cfgRotationFilter, cfgAgentFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Rotation.CFG_ROTATION_SORT_NAME).withAscending(true),
                new CfgRotationDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {
        WFMConfigService852Soap cfgRotation = super.getService(syncDTO.getMergedObject());
        CfgValidationHolder res = cfgRotation.updateRotation((CfgRotation) syncDTO.getMergedObject().getDto().getDataSOAP(), false, false);

        checkResponse(res);
    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {
        CfgRotation objectToInsert = (CfgRotation) syncDTO.getMergedObject().getDto().getDataSOAP();
        WfmObjectHolder mergedObject = syncDTO.getMergedObject();
        WFMConfigService852Soap cfgService = super.getService(mergedObject);
        CfgValidationHolder res = cfgService.insertRotation(objectToInsert, true);
        checkResponse(res);

        objectToInsert.setWmRotatingSchId(res.getObjectID());

        WfmObjectHolder newMergedObject = this.checkDiff(syncDTO.getSource(), syncDTO.getMergedObject(), SyncDTO.SyncStatus.UPDATE);
        syncDTO.setMergedObject(newMergedObject);

        try {
            this.event.publishEvent(new LogEvent(this, "update rotating weeks... "));
            this.update(syncDTO);
        } catch (Exception e) {
            this.event.publishEvent(new LogEvent(this, "FAIL to update rotating pattern: " + e.getMessage()));
        }

        CfgRotation sourceObject = (CfgRotation) syncDTO.getSource().getDto().getDataSOAP();
        this.linkID.put(sourceObject.getWmRotatingSchId(), res.getObjectID());
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgRotation wfmObject = (CfgRotation) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "Rotating Pattern";
    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {

        CfgRotation sourceObject = (CfgRotation) source.getDto().getDataSOAP();

        WfmObjectHolder mergedObjectHolder = null;

        CfgRotationDTO cfgDTO = new CfgRotationDTO();
        boolean doAction = false;
        switch (status) {
            case UPDATE:
                CfgRotation targetObject = (CfgRotation) target.getDto().getDataSOAP();

                this.linkID.put(sourceObject.getWmRotatingSchId(), targetObject.getWmRotatingSchId());

                sourceObject.setWmRotatingSchId(targetObject.getWmRotatingSchId());
                sourceObject.setWmSiteId(targetObject.getWmSiteId());

                if (sourceObject.getWmRotationWeeks().size() > 0 && targetObject.getWmRotationWeeks().size() == 0) {
                    doAction = true;

                    for (CfgRotationWeek cfgSourceRotationWeek : sourceObject.getWmRotationWeeks()) {
                        for (int i = 0; i < cfgSourceRotationWeek.getWmWeekDays().size(); i++) {
                            CfgRotationWeekDay cfgSourceRotationWeekDay = cfgSourceRotationWeek.getWmWeekDays().get(i);

                            if (cfgSourceRotationWeekDay.getWmShiftId() != 0) {
                                try {
                                    Integer targetShiftId = shiftService.getTargetIdBySourceId(cfgSourceRotationWeekDay.getWmShiftId());
                                    cfgSourceRotationWeekDay.setWmShiftId(targetShiftId);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }

                if (sourceObject.getWmRotationWeeks().size() > 0 && targetObject.getWmRotationWeeks().size() > 0) {
                    for (CfgRotationWeek cfgSourceRotationWeek : sourceObject.getWmRotationWeeks()) {

                        for (CfgRotationWeek cfgTargetRotationWeek : targetObject.getWmRotationWeeks()) {
                            if (!Objects.equals(cfgSourceRotationWeek.getWmName(), cfgTargetRotationWeek.getWmName())) {
                                continue;
                            }

                            for (int i = 0; i < cfgSourceRotationWeek.getWmWeekDays().size(); i++) {
                                CfgRotationWeekDay cfgSourceRotationWeekDay = cfgSourceRotationWeek.getWmWeekDays().get(i);
                                CfgRotationWeekDay cfgTargetRotationWeekDay = cfgTargetRotationWeek.getWmWeekDays().get(i);

                                if (cfgSourceRotationWeekDay.getWmShiftId() != 0) {
                                    try {
                                        Integer targetShiftId = shiftService.getTargetIdBySourceId(cfgSourceRotationWeekDay.getWmShiftId());

                                        if (targetShiftId != cfgTargetRotationWeekDay.getWmShiftId()) {
                                            doAction = true;
                                            cfgSourceRotationWeekDay.setWmShiftId(targetShiftId);
                                        }

                                    } catch (Exception e) {

                                        e.printStackTrace();
                                        doAction = true;
                                    }
                                }

                                if (cfgSourceRotationWeekDay.getWmPossibleDayOff() != cfgTargetRotationWeekDay.getWmPossibleDayOff()) {
                                    doAction = true;
                                }

                                if (cfgSourceRotationWeekDay.getWmDuration() != cfgTargetRotationWeekDay.getWmDuration()) {
                                    doAction = true;
                                }

                                if (cfgSourceRotationWeekDay.getWmStartTime() != cfgTargetRotationWeekDay.getWmStartTime()) {
                                    doAction = true;
                                }

                                if (cfgSourceRotationWeekDay.getWmStartTimeFlag() != cfgTargetRotationWeekDay.getWmStartTimeFlag()) {
                                    doAction = true;
                                }

                                if (cfgSourceRotationWeekDay.getWmActivities().size() != cfgTargetRotationWeekDay.getWmActivities().size()) {
                                    doAction = true;
                                }

                                for (Integer activityId : cfgSourceRotationWeekDay.getWmActivities()) {
                                    try {
                                        Integer targetActivityId = activityService.getTargetIdBySourceId(activityId);

                                        for (Integer targetWeekActivityId : cfgTargetRotationWeekDay.getWmActivities()) {
                                            if (Objects.equals(targetActivityId, targetWeekActivityId)) {
                                                continue;
                                            }

                                            doAction = true;
                                        }

                                    } catch (Exception e) {
                                        doAction = true;
                                    }
                                }

                            }
                        }
                    }
                }

                cfgDTO.setDataSOAP(sourceObject);
                break;
            case INSERT:
                doAction = true;
                List<Integer> sourceSiteIDs = new ArrayList<>();
                sourceSiteIDs.add(sourceObject.getWmSiteId());

                CfgRotation toInsertObject = new CfgRotation();

                toInsertObject.setWmName(sourceObject.getWmName());
                toInsertObject.setWmSiteId(this.siteService.getTargetSiteIDsBySourceSiteIDs(sourceSiteIDs).get(0));

                cfgDTO.setDataSOAP(toInsertObject);

                break;
        }

        if (doAction) {
            try {
                mergedObjectHolder = target.setDto(cfgDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mergedObjectHolder;
    }

    List<CfgAgentRotation> syncAgentRotatingSch(List<CfgAgentRotation> sourceAgentRotations) {
        for (CfgAgentRotation sourceAgentRotation : sourceAgentRotations) {
            if (this.linkID.containsKey(sourceAgentRotation.getWmRotatingSchId())) {
                sourceAgentRotation.setWmRotatingSchId(this.linkID.get(sourceAgentRotation.getWmRotatingSchId()));
            }
        }

        return sourceAgentRotations;
    }

    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {

        HashMap<Integer, Integer> targetSiteIDs = new HashMap<>();

        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgRotation sourceObject = (CfgRotation) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgRotation targetObject = (CfgRotation) listTarget.get(hashCode).getDataSOAP();
                targetSiteIDs.put(sourceObject.getWmRotatingSchId(), targetObject.getWmRotatingSchId());
            }
        }

        this.linkID = targetSiteIDs;
    }
}
