package com.blackchair.wfm;

import com.blackchair.dto.ConnectionDTO;
import com.blackchair.dto.EnvironmentRequestDTO;
import com.blackchair.dto.WfmObjectHolder;
import com.blackchair.tools.JwtTokenFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class CreateHolder {

    @Autowired
    protected JwtTokenFactory jwtTokenFactory;

    public WfmObjectHolder holder(EnvironmentRequestDTO requestDTO) throws IOException, ClassNotFoundException {

        WfmObjectHolder holder = new WfmObjectHolder();
        holder.setConnection((ConnectionDTO) this.jwtTokenFactory.parse(requestDTO.getToken()));
        holder.setFilter(requestDTO.getFilter());

        return holder;
    }
}
