package com.blackchair.wfm;

import java.util.HashMap;
import java.util.Map;

import com.blackchair.aggregator.FactoryDTO;
import com.blackchair.dto.*;
import com.genesyslab.wfm8.API.service.config852.*;
import org.springframework.stereotype.Service;


@Service
public class ActivityService extends WFMConfigService {

    @Override
    public HashMap<Integer, BaseDTO> getAll(WfmObjectHolder wfmObjectHolder) throws Exception {
        CfgActivityHolder cfgActivityHolder;
        CfgActivityFilter cfgActivityFilter = new CfgActivityFilter();
        CfgAgentFilter cfgAgentFilter = new CfgAgentFilter();
        CfgProfileFilter cfgProfileFilter = new CfgProfileFilter();
        FilterDTO filterDTO = wfmObjectHolder.getFilter();
        if (filterDTO != null) {
            if (filterDTO.getBuId() != null) {
                cfgActivityFilter.withWmBUId(filterDTO.getBuId());
            }
            if (filterDTO.getIds() != null) {
                cfgActivityFilter.withWmActivityId(filterDTO.getIds());
            }
            if (filterDTO.getSiteId() != null) {
                cfgActivityFilter.withWmSiteId(filterDTO.getSiteId());
            }
            if (filterDTO.getVirtualActivityId() != null) {
                cfgActivityFilter.withWmVirtualActivityId(filterDTO.getVirtualActivityId());
            }
            if (filterDTO.getAgentId() != null) {
                cfgAgentFilter.withGswAgentId(filterDTO.getAgentId());
            }
            if (filterDTO.getProfileId() != null) {
                cfgProfileFilter.withWmProfileId(filterDTO.getProfileId());
            }

        }

        cfgActivityHolder = super.getService(wfmObjectHolder).getActivity(cfgActivityFilter, cfgAgentFilter, cfgProfileFilter,
                new CfgSortMode().withSortMode(ECfgSortMode.Activity.CFG_ACTIVITY_SORT_NAME).withAscending(true),
                new CfgActivityDetails().withInfoType(ECfgInfoType.CFG_INFO_OBJECT));


        HashMap<Integer, BaseDTO> list = new HashMap<>();

        for (CfgActivity wfmEntity : cfgActivityHolder.getObjectArray()) {
            CfgActivityDTO dto = (CfgActivityDTO) FactoryDTO.create(wfmEntity);
            list.put(dto.hashCode(), dto);
        }

        return list;
    }

    @Override
    public void update(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public void insert(SyncDTO syncDTO) throws Exception {

    }

    @Override
    public WfmObjectHolder checkDiff(WfmObjectHolder source, WfmObjectHolder target, SyncDTO.SyncStatus status) {
        return null;
    }

    @Override
    public void setupLinkID(WfmObjectHolder source, WfmObjectHolder target) throws Exception {
        HashMap<Integer, BaseDTO> listSource = this.getAll(source);
        HashMap<Integer, BaseDTO> listTarget = this.getAll(target);

        for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
            Integer hashCode = entry.getKey();
            CfgActivity sourceObject = (CfgActivity) entry.getValue().getDataSOAP();

            if (listTarget.containsKey(hashCode)) {
                CfgActivity targetObject = (CfgActivity) listTarget.get(hashCode).getDataSOAP();
                this.linkID.put(sourceObject.getWmActivityId(), targetObject.getWmActivityId());
            }
        }
    }

    @Override
    public String getObjectName(WfmObjectHolder wfmObjectHolder) {
        CfgActivity wfmObject = (CfgActivity) wfmObjectHolder.getDto().getDataSOAP();
        return wfmObject.getWmName();
    }

    @Override
    public String getObjectServiceName() {
        return "Activity";
    }
}
