package com.blackchair.auth;

import com.genesyslab.wfm8.API.service.session800.ServiceInfo;
import com.genesyslab.wfm8.API.service.session800.WFMSessionService800Soap;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class Session {
    private HashMap<String, ServiceInfo> serviceMap = new HashMap<>();

    private WFMSessionService800Soap sessionService;

    WFMSessionService800Soap getSessionService() {
        return sessionService;
    }

    void setSessionService(WFMSessionService800Soap sessionService) {
        System.out.println("session start");
        this.sessionService = sessionService;
    }

    public HashMap<String, ServiceInfo> getServiceMap() {
        return serviceMap;
    }

    void setServiceMap(HashMap<String, ServiceInfo> serviceMap) {
        this.serviceMap = serviceMap;
    }
}
