package com.blackchair.auth;

import com.blackchair.FactoryServiceSoap;
import com.blackchair.dto.ConnectionDTO;
import com.genesyslab.wfm8.API.service.locator800.LoginInfo;
import com.genesyslab.wfm8.API.service.locator800.WFMLocatorService800Soap;
import com.genesyslab.wfm8.API.service.session800.ServiceInfo;
import com.genesyslab.wfm8.API.service.session800.SessionInfo;
import com.genesyslab.wfm8.API.service.session800.WFMSessionService800Soap;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class AuthenticationService {

    private HashMap<String, Session> sessions = new HashMap<>();

    public Session connect(ConnectionDTO connectionDTO) throws Exception {

        String sessionKey = connectionDTO.getWfmServerHost() + ":" + connectionDTO.getWfmServerPort() + ":" + connectionDTO.getUsername() + ":" + connectionDTO.getPassword();

        if (this.sessions.containsKey(sessionKey)) {
            Session session = this.sessions.get(sessionKey);
            //check if session is steal live;
            try {
                session.getSessionService().pingSession();
                return session;
            } catch (Exception e) {
            }
        }

        WFMLocatorService800Soap locator = FactoryServiceSoap.create(WFMLocatorService800Soap.class,
                "/LocatorService/WFMLocatorService800.wsdl", "WFMLocatorService800", "WFMLocatorService800Soap",
                connectionDTO.getWfmServerHost() + ":" + connectionDTO.getWfmServerPort() + "/?Handler=WFMLocatorService800");

        LoginInfo loginInfo = locator.locateServerLogin("SessionService800", connectionDTO.getAppName(),
                connectionDTO.getUsername(), connectionDTO.getPassword());

        System.out.println("Is Agent: " + loginInfo.isIsAgent());
        if (loginInfo.isIsAgent()) {
            System.out.println(connectionDTO.getUsername() + " has insufficient rights to access the application");
            throw new IllegalAccessException(connectionDTO.getUsername() + " has insufficient rights to access the application");
        }

//        String serviceUrl = fixUrl(loginInfo.getSessionServiceURL(), connectionDTO.getWfmServerHost());
        String serviceUrl = loginInfo.getSessionServiceURL();

        WFMSessionService800Soap sessionService = FactoryServiceSoap.create(WFMSessionService800Soap.class,
                "/SOAPServer/WFMSessionService800.wsdl", "WFMSessionService800", "WFMSessionService800Soap",
                serviceUrl);


        SessionInfo sessionInfo = sessionService.openSession(connectionDTO.getAppName(), loginInfo.getUserID());

        System.out.println("Session Opened with ID " + sessionInfo.getSid());

        HashMap<String, ServiceInfo> serviceMap = new HashMap<>();

        for (ServiceInfo serviceInfo : sessionInfo.getServices()) {
//            serviceInfo.setServiceURL(this.fixUrl(serviceInfo.getServiceURL(), connectionDTO.getWfmServerHost()));
//            serviceInfo.setServiceWSDL(this.fixUrl(serviceInfo.getServiceWSDL(), connectionDTO.getWfmServerHost()));
            serviceInfo.setServiceURL(serviceInfo.getServiceURL());
            serviceInfo.setServiceWSDL(serviceInfo.getServiceWSDL());

            serviceMap.put(serviceInfo.getServiceName(), serviceInfo);

            if (serviceInfo.getServiceName().compareTo("SessionService800") == 0) {
                sessionService = FactoryServiceSoap.create(WFMSessionService800Soap.class, "/SOAPServer/WFMSessionService800.wsdl",
                        "WFMSessionService800", "WFMSessionService800Soap", serviceInfo.getServiceURL());
            }
        }

        Session session = new Session();
        session.setSessionService(sessionService);
        session.setServiceMap(serviceMap);

        sessions.put(sessionKey, session);

        return session;
    }

    private String fixUrl(String url, String ip) {
        String correctUrl = "";

        Matcher matcher = Pattern.compile("http:\\/\\/(.*):").matcher(url);
        while (matcher.find()) {
            correctUrl = url.replace(matcher.group(1), ip);
            break;
        }

        return correctUrl;
    }
}
