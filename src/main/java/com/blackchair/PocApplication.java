package com.blackchair;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.URISyntaxException;

@SpringBootApplication
public class PocApplication {

    public static boolean debugMode = false;

    public static void main(String[] args) throws URISyntaxException {
        if (args.length > 0) {
            if (args[0].equals("-debug")) {
                debugMode = true;
            }
        }
        SpringApplication.run(PocApplication.class, args);
    }
}
