package com.blackchair.tools;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Logger {

    private static String logDir = Logger.getPath() + File.separator + "log";
    private static String backupLogDir = logDir + File.separator + "backup";
    private static String systemLogDir = logDir + File.separator + "system";
    private static String fileName = logDir + File.separator + "merge.log";
    private static String fileNameImport = logDir + File.separator + "import.log";
    private static String fileNameSystem = logDir + File.separator + "system.log";

    public static void log(String message) {
        System.out.println(message);
        Path path = Paths.get(fileName);
        Charset charset = StandardCharsets.UTF_8;
        List<String> list = Collections.singletonList(message);

        try {
            Files.write(path, list, charset, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void logImport(String message) {
        System.out.println(message);
        Path path = Paths.get(fileNameImport);
        Charset charset = StandardCharsets.UTF_8;
        List<String> list = Collections.singletonList(message);

        try {
            Files.write(path, list, charset, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void logSystem(String message) {
        System.out.println(message);
        Path path = Paths.get(fileNameSystem);
        Charset charset = StandardCharsets.UTF_8;
        List<String> list = Collections.singletonList(message);

        try {
            Files.write(path, list, charset, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clear() {
        checkPath(logDir);

        File checkFile = new File(fileName);
        if (checkFile.exists()) {
            checkPath(backupLogDir);
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
            String newFileName = backupLogDir + File.separator + "merge_" + timeStamp + ".log";
            // rename file
            checkFile.renameTo(new File(newFileName));

            try {
                FileWriter fwOb = new FileWriter(fileName, false);
                PrintWriter pwOb = new PrintWriter(fwOb, false);
                pwOb.flush();
                pwOb.close();
                fwOb.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void clearImport() {
        checkPath(logDir);

        File checkFile = new File(fileNameImport);
        if (checkFile.exists()) {
            checkPath(backupLogDir);
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
            String newFileName = backupLogDir + File.separator + "import_" + timeStamp + ".log";
            // rename file
            checkFile.renameTo(new File(newFileName));

            try {
                FileWriter fwOb = new FileWriter(fileNameImport, false);
                PrintWriter pwOb = new PrintWriter(fwOb, false);
                pwOb.flush();
                pwOb.close();
                fwOb.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void clearSystem() {
        checkPath(logDir);

        File checkFile = new File(fileNameSystem);
        if (checkFile.exists()) {
            checkPath(systemLogDir);
            String timeStamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
            String newFileName = systemLogDir + File.separator + "system_" + timeStamp + ".log";
            // rename file
            checkFile.renameTo(new File(newFileName));

            try {
                FileWriter fwOb = new FileWriter(fileNameSystem, false);
                PrintWriter pwOb = new PrintWriter(fwOb, false);
                pwOb.flush();
                pwOb.close();
                fwOb.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void checkPath(String dirPath) {
        File theDir = new File(dirPath);
// if the directory does not exist, create it
        if (!theDir.exists()) {
            try {
                theDir.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static String getPath() {
        String jarPath = null;
        Boolean isWindows = false;

        // includes: Windows 2000,  Windows 95, Windows 98, Windows NT, Windows Vista, Windows XP
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            isWindows = true;
        }

        try {
            String mainClassFolder = Logger.class.getProtectionDomain().getCodeSource().getLocation().getPath();
            mainClassFolder = URLDecoder.decode(mainClassFolder, "UTF-8");

            //windows OS is buggy and returns wrong separator character from ...getPath();
            mainClassFolder = mainClassFolder.replace('/', File.separatorChar);
            //here we check if the jar is compiled by IDE. By IDE we will not have "file:"... in the begging of the path
            if (mainClassFolder.contains("file:") && mainClassFolder.contains(".jar")) {
                String regex = "file:(.*)[\\/\\\\].*\\.jar";

                Matcher matcher = Pattern.compile(regex).matcher(mainClassFolder);
                while (matcher.find()) {
                    jarPath = matcher.group(1);
                }
            } else {
                String spliter;
                if (isWindows) {
                    spliter = "\\\\classes\\\\";
                } else {
                    spliter = File.separator + "classes" + File.separator;
                }

                String[] paths = mainClassFolder.split(spliter);

                if (paths.length > 0) {
                    jarPath = paths[0];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //if OS is windows we have to remove first char //\C:\... should be only //C:\...
        if (isWindows && jarPath != null) {
            jarPath = jarPath.substring(1);
        }

        return jarPath;
    }


}

