package com.blackchair.event;

import com.blackchair.event.importcsv.StartedImportEvent;
import com.blackchair.event.importcsv.StartedImportObjectEvent;
import com.blackchair.event.importcsv.StoppedImportEvent;
import com.blackchair.event.importcsv.StoppedImportObjectEvent;
import com.blackchair.event.merge.*;
import com.blackchair.event.systemlog.StartedSystemEvent;
import com.blackchair.tools.Logger;
import com.blackchair.websocket.ImportHandler;
import com.blackchair.websocket.LogHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MergeEventListener {

    @Autowired
    private LogHandler logHandler;

    @Autowired
    private ImportHandler importHandler;

    @EventListener
    public void handleLogEvent(LogEvent event) {
        if (!event.getMessage().isEmpty()) {
            String time = "[" + LocalDateTime.now() + "] ";
            this.log(time + event.getMessage());
        }
    }

    @EventListener
    public void handleLogImportEvent(LogImportEvent event) {
        if (!event.getMessage().isEmpty()) {
            String time = "[" + LocalDateTime.now() + "] ";
            this.logImport(time + event.getMessage());
        }
    }

    @EventListener
    public void handleLogSystemEvent(LogSystemEvent event) {
        if (!event.getMessage().isEmpty()) {
            String time = "[" + LocalDateTime.now() + "] ";
            this.logSystem(time + event.getMessage());
        }
    }

    @EventListener
    public void handlePercentEvent(PercentEvent event) {
        System.out.println(event.getMessage() + "% Ready");
        this.logHandler.updateLog(event.getMessage());
    }

    @EventListener
    public void handleStartMergeEvent(StartedMergeEvent event) {
        Logger.clear();
    }

    @EventListener
    public void handleStartImportEvent(StartedImportEvent event) {
        Logger.clearImport();
    }

    @EventListener
    public void handleStartSystemEvent(StartedSystemEvent event) {
        Logger.clearSystem();
    }

    @EventListener
    public void handleStartObjectMergeEvent(StartedMergeObjectEvent event) {
        String message = "------------------------------------------------------------------------";
        message += System.lineSeparator();
        message += "Merging: " + event.getMessage();
        message += System.lineSeparator();
        message += "------------------------------------------------------------------------";

        this.log(message);
    }

    @EventListener
    public void handleStartObjectImportEvent(StartedImportObjectEvent event) {
        String message = "------------------------------------------------------------------------";
        message += System.lineSeparator();
        message += "Importing: " + event.getMessage();
        message += System.lineSeparator();
        message += "------------------------------------------------------------------------";

        this.logImport(message);
    }

    @EventListener
    public void handleStopObjectMergeEvent(StoppedMergeEvent event) {
        this.log(event.getMessage());
    }

    @EventListener
    public void handleStopObjectMergeEvent(StoppedMergeObjectEvent event) {
        String message = "FINISH: " + event.getMessage() + "------------------------------------";
        this.log(message);
    }

    @EventListener
    public void handleStopObjectImportEvent(StoppedImportEvent event) {
        this.logImport(event.getMessage());
    }

    @EventListener
    public void handleStopObjectImportEvent(StoppedImportObjectEvent event) {
        String message = "FINISH: " + event.getMessage() + "------------------------------------";
        this.logImport(message);
    }

    private void log(String message) {
        Logger.log(message);
        this.logHandler.updateLog(message);
    }

    private void logImport(String message) {
        Logger.logImport(message);
        this.importHandler.updateLog(message);
    }

    private void logSystem(String message) {
        Logger.logSystem(message);
    }
}