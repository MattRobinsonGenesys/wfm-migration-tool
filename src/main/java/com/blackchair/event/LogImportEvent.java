package com.blackchair.event;

import org.springframework.context.ApplicationEvent;


public class LogImportEvent extends ApplicationEvent {
    private String message;

    public LogImportEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
