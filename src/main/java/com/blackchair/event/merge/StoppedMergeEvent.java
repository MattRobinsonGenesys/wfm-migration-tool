package com.blackchair.event.merge;


public class StoppedMergeEvent extends MergeEvent {
    public StoppedMergeEvent(Object source, String message) {
        super(source, message);
    }
}
