package com.blackchair.event.merge;

public class StoppedMergeObjectEvent extends MergeEvent {
    public StoppedMergeObjectEvent(Object source) {
        super(source);
    }
}
