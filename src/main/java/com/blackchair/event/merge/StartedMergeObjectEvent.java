package com.blackchair.event.merge;

public class StartedMergeObjectEvent extends MergeEvent {
    public StartedMergeObjectEvent(Object source, String message) {
        super(source, message);
    }
}
