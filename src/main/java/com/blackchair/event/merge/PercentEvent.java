package com.blackchair.event.merge;

import org.springframework.context.ApplicationEvent;

public class PercentEvent extends ApplicationEvent {
    private String message;

    public PercentEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
