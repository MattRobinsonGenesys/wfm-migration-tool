package com.blackchair.event.merge;

import org.springframework.context.ApplicationEvent;

public abstract class MergeEvent extends ApplicationEvent {
    private String message;

    MergeEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    MergeEvent(Object source) {
        super(source);
    }

    public String getMessage() {
        return message;
    }
}
