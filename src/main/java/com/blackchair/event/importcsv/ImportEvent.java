package com.blackchair.event.importcsv;

import org.springframework.context.ApplicationEvent;

public abstract class ImportEvent extends ApplicationEvent {
    private String message;

    public ImportEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    public ImportEvent(Object source) {
        super(source);
    }

    public String getMessage() {
        return message;
    }
}
