package com.blackchair.event.importcsv;

public class StoppedImportEvent extends ImportEvent {
    public StoppedImportEvent(Object source, String message) {
        super(source, message);
    }
}
