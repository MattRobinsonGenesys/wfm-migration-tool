package com.blackchair.event.importcsv;

public class StartedImportEvent extends ImportEvent {
    public StartedImportEvent(Object source) {
        super(source);
    }
}
