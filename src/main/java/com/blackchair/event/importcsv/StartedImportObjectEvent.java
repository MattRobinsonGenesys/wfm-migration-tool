package com.blackchair.event.importcsv;

public class StartedImportObjectEvent extends ImportEvent {
    public StartedImportObjectEvent(Object source) {
        super(source);
    }
}
