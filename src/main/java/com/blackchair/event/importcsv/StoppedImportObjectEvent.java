package com.blackchair.event.importcsv;

public class StoppedImportObjectEvent extends ImportEvent {
    public StoppedImportObjectEvent(Object source) {
        super(source);
    }
}
