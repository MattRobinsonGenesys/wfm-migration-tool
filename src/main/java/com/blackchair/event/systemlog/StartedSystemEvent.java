package com.blackchair.event.systemlog;

import com.blackchair.event.importcsv.ImportEvent;

public class StartedSystemEvent extends ImportEvent {
    public StartedSystemEvent(Object source, String message) {
        super(source, message);
    }

    public StartedSystemEvent(Object source) {
        super(source);
    }
}
