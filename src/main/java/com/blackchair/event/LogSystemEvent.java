package com.blackchair.event;

import org.springframework.context.ApplicationEvent;


public class LogSystemEvent extends ApplicationEvent {
    private String message;

    public LogSystemEvent(Object source, String message) {
        super(source);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
