package com.blackchair;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.URL;

public class FactoryServiceSoap {

    /**
     * Use this or similar method to create services. By default generated classes WFMSessionService800
     * and similar are useless, since a lot of manual work is required to create usable service
     *
     * @param <T>
     * @param soapClassName   - *Soap class, to be created
     * @param wsdlLocation    - location of wsdl inside wfm api jar. Jars are included into jar to avoid attempts
     *                        connecting to WFM servers to get wsdls from there
     * @param wsdlServiceName - name of service, as specified in wsdl
     * @param serviceSOAPName - name of *Soap class
     * @param serverUrl       - location of WFM server to which to connect
     * @return <T>
     * @throws Exception
     */
    public static <T> T create(Class<T> soapClassName, String wsdlLocation, String wsdlServiceName, String serviceSOAPName, String serverUrl) throws Exception {
        URL tmpUrl = soapClassName.getResource(wsdlLocation);
        Service srvc = Service.create(tmpUrl, new QName("urn:" + wsdlServiceName, wsdlServiceName));
        T rez = srvc.getPort(new QName("urn:" + wsdlServiceName, serviceSOAPName), soapClassName);
        ((BindingProvider) rez).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, serverUrl);
        return rez;
    }
}
