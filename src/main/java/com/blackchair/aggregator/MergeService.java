package com.blackchair.aggregator;

import java.io.IOException;
import java.util.*;

import com.blackchair.PocApplication;
import com.blackchair.dto.*;
import com.blackchair.event.LogEvent;
import com.blackchair.event.merge.PercentEvent;
import com.blackchair.event.merge.StoppedMergeEvent;
import com.blackchair.event.systemlog.StartedSystemEvent;
import com.blackchair.job.ScanThread;
import com.blackchair.job.SetupObjects;
import com.blackchair.job.Sync;
import com.blackchair.wfm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.blackchair.event.merge.StartedMergeEvent;
import com.blackchair.tools.JwtTokenFactory;


@Service
public class MergeService {

    @Autowired
    protected BreakService breakService;
    @Autowired
    protected MealService mealService;
    @Autowired
    protected TaskSequenceService taskSequenceService;
    @Autowired
    protected RotatingPatternService rotatingPatternService;
    @Autowired
    protected JwtTokenFactory jwtTokenFactory;
    @Autowired
    protected SiteService siteService;
    @Autowired
    protected ShiftService shiftService;
    @Autowired
    protected ContractService contractService;
    @Autowired
    protected AgentService agentService;
    @Autowired
    protected CalendarItemService calendarItemService;
    @Autowired
    protected ActivitySetService activitySetService;
    @Autowired
    protected ActivityService activityService;
    @Autowired
    private TeamService teamService;
    @Autowired
    private CarpoolService carpoolService;
    @Autowired
    private ApplicationEventPublisher event;
    @Autowired
    private Sync sync;
    public static List<WFMConfigService> servicesForMerge;
    private static int mergeCount;

    public void start(EnvironmentRequestDTO environmentRequestDTO) throws IOException, ClassNotFoundException {
        //Get start time of merge
        Long start = Calendar.getInstance().getTimeInMillis();

        this.event.publishEvent(new StartedMergeEvent(this));
        if (PocApplication.debugMode) {
            this.event.publishEvent(new StartedSystemEvent(this));
        }

        MergeService.servicesForMerge = new ArrayList<>();

        MergeService.servicesForMerge.add(this.breakService);
        MergeService.servicesForMerge.add(this.mealService);
        MergeService.servicesForMerge.add(this.taskSequenceService);
        MergeService.servicesForMerge.add(this.contractService);
        MergeService.servicesForMerge.add(this.shiftService);
        MergeService.servicesForMerge.add(this.rotatingPatternService);
        MergeService.servicesForMerge.add(this.agentService);
        MergeService.servicesForMerge.add(this.calendarItemService);

        MergeService.mergeCount = environmentRequestDTO.getMerge().size() - 1;

        sync.init(MergeService.mergeCount * MergeService.servicesForMerge.size());

        for (int i = 0; i < environmentRequestDTO.getMerge().size(); i++) {

            if (i == environmentRequestDTO.getMerge().size() - 1) {
                break;
            }

            EnvironmentRequestDTO masterRequest = environmentRequestDTO.getMerge().get(i);
            EnvironmentRequestDTO targetRequest = environmentRequestDTO.getMerge().get(i + 1);

            FilterDTO sourceFilterDTO = masterRequest.getFilter();
            FilterDTO targetFilterDTO = targetRequest.getFilter();

            WfmObjectHolder source = new WfmObjectHolder();
            WfmObjectHolder target = new WfmObjectHolder();

            source.setFilter(sourceFilterDTO);
            source.setConnection((ConnectionDTO) jwtTokenFactory.parse(masterRequest.getToken()));

            target.setFilter(targetFilterDTO);
            target.setConnection((ConnectionDTO) jwtTokenFactory.parse(targetRequest.getToken()));

            //reset linked maps
            siteService.setLinkID(new HashMap<>());
            activitySetService.setLinkID(new HashMap<>());
            activityService.setLinkID(new HashMap<>());
            carpoolService.setLinkID(new HashMap<>());
            teamService.setLinkID(new HashMap<>());

            this.event.publishEvent(new LogEvent(this, "Start scanning..."));
            try {
                siteService.setupLinkID(source, target);
                for (Map.Entry<Integer, Integer> entry : siteService.getLinkID().entrySet()) {
                    source.getFilter().setSiteId(entry.getKey());
                    target.getFilter().setSiteId(entry.getValue());

                    try {
                        this.event.publishEvent(new LogEvent(this, "linking activitySet..."));
                        activitySetService.setupLinkID(source, target);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                    try {
                        this.event.publishEvent(new LogEvent(this, "linking activity..."));
                        activityService.setupLinkID(source, target);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                    try {
                        this.event.publishEvent(new LogEvent(this, "linking carpool..."));
                        carpoolService.setupLinkID(source, target);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                    try {
                        this.event.publishEvent(new LogEvent(this, "linking team..."));
                        teamService.setupLinkID(source, target);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                    source.getFilter().setSiteId(null);
                    target.getFilter().setSiteId(null);
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            Thread scanThread = new Thread(new ScanThread(siteService.getLinkID(), MergeService.servicesForMerge, source, target));
            scanThread.start();

            while (scanThread.isAlive() || !SetupObjects.queue.isEmpty()) {
                if (!scanThread.isAlive() && !scanThread.isInterrupted()) {
                    scanThread.interrupt();
                }

                if (SetupObjects.queue.isEmpty()) {

                    Thread.currentThread();
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    QueueItem queueItem = SetupObjects.queue.poll();

                    if (queueItem != null) {
                        this.sync.merge(queueItem);
                    }
                }
            }


            MergeService.mergeCount--;
        }
        this.event.publishEvent(new LogEvent(this, "Merge ended in " + ((Calendar.getInstance().getTimeInMillis() - start) / 1000) + " seconds"));
        this.event.publishEvent(new PercentEvent(this, "100"));
        this.event.publishEvent(new StoppedMergeEvent(this, "Done!"));
        //Get total time of merge in seconds
        //System.out.println("Merge ended in " + ((Calendar.getInstance().getTimeInMillis() - start) / 1000) + " seconds");
    }
}
