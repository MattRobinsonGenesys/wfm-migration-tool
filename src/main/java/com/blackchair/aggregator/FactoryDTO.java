package com.blackchair.aggregator;


import com.blackchair.dto.BaseDTO;

public class FactoryDTO {


    public static <T> BaseDTO create(T SOAPObject) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String classDTOName = "com.blackchair.dto." + SOAPObject.getClass().getSimpleName() + "DTO";

        Class<?> clazz = Class.forName(classDTOName);
        BaseDTO object = (BaseDTO) clazz.newInstance();
        object.setDataSOAP(SOAPObject);

        return object;
    }
}
