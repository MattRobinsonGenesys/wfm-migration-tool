package com.blackchair.resourse;

import com.blackchair.Response;
import com.blackchair.dto.BaseDTO;
import com.blackchair.dto.EnvironmentRequestDTO;
import com.blackchair.dto.WfmObjectHolder;
import com.blackchair.wfm.BusinessUnitService;
import com.blackchair.wfm.CreateHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")
public class BusinessUnitController {
//    @Autowired
//    BusinessUnitController(BusinessUnitService service) {
//        super(service);
//    }

    @Autowired
    private CreateHolder holderService;

    @Autowired
    private BusinessUnitService businessUnitService;

    @RequestMapping(value = "/bu", method = RequestMethod.POST)
    public ResponseEntity<Response<HashMap<Integer, BaseDTO>>> list(@RequestBody EnvironmentRequestDTO requestDTO) {
        Response<HashMap<Integer, BaseDTO>> response = new Response<>();
        HttpStatus status = HttpStatus.OK;

        try {
            WfmObjectHolder source = this.holderService.holder(requestDTO);
            response.setData(businessUnitService.getAll(source));
        } catch (Exception e) {
            e.printStackTrace();
            response.setMessage(e.getMessage());
            status = HttpStatus.BAD_REQUEST;
            response.setSuccess(false);
        }

        return new ResponseEntity<>(response, status);
    }
}
