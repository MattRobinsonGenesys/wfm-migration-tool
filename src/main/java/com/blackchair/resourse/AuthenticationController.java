package com.blackchair.resourse;

import com.blackchair.Response;
import com.blackchair.auth.AuthenticationService;
import com.blackchair.dto.ConnectionDTO;
import com.blackchair.tools.JwtTokenFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1/auth")
public class AuthenticationController{

    @Autowired
    protected AuthenticationService authenticationService;

    @Autowired
    protected JwtTokenFactory jwtTokenFactory;

    @RequestMapping(value = "/validate", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Response<String>> validate(@RequestBody ConnectionDTO connectionDTO) {
        System.out.println("Validate.....");
        HttpStatus status = HttpStatus.OK;
        String token = "";
        Boolean success = true;
        Response<String> response = new Response<>();

        try {
            this.authenticationService.connect(connectionDTO);
            token = jwtTokenFactory.generate(connectionDTO);
        } catch (Exception e) {
            e.printStackTrace();
            response.setMessage(e.getMessage());
            status = HttpStatus.BAD_REQUEST;
            success = false;
        }

        response.setData(token);
        response.setSuccess(success);

        return new ResponseEntity<>(response, status);
    }
}
