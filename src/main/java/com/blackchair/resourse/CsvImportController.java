package com.blackchair.resourse;

import com.blackchair.dto.*;
import com.blackchair.wfm.SupportedObjectService;
import com.blackchair.wfm.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@CrossOrigin
@Controller
@RequestMapping("/api/v1/csv")
public class CsvImportController {

    @Autowired
    private SupportedObjectService supportedObjectService;

    @Autowired
    private UploadFileService uploadFileService;

    @RequestMapping(value = "/supportedObjects", method = RequestMethod.GET)
    public @ResponseBody
    List<SupportedObject> supportedObjects() {
        return this.supportedObjectService.getSupprotedObjects();
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<Void> update(@RequestParam("file") MultipartFile file) {

        ResponseEntity<Void> response;

        if (this.uploadFileService.uploadFile(file)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return response;
    }
}
