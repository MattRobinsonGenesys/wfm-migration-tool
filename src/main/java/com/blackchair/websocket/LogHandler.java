package com.blackchair.websocket;

import java.io.IOException;

import com.blackchair.aggregator.MergeService;
import com.blackchair.dto.EnvironmentRequestDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Component
public class LogHandler extends TextWebSocketHandler {

    @Autowired
    private MergeService mergeService;

    private WebSocketSession session;

    public void updateLog(String logMessage) {
        if (this.session != null && this.session.isOpen()) {
            try {
                session.sendMessage(new TextMessage(logMessage));
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("Connection established!");
        this.session = session;
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        if ("CLOSE".equalsIgnoreCase(message.getPayload())) {
            session.close();
        } else {
            EnvironmentRequestDTO requestDTO = null;
            ObjectMapper mapper = new ObjectMapper();

            try {
                requestDTO = mapper.readValue(message.getPayload(), EnvironmentRequestDTO.class);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            try {
                this.mergeService.start(requestDTO);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
