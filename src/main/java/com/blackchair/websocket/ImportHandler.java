package com.blackchair.websocket;

import com.blackchair.dto.EnvironmentRequestDTO;
import com.blackchair.dto.FilterDTO;
import com.blackchair.dto.ImportDTO;
import com.blackchair.dto.WfmObjectHolder;
import com.blackchair.wfm.CreateHolder;
import com.blackchair.wfm.CsvParserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import java.io.IOException;

@Component
public class ImportHandler extends TextWebSocketHandler {

    private WebSocketSession session;

    @Autowired
    private CsvParserService parserService;

    @Autowired
    private CreateHolder holderService;

    public void updateLog(String logMessage) {
        if (this.session != null && this.session.isOpen()) {
            try {
                session.sendMessage(new TextMessage(logMessage));
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("Connection established!");
        this.session = session;
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        if ("CLOSE".equalsIgnoreCase(message.getPayload())) {
            session.close();
        } else {
            ImportDTO requestDTO;
            ObjectMapper mapper = new ObjectMapper();
            try {
                System.out.println(message.getPayload());
                requestDTO = mapper.readValue(message.getPayload(), ImportDTO.class);
                EnvironmentRequestDTO environmentRequestDTO = new EnvironmentRequestDTO();
                environmentRequestDTO.setToken(requestDTO.getToken());
                environmentRequestDTO.setFilter(new FilterDTO());
                WfmObjectHolder holder = this.holderService.holder(environmentRequestDTO);
                this.parserService.csvParser(holder, requestDTO.getType());
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
