package com.blackchair.csvheaders;

import java.util.Arrays;
import java.util.List;

public class AgentHeaders implements BaseHeaders{
    //OPTIONAL FIELDS
    public static final String TARGET_SITE = "Target Site";
    public static final String TEAM = "Team";
    public static final String EMAIL = "Email";
    public static final String HIRE_DATE = "Hire Date";
    public static final String TERMINATION_DATE = "Termination Date";
    public static final String HOURLY_WAGE = "Hourly Wage";
    public static final String RANK = "Rank";
    public static final String COMMENTS = "Comments";
    //OPTIONAL CONTRACT ASSOCIATION
    public static final String CONTRACT_NAME = "Contract Name";
    public static final String CONTRACT_EFFECTIVE_DATE = "Contract Effective Date";
    //OPTIONAL TIME-OFF RULE ASSOCIATION
    public static final String TIMEOFF_RULE_NAME = "Time-off Rule Name";
    public static final String TIMEOFF_TYPE = "Time-off Type";
    public static final String TIMEOFF_RULE_START_DATE = "Time-off Rule Start Date";
    public static final String TRANSFER_BALANCE = "Transfer Balance";
    //optional
    public static final String TIMEOFF_RULE_END_DATE = "Time-off Rule End Date";
    //OPTIONAL ACTIVITY ASSOCIATION
    public static final String ACTIVITY_NAME = "Activity Name";
    public static final String ACTIVITY_EFFECTIVE_DATE = "Activity Effective Date";
    public static final String ACTIVITY_TYPE = "Activity Status";
    //OPTIONAL ROTATING PATTERN ASSOCIATION
    public static final String ROTATING_PATTERN_NAME = "Rotating Pattern Name";
    public static final String ROTATING_PATTERN_EFFECTIVE_DATE = "Rotating Pattern Effective Date";
    public static final String ROTATING_PATTERN_STARTING_WEEK = "Rotating Pattern Starting Week";

    @Override
    public List<String> getRequiredFields() {
        return null;
    }

    @Override
    public List<String> getOptionalFields() {
        return Arrays.asList(TARGET_SITE, TEAM, EMAIL, HIRE_DATE, TERMINATION_DATE, HOURLY_WAGE, RANK, COMMENTS);
    }
}
