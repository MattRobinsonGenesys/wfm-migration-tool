package com.blackchair.csvheaders;

import java.util.Arrays;
import java.util.List;

public class TimeOffBonusHeaders implements BaseHeaders{
    //REQUIRED FIELDS
    public static final String TIMEOFF_TYPE = "Time-off Type";
    public static final String START_DATE = "Start Date";
    //OPTIONAL FIELDS
    public static final String END_DATE = "End Date";
    public static final String COMMENTS = "Comments";
    public static final String BONUS_HOURS = "Bonus Hours";


    @Override
    public List<String> getRequiredFields() {
        return Arrays.asList(TIMEOFF_TYPE, START_DATE, SITE);
    }

    @Override
    public List<String> getOptionalFields() {
        return Arrays.asList(END_DATE, COMMENTS, BONUS_HOURS);
    }
}
