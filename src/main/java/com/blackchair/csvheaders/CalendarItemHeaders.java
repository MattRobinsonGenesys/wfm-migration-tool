package com.blackchair.csvheaders;

import java.util.Arrays;
import java.util.List;

public class CalendarItemHeaders implements BaseHeaders{
    //REQUIRED FIELDS
    public static final String DATE = "Date";
    public static final String ITEM_TYPE = "Item Type";
    public static final String FULL_DAY = "Full Day";
    //OPTIONAL FIELDS
    public static final String START_TIME = "Start Time";
    public static final String END_TIME = "End Time";
    public static final String COMMENT = "Comment";
    public static final String PAID_TIME = "Paid Time";

    @Override
    public List<String> getRequiredFields() {
        return Arrays.asList(SITE, DATE, ITEM_TYPE, FULL_DAY);
    }

    @Override
    public List<String> getOptionalFields() {
        return Arrays.asList(START_TIME, END_TIME, COMMENT, PAID_TIME);
    }
}
