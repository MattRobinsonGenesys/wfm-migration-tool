package com.blackchair.csvheaders;

import java.util.Arrays;
import java.util.List;

public interface BaseHeaders {
    //IDENTIFIER FIELDS
    String EMPLOYEE_ID = "Employee ID";
    String FIRST_NAME = "First Name";
    String LAST_NAME = "Last Name";
    //SITE IS USED IN ALMOST ALL FIELD COMBINATIONS
    String SITE = "Site";

    static List<String> getIdentifiers() {
        return Arrays.asList(EMPLOYEE_ID, FIRST_NAME, LAST_NAME);
    }
    List<String> getRequiredFields();
    List<String> getOptionalFields();
}
