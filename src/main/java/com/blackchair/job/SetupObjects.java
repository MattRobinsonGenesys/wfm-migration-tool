package com.blackchair.job;

import com.blackchair.dto.BaseDTO;
import com.blackchair.dto.QueueItem;
import com.blackchair.dto.SyncDTO;
import com.blackchair.dto.WfmObjectHolder;
import com.blackchair.wfm.WFMConfigService;

import java.util.*;

public class SetupObjects {

    private static List<SyncDTO> syncObjects = new ArrayList<>();
    public static int syncObjectsCount;
    public static volatile ArrayDeque<QueueItem> queue = new ArrayDeque<>();

    static void scan(HashMap<Integer, Integer> siteLinkId, List<WFMConfigService> servicesForMerge, WfmObjectHolder source, WfmObjectHolder target) {

        for (Map.Entry<Integer, Integer> entry : siteLinkId.entrySet()) {
            source.getFilter().setSiteId(entry.getKey());
            target.getFilter().setSiteId(entry.getValue());
            for (WFMConfigService wfmService : servicesForMerge) {
                SetupObjects.syncObjects = new ArrayList<>();

                System.out.println("Start scanning " + wfmService.getObjectServiceName());

                SetupObjects.scan(wfmService, source, target);

                QueueItem queueItem = new QueueItem();
                queueItem.setServiceName(wfmService.getObjectServiceName());
                queueItem.setObjects(SetupObjects.syncObjects);
                SetupObjects.syncObjectsCount += SetupObjects.syncObjects.size();
                SetupObjects.queue.add(queueItem);
            }
        }
    }

    public static void scan(WFMConfigService wfmService, WfmObjectHolder source, WfmObjectHolder target) {

        List<SyncDTO> toSync = new ArrayList<>();

        try {
            //get source list
            HashMap<Integer, BaseDTO> listSource = wfmService.getAll(source);
            HashMap<Integer, BaseDTO> listTarget = wfmService.getAll(target);

            for (Map.Entry<Integer, BaseDTO> entry : listSource.entrySet()) {
                Integer hashCode = entry.getKey();
                BaseDTO sourceObject = entry.getValue();

                WfmObjectHolder sourceHolder = source.setDto(sourceObject);
                WfmObjectHolder targetHolder = target;

                SyncDTO.SyncStatus status = SyncDTO.SyncStatus.INSERT;

                if (listTarget.containsKey(hashCode)) {
                    targetHolder = target.setDto(listTarget.get(hashCode));
                    status = SyncDTO.SyncStatus.UPDATE;
                }

                WfmObjectHolder mergedObjectHolder = wfmService.checkDiff(sourceHolder, targetHolder, status);

                if (mergedObjectHolder != null) {

                    SyncDTO syncDTO = new SyncDTO();
                    syncDTO.setStatus(status);
                    syncDTO.setSource(sourceHolder);
                    syncDTO.setTarget(targetHolder);
                    syncDTO.setService(wfmService);
                    syncDTO.setMergedObject(mergedObjectHolder);

                    toSync.add(syncDTO);
                }
            }

            SetupObjects.syncObjects.addAll(toSync);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
