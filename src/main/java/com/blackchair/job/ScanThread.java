package com.blackchair.job;

import com.blackchair.dto.WfmObjectHolder;
import com.blackchair.wfm.WFMConfigService;

import java.util.HashMap;
import java.util.List;

public class ScanThread implements Runnable {

    protected WfmObjectHolder source;
    protected WfmObjectHolder target;
    private HashMap<Integer, Integer> siteLinkId;
    private List<WFMConfigService> servicesForMerge;

    public ScanThread(HashMap<Integer, Integer> siteLinkId, List<WFMConfigService> servicesForMerge, WfmObjectHolder source, WfmObjectHolder target) {
        this.source = source;
        this.target = target;
        this.siteLinkId = siteLinkId;
        this.servicesForMerge = servicesForMerge;
    }

    @Override
    public void run() {
        SetupObjects.scan(this.siteLinkId, this.servicesForMerge, this.source, this.target);
    }
}
