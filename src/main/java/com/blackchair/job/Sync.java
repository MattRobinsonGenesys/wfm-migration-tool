package com.blackchair.job;

import com.blackchair.PocApplication;
import com.blackchair.dto.QueueItem;
import com.blackchair.dto.SyncDTO;
import com.blackchair.dto.WfmObjectHolder;
import com.blackchair.event.LogEvent;
import com.blackchair.event.LogSystemEvent;
import com.blackchair.event.merge.PercentEvent;
import com.blackchair.event.merge.StartedMergeObjectEvent;
import com.blackchair.wfm.SiteService;
import com.blackchair.wfm.WFMConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class Sync {
    private Set<String> mergeService;
    private Integer wfmObjectsReady;
    private Integer wfmSection = 0;
    private Integer allSectionCount = 0;
    @Autowired
    private ApplicationEventPublisher event;
    @Autowired
    private SiteService siteService;

    public void init(int allSectionCount) {
        this.allSectionCount = allSectionCount;
        this.wfmSection = 0;
        //this should collect finished source services
        //this should be empty every time when we have new source environment
        this.mergeService = new HashSet<>();
    }

    public void merge(QueueItem queueItem) {

        this.wfmObjectsReady = 0;
        this.wfmSection++;
        int tryActionCount = 10;
        for (SyncDTO syncDTO : queueItem.getObjects()) {
            WFMConfigService wfmService = syncDTO.getService();
            WfmObjectHolder mergedObject = syncDTO.getMergedObject();

            this.event.publishEvent(new StartedMergeObjectEvent(this, wfmService.getObjectServiceName() + " -> object: " + wfmService.getObjectName(mergedObject)));

            String message = "";
            switch (syncDTO.getStatus()) {
                case INSERT:
                    this.event.publishEvent(new LogEvent(this, "inserting... " + wfmService.getObjectName(mergedObject)));
                    try {
                        boolean success = false;
                        for (int i = 0; i < tryActionCount; i++) {
                            try {
                                wfmService.insert(syncDTO);
                                success = true;
                                break;
                            } catch (Exception e) {
                                if (e.getMessage().equals("Client received SOAP Fault from server: Database error has occurred Please see the server log to find more detail regarding exact cause of the failure.")) {
                                    this.event.publishEvent(new LogEvent(this, e.getMessage()));
                                    this.event.publishEvent(new LogEvent(this, "retrying to insert..."));
                                } else {
                                    throw e;
                                }
                            }
                        }

                        if (!success) {
                            throw new IllegalStateException("There was a problem with the server, try again later");
                        }

                        message = "INSERTED - " + wfmService.getObjectServiceName() + ": " + wfmService.getObjectName(mergedObject);

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (PocApplication.debugMode) {
                            for (int i = 0; i < e.getStackTrace().length; i++) {
                                this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                            }
                        }
                        message = "FAIL INSERT - " + wfmService.getObjectServiceName() + ": " + wfmService.getObjectName(mergedObject) + " :: " + e.getMessage();
                    }
                    break;
                case UPDATE:
                    this.event.publishEvent(new LogEvent(this, "updating... " + wfmService.getObjectName(mergedObject)));
                    try {
                        boolean success = false;
                        for (int i = 0; i < tryActionCount; i++) {
                            try {
                                wfmService.update(syncDTO);
                                success = true;
                                break;
                            } catch (Exception e) {
                                if (e.getMessage().equals("Client received SOAP Fault from server: Database error has occurred Please see the server log to find more detail regarding exact cause of the failure.")) {
                                    this.event.publishEvent(new LogEvent(this, e.getMessage()));
                                    this.event.publishEvent(new LogEvent(this, "retrying to update..."));
                                } else {
                                    throw e;
                                }
                            }
                        }

                        if (!success) {
                            throw new IllegalStateException("There was a problem with the server, try again later");
                        }
                        message = "UPDATED - " + wfmService.getObjectServiceName() + ": " + wfmService.getObjectName(mergedObject);

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (PocApplication.debugMode) {
                            for (int i = 0; i < e.getStackTrace().length; i++) {
                                this.event.publishEvent(new LogSystemEvent(this, "" + e.getStackTrace()[i]));
                            }
                        }
                        message = "FAIL UPDATE - " + wfmService.getObjectServiceName() + ": " + wfmService.getObjectName(mergedObject) + ":: " + e.getMessage();
                    }
                    break;
            }

            this.event.publishEvent(new LogEvent(this, message));
            this.calculatePercent(queueItem.getObjects());
        }

        mergeService.add(queueItem.getServiceName());
    }

    private void calculatePercent(List<SyncDTO> syncObjects) {
        double sitesCount = (!this.siteService.getLinkID().isEmpty() ? this.siteService.getLinkID().size() : 1);
        double sectionPercent = 100 / (this.allSectionCount * sitesCount);
        double proportionCorrect = ((double) ++this.wfmObjectsReady) / ((double) syncObjects.size());
        proportionCorrect *= 100;
        Integer percent = (int) ((sectionPercent * (this.wfmSection - 1)) + ((sectionPercent * proportionCorrect) / 100));
        this.event.publishEvent(new PercentEvent(this, percent.toString()));
    }
}
