package com.blackchair.dto;

import org.apache.commons.lang.builder.HashCodeBuilder;


import com.genesyslab.wfm8.API.service.config852.CfgRotation;

public class CfgRotationDTO  extends BaseDTO<CfgRotation> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
