package com.blackchair.dto;

public abstract class BaseDTO<T> {

    protected T dataSOAP;

    public T getDataSOAP() {
        return dataSOAP;
    }

    public void setDataSOAP(T dataSOAP) {
        this.dataSOAP = dataSOAP;
    }
}
