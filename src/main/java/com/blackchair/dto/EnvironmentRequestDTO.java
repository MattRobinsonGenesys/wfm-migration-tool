package com.blackchair.dto;

import java.util.List;

public class EnvironmentRequestDTO {
    private String token;

    private FilterDTO filter;

    private List<EnvironmentRequestDTO> merge;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public FilterDTO getFilter() {
        return filter;
    }

    public void setFilter(FilterDTO filter) {
        this.filter = filter;
    }

    public List<EnvironmentRequestDTO> getMerge() {
        return merge;
    }

    public void setMerge(List<EnvironmentRequestDTO> merge) {
        this.merge = merge;
    }
}
