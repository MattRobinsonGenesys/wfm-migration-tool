package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgBU;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgBUDTO extends BaseDTO<CfgBU> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
