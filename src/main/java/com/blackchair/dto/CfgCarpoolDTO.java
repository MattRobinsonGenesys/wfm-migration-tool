package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgCarpool;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgCarpoolDTO extends BaseDTO<CfgCarpool> {
    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
