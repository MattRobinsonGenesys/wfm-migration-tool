package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgTaskSequence;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgTaskSequenceDTO extends BaseDTO<CfgTaskSequence> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
