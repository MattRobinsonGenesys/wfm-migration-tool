package com.blackchair.dto;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.genesyslab.wfm8.API.service.config852.CfgSite;


public class CfgSiteDTO extends BaseDTO<CfgSite>
{

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder().append(super.dataSOAP.getWmName()).toHashCode();
	}
}
