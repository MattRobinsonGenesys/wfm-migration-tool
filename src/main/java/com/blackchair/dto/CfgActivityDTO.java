package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgActivity;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgActivityDTO extends BaseDTO<CfgActivity> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
