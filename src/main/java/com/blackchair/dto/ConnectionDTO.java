package com.blackchair.dto;

import java.io.Serializable;

public class ConnectionDTO implements Serializable {

    private String wfmServerHost;
    private String wfmServerPort;
    private String appName;
    private String username;
    private String password;


    public String getWfmServerHost() {
        return wfmServerHost;
    }

    public void setWfmServerHost(String wfmServerHost) {
        this.wfmServerHost = wfmServerHost;
    }

    public String getWfmServerPort() {
        return wfmServerPort;
    }

    public void setWfmServerPort(String wfmServerPort) {
        this.wfmServerPort = wfmServerPort;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "username:" + username + " wfmServerHost:" + wfmServerHost + " wfmServerPort:" + wfmServerPort + " appName:" + appName;
    }
}
