package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgAccrualRule;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgAccrualRuleDTO extends BaseDTO<CfgAccrualRule> {
    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
