package com.blackchair.dto;

import java.util.List;

public class SupportedObject {
    private String objectName;
    private int type;
    private List<String> fieldsForIdentifier;
    private List<String> requiredFields;
    private List<String> optionalFields;
    private List<String> optionalAssociations;
    private List<String> description;
    private List<String> examples;

    public SupportedObject() {
    }

    public SupportedObject(String objectName, int type, List<String> fieldsForIdentifier, List<String> requiredFields, List<String> optionalFields, List<String> optionalAssociations, List<String> description, List<String> examples) {
        this.objectName = objectName;
        this.type = type;
        this.fieldsForIdentifier = fieldsForIdentifier;
        this.requiredFields = requiredFields;
        this.optionalFields = optionalFields;
        this.optionalAssociations = optionalAssociations;
        this.description = description;
        this.examples = examples;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<String> getFieldsForIdentifier() {
        return fieldsForIdentifier;
    }

    public void setFieldsForIdentifier(List<String> fieldsForIdentifier) {
        this.fieldsForIdentifier = fieldsForIdentifier;
    }

    public List<String> getRequiredFields() {
        return requiredFields;
    }

    public void setRequiredFields(List<String> requiredFields) {
        this.requiredFields = requiredFields;
    }

    public List<String> getOptionalFields() {
        return optionalFields;
    }

    public void setOptionalFields(List<String> optionalFields) {
        this.optionalFields = optionalFields;
    }

    public List<String> getOptionalAssociations() {
        return optionalAssociations;
    }

    public void setOptionalAssociations(List<String> optionalAssociations) {
        this.optionalAssociations = optionalAssociations;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public List<String> getExamples() {
        return examples;
    }

    public void setExamples(List<String> examples) {
        this.examples = examples;
    }
}
