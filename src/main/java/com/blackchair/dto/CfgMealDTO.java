package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgMeal;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgMealDTO extends BaseDTO<CfgMeal> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
