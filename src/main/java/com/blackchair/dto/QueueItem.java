package com.blackchair.dto;

import java.util.List;

public class QueueItem {
    private String serviceName;

    private List<SyncDTO> objects;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public List<SyncDTO> getObjects() {
        return objects;
    }

    public void setObjects(List<SyncDTO> objects) {
        this.objects = objects;
    }
}
