package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgExceptionType;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgExceptionTypeDTO extends BaseDTO<CfgExceptionType> {
    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
