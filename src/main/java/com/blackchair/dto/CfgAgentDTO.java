package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgAgent;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgAgentDTO extends BaseDTO<CfgAgent> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getGswEmployeeId())
                .append(super.dataSOAP.getGswFirstName())
                .append(super.dataSOAP.getGswLastName())
                .toHashCode();
    }
}
