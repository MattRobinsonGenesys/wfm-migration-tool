package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgActivitySet;
import com.genesyslab.wfm8.API.service.config852.CfgBreak;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgActivitySetDTO extends BaseDTO<CfgActivitySet> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
