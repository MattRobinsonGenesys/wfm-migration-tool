package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgTimeOffType;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgTimeOffTypeDTO extends BaseDTO<CfgTimeOffType> {
    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
