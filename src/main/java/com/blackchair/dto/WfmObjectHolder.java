package com.blackchair.dto;

public class WfmObjectHolder implements Cloneable {
    private FilterDTO filter;
    private ConnectionDTO connection;
    private BaseDTO dto;

    public FilterDTO getFilter() {
        return filter;
    }

    public void setFilter(FilterDTO filter) {
        this.filter = filter;
    }

    public ConnectionDTO getConnection() {
        return connection;
    }

    public void setConnection(ConnectionDTO connection) {
        this.connection = connection;
    }

    public BaseDTO getDto() {
        return dto;
    }

    public WfmObjectHolder setDto(BaseDTO dto) throws CloneNotSupportedException {
        WfmObjectHolder holder = this.clone();
        holder.dto = dto;
        return holder;
    }

    @Override
    public WfmObjectHolder clone() throws CloneNotSupportedException {
        WfmObjectHolder holder = (WfmObjectHolder) super.clone();
        holder.setFilter(this.filter.clone());
        return holder;
    }
}
