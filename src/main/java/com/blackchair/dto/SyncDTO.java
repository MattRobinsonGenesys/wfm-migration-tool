package com.blackchair.dto;

import com.blackchair.wfm.WFMConfigService;

public class SyncDTO {
    protected WfmObjectHolder source;
    protected WfmObjectHolder target;
    private WfmObjectHolder mergedObject;
    protected WFMConfigService service;
    protected SyncStatus status;

    public enum SyncStatus {
        UPDATE, INSERT
    }

    public WfmObjectHolder getSource() {
        return source;
    }

    public void setSource(WfmObjectHolder source) {
        this.source = source;
    }

    public WfmObjectHolder getTarget() {
        return target;
    }

    public void setTarget(WfmObjectHolder target) {
        this.target = target;
    }

    public SyncStatus getStatus() {
        return status;
    }

    public void setStatus(SyncStatus status) {
        this.status = status;
    }

    public WfmObjectHolder getMergedObject() {
        return mergedObject;
    }

    public void setMergedObject(WfmObjectHolder mergedObject) {
        this.mergedObject = mergedObject;
    }

    public WFMConfigService getService() {
        return service;
    }

    public void setService(WFMConfigService service) {
        this.service = service;
    }
}
