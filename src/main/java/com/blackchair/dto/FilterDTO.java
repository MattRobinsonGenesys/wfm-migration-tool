package com.blackchair.dto;

import java.util.List;

public class FilterDTO implements Cloneable {

    private Integer id;
    private List<Integer> ids;
    private Integer buId;
    private Integer siteId;
    private Integer shiftId;
    private Integer agentId;
    private String agentEmployeeId;
    private String agentFirstName;
    private String agentLastName;
    private Integer contractId;
    private Integer breakId;
    private Integer mealId;
    private Integer taskSequenceId;
    private Integer profileId;
    private Integer virtualActivityId;

    public String getAgentFirstName() {
        return agentFirstName;
    }

    public void setAgentFirstName(String agentFirstName) {
        this.agentFirstName = agentFirstName;
    }
    public String getAgentLastName() {
        return agentLastName;
    }

    public void setAgentLastName(String agentLastName) {
        this.agentLastName = agentLastName;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShiftId() {
        return shiftId;
    }

    public void setShiftId(Integer shiftId) {
        this.shiftId = shiftId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getBuId() {
        return buId;
    }

    public void setBuId(Integer buId) {
        this.buId = buId;
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getBreakId() {
        return breakId;
    }

    public void setBreakId(Integer breakId) {
        this.breakId = breakId;
    }

    public Integer getMealId() {
        return mealId;
    }

    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    public Integer getTaskSequenceId() {
        return taskSequenceId;
    }

    public void setTaskSequenceId(Integer taskSequenceId) {
        this.taskSequenceId = taskSequenceId;
    }

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public Integer getVirtualActivityId() {
        return virtualActivityId;
    }

    public void setVirtualActivityId(Integer virtualActivityId) {
        this.virtualActivityId = virtualActivityId;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }

    public String getAgentEmployeeId() { return agentEmployeeId; }

    public void setAgentEmployeeId(String agentEmployeeId) { this.agentEmployeeId = agentEmployeeId; }

    @Override
    public FilterDTO clone() throws CloneNotSupportedException {
        return (FilterDTO) super.clone();
    }
}
