package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgShift;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgShiftDTO extends BaseDTO<CfgShift> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
