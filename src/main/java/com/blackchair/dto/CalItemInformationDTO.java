package com.blackchair.dto;

import com.blackchair.tools.ApplicationContextHolder;
import com.blackchair.wfm.AgentService;
import com.genesyslab.wfm8.API.service.calendar851.CalItemInformation;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CalItemInformationDTO extends BaseDTO<CalItemInformation> implements Comparable<CalItemInformationDTO> {

    @Override
    public int hashCode() {
        AgentService agentService = ApplicationContextHolder.getContext().getBean(AgentService.class);
        return new HashCodeBuilder().append(agentService.getLinkID().get(super.dataSOAP.getAgentId())).append(super.dataSOAP.getDate()).toHashCode();
    }

    @Override
    public int compareTo(CalItemInformationDTO o) {
        int sourceStatus = this.getDataSOAP().getSavedStatus();
        int targetStatus = o.getDataSOAP().getSavedStatus();
        //FIRST, SORT BASED ON STATUS (GRANTED>PREFERRED>DECLINED)
        switch (sourceStatus) {
            case 1: {
                //IF TARGET IS NOT GRANTED, SOURCE IS HIGHER PRIORITY
                if (targetStatus < 1) {
                    return 1;
                }
                //IF TARGET IS GRANTED, SORT BASED ON ITEM
                return sortByItem(o);
            }
            case 0: {
                switch (targetStatus) {
                    //IF TARGET IS GRANTED, TARGET IS HIGHER PRIORITY
                    case 1: {
                        return -1;
                    }
                    //IF BOTH ARE PREFERRED, SORT BASED ON ITEM
                    case 0: {
                        return sortByItem(o);
                    }
                    //IF TARGET IS DECLINED, SOURCE IS HIGHER PRIORITY
                    case -1: {
                        return 1;
                    }
                }
            }
            case -1: {
                //FOR NOW, TARGET IS ALWAYS HIGHER PRIORITY IF SOURCE IS DECLINED
                return -1;
            }
            //THIS CODE SHOULD BE UNREACHABLE, BUT IN CASE THE ITEM IS NONE OF THESE, RETURNING THAT TARGET IS HIGHER
            default : return -1;
        }
    }

    private int sortByItem(CalItemInformationDTO o) {
        int sourceItem = this.getDataSOAP().getItem();
        int targetItem = o.getDataSOAP().getItem();
        if (sourceItem == 0) {
            if (targetItem == 0) {
                return 0;
            }
            if (targetItem != 0) {
                return 1;
            }
        }
        //DAY OFF - 2
        if (sourceItem == 2) {
            if (targetItem == 0) {
                return -1;
            }
            if (targetItem == 2) {
                return 0;
            }
            if (targetItem == 1 || targetItem >= 3) {
                return 1;
            }
        }
        //FULL-DAY TIME OFF - 3, full day - 1
        if (sourceItem == 3 && this.getDataSOAP().getFullDay() == 1) {
            if (targetItem == 0 || targetItem == 2) {
                return -1;
            }
            if (targetItem == 3 && o.getDataSOAP().getFullDay() == 1) {
                return 0;
            }
            if (targetItem == 1 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 0) || targetItem >= 4) {
                return 1;
            }
        }
        //AVAILABILITY - 5
        if (sourceItem == 5) {
            if (targetItem == 0 || targetItem == 2 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 1)) {
                return -1;
            }
            if (targetItem == 5) {
                return 0;
            }
            if (targetItem == 4 || targetItem == 6 || targetItem == 1 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 0)) {
                return 1;
            }
        }
        //SHIFTS - 4
        if (sourceItem == 4) {
            if (targetItem == 0 || targetItem == 2 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 1) || targetItem == 5) {
                return -1;
            }
            if (targetItem == 4) {
                return 0;
            }
            if (targetItem == 6 || targetItem == 1 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 0)) {
                return 1;
            }
        }
        //WORKING HOURS - 6
        if (sourceItem == 6) {
            if (targetItem == 0 || targetItem == 2 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 1) || targetItem == 5 || targetItem == 4) {
                return -1;
            }
            if (targetItem == 4) {
                return 0;
            }
            if (targetItem == 1 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 0)) {
                return 1;
            }
        }
        //PART-DAY EXCEPTIONS - 1 AND PART-DAY TIME OFF - 3, full day - 0
        if (sourceItem == 1 || (sourceItem == 3 && this.getDataSOAP().getFullDay() == 0)) {
            if (targetItem == 0 || targetItem == 2 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 1) || targetItem >= 4) {
                return -1;
            }
            if (targetItem == 1 || (targetItem == 3 && o.getDataSOAP().getFullDay() == 0)) {
                return 0;
            }
        }
        //THIS CODE SHOULD BE UNREACHABLE, BUT IN CASE THE ITEM IS NONE OF THESE, RETURNING THAT TARGET IS HIGHER
        return -1;
    }
}
