package com.blackchair.dto;

import com.genesyslab.wfm8.API.service.config852.CfgContract;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class CfgContractDTO extends BaseDTO<CfgContract> {

    @Override
    public int hashCode() {
        return new HashCodeBuilder().
                append(super.dataSOAP.getWmName()).
                toHashCode();
    }
}
