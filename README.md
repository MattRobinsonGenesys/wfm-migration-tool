## Run application (on Windows)

First of all, you have to have java 8. If you don`t have java see this installation guide -> https://java.com/en/download/help/windows_manual_download.xml#download

When you have java, do one of the following:

#### Run automatically:

Execute the `start.bat` file in the tool directory

#### Run manually from command prompt:

Open command prompt and go to `wfm-config-copy-1.2.2.jar` file. If it is in `Desktop\poc` for example:

type: `cd Desktop\poc` -> hit Enter

type: `java -jar wfm-config-copy-1.2.4.jar` -> hit Enter
it will start the app

If you are using the testing vm machine go to browser and check wfm and wfmb:

go to
```
http://localhost:8080/wfm/
http://localhost:8080/wfmb/
```
User:`default`

Pass:`password`

Verify that the source and the target have different objects.

## Update application

Just replace the existing .jar file with the new one.

## Setup environments
Go to  http://localhost:8090
you will see the user interface

Add source and target environments

You have to write `http://` or `https://` in front of the environment host

testing vm machine (source):
```
Env host: http://192.168.45.130
Env port: 7006
App name: wfmserver85a
Username: default
```

testing vm machine (target):
```
Env host: http://192.168.45.130
Env port: 7003
App name: wfmserver85b
Username: default
```

## Merge
From the menu on the top, go to MERGE

Choose source and target environments.
Choose source business unit and target business unit.

```
It is strongly recommended that you create a snapshot/backup of the target database before merging, especially if you are modifying existing records
```

Hit `Start merge process` button

Merging will start.
At the end of merging log you will see "Done!".

If you want, you can check the log file in `Desktop\poc\log\merge.log`
The logs folder is in the jar file folder.

If you merge again the old merge.log file will be in "Desktop\poc\log\backup\" dir.

Verify that the source and the target have same objects.

## Import Data from CSV
From the menu on the top, go to DATA IMPORT

Choose the target environment and the type of object you want to import.

Read the given information and examples about the specific CSV formatting. The CSV headers don't have to be in a specific order, they just need to be present.
When you are inserting an `Optional association`, ALL fields given for the specific association are required and must be present unless specifically noted as optional
(they are listed after the required fields like this: "requiredfield1, requiredfield2; Optional: optionalfield1, optionalfield2").

Select a `.csv` file to import by pressing the `arrow-pointing-up` button in the `Select a file to import` section.

```
It is strongly recommended that you create a snapshot/backup of the target database before importing, especially if you are modifying existing records
```

Hit `Start import process` button.

Importing will start. At the end of importing log you will see "Done!".

If you want, you can check the log file in `Desktop\poc\log\import.log`
The logs folder is in the jar file folder.

If you merge again the old import.log file will be in "Desktop\poc\log\backup\" dir.

Verify that the correct objects have been imported.

## Detailed Error Logging (debug logging)
For troubleshooting reasons, you might have to run the application in detailed logging mode.

To do this, run the application the same way you would've done usually, just add a `-debug` flag at the end, like this:

`java -jar wfm-config-copy-1.2.4.jar -debug`

This will create a new log file, `system.log`, in which each error stack trace will be logged. The file itself is contained in the same folder as the other logs,
and the directory in which the older system logs are stored is called `system` instead of `backup` (still located in the `log` folder). 


